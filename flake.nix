{
  description = "The pyjam.as minecraft setup";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  inputs.minecraft = {
    url = "github:Ninlives/minecraft.nix";
    inputs.metadata.follows = "minecraft-metadata";
  };
  inputs.minecraft-metadata.url = "github:Ninlives/minecraft.json";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, minecraft, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        client = minecraft.legacyPackages.${system}.v1_20_1.fabric.client;

        server = minecraft.legacyPackages.${system}.v1_20_1.fabric.server;

        fabricApi = pkgs.fetchurl {
          name = "fabric-api.jar"; # must end in '.jar'
          url = "https://mediafilez.forgecdn.net/files/4787/692/fabric-api-0.90.0%2B1.20.1.jar";
          sha256 = "sha256-AFrtcV1tFOFrik8Dfv93BchMOZZ409AC7bQ4mchb6AI=";
        };

        ccTweaked = pkgs.fetchurl {
          name = "cc-tweaked.jar"; # must end in '.jar'
          url =
            "https://mediafilez.forgecdn.net/files/4787/783/cc-tweaked-1.20.1-fabric-1.108.3.jar";
          sha256 = "sha256-TneKVNvt0A9QoD7lJtrjDWyzNWJfWUgoFXI1+kU5u0o=";
        };

        moddedClient = client.withConfig [{ mods = [ fabricApi ccTweaked ]; }];

        moddedServer = server.withConfig [{ mods = [ fabricApi ccTweaked ]; }];

        clientApp = {
          type = "app";
          program = "${moddedClient}/bin/minecraft";
        };

        serverApp = {
          type = "app";
          program = "${moddedServer}/bin/minecraft-server";
        };

      in {
        packages.default = moddedClient;
        packages.client = moddedClient;
        packages.server = moddedServer;

        apps.default = clientApp;
        apps.client = clientApp;
        apps.server = serverApp;
      });
}
