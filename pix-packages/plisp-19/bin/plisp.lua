local std = pix_require "@stdlib"
local fp = pix_require "@libfunc"
local codegen = pix_require "codegen"

local DEBUG = true

local function compileplispfile(plisppath, outpath)
  local plispfile = fs.open(plisppath, "r")
  local plispcode = plispfile.readAll()
  
  local luacode = codegen.plisp2lua(plispcode, plisppath)

  local outfile = fs.open(outpath, "w")
  outfile.write(luacode)
  outfile.close()
end

local function runplispfile(plisppath)
  -- figure out where to save stuff
  local pattern = "[^/]+$"
  local filename = plisppath:match(pattern)
  local outpath = plisppath:gsub(pattern, "").."."..filename..".lua"
  
  -- do the thang
  compileplispfile(plisppath, outpath)
  shell.run("/" .. outpath)
end

local function run_repl_line(plispcode)
  local luacode = codegen.plisp2lua(plispcode)
  if DEBUG then 
    term.setTextColor(colors.lightGray)
    print(luacode)
    term.setTextColor(colors.white)
  end
  
  -- this fails if the plisp compiles to a lua statement
  local result, err = load("return " .. luacode)
  -- because `return a = 2` is bad lua syntax
  
  -- ...so if the code is a statement, just drop the value
  if result == nil then
    result, err = load(luacode)
  end
  
  -- TODO: require? pix_require?
  if result == nil then
    print(err)
    error()
  else 
    std.pprint(result())
  end
end

local remove_trailing = function(line, char)
  if string.sub(line, #line, #line) == char then
    return string.sub(line, 1, #line-1)
  end
  return line
end

local repl_complete = function(line)
  -- TODO: replace with identifier parser from parse
  local complete_word_pos = string.find(line, "[a-zA-Z0-9%[%]%._]+$")
  local complete_word = ""
  if complete_word_pos then
    complete_word = string.sub(line, complete_word_pos)
  end
  if #complete_word >= 1 then
    local result = textutils.complete(complete_word, _ENV)
    return fp.map(result, function(s) return remove_trailing(s, "(") end)
  end
end

local function repl()
  -- quick and dirty repl
  local history = {}
  while true do
    write("plisp> ")
    local plispcode = read(nil, history, repl_complete)
    table.insert(history, plispcode)
    local ok, err = pcall(run_repl_line, plispcode)
    if DEBUG and (not ok) then
      print(err)
    end
  end
end

local function main()
  if arg[1] == nil then
    repl()
  elseif fs.exists(arg[1]) then
    runplispfile(arg[1])
  elseif fs.exists(shell.resolve(arg[1])) then
    runplispfile(shell.resolve(arg[1]))
  else
    print("could not find file: " .. arg[1])
    error()
  end
end

main()

return {
  runplisp = runplisp
}
