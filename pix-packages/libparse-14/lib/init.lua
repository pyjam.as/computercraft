local libparse = {}

-- ok so heres the deal
-- a parser is a function
-- (text, loc) -> ({loc=loc, thing=<whatever>}, remaining, nextloc)
-- you can make a parser from a lua pattern with `parse_pattern`
-- everything else makes a new parser from one or more parsers
-- loc is { line=number, col=number }

local incr_loc = function(text, loc)
  -- count lines
  local _, newlines = string.gsub(text, "\n", "")
  
  -- count cols
  local col
  if newlines == 0 then
    col = loc.col + #text
  else
    last_line = text:match("\n[^\n]*$")
    col = #last_line
  end
  return {
    line = loc.line + newlines,
    col = col
  }
end

libparse.parse_pattern = function(pattern)
  -- make a parser from a lua pattern
  return function(text, loc)
    local patternhere = pattern
    
    -- prepend ^ if not in the pattern
    -- (we only munch from the head)
    if string.sub(patternhere, 1, 1) ~= "^" then
      patternhere = "^" .. patternhere
    end
    
    -- do match magic
    local matched = text:match(patternhere)
    if matched == nil then
      return nil, loc, text
    end
    local remaining = string.sub(text, matched:len()+1, text:len())
    local newloc = incr_loc(matched, loc)
    return {loc=loc, thing=matched}, remaining, newloc
  end
end

libparse.parse_many = function(parserf)
  -- parse one or more
  return function(text, loc)
    local firstloc = loc
    local remaining = text
    local results = { }
    while true do
      parsed, newremaining, newloc = parserf(remaining, loc)
      if parsed == nil and #results > 0 then
        local returnval = { thing = results, loc = firstloc }
        return returnval, remaining, loc
      elseif parsed == nil and #results == 0 then
        return nil, remaining, loc
      end
      loc = newloc
      remaining = newremaining
      table.insert(results, parsed)
    end
  end
end

libparse.parse_any = function(...)
  -- parse one of the given parsers
  local parsers = {...}
  return function(text, loc)
    for _,parserf in pairs(parsers) do
      local parsed, remaining, newloc = parserf(text, loc)
      if parsed ~= nil then
        return parsed, remaining, newloc
      end
    end
    return nil, text, loc
  end
end

libparse.parse_sequence = function(...)
  -- parse all of the given parsers, in order
  local parsers = {...}
  return function(text, loc)
    local results = { }
    local firstloc = loc
    local remaining = text
    for k, parser in pairs(parsers) do
      local parsed, newremaining, newloc = parser(remaining, loc)
      if parsed == nil then
        return nil, text, loc
      end
      remaining = newremaining
      loc = newloc
      table.insert(results, parsed)
    end
    return { thing = results, loc = firstloc }, remaining, newloc
  end
end

libparse.parser_map = function(p, f)
  -- apply function to the result of a parser if it succeeds
  return function(text, loc)
    local parsed, remaining, newloc = p(text, loc)
    if parsed ~= nil then
      local result = { loc = parsed.loc, thing = f(parsed.thing) }
      return result, remaining, newloc
    end
    return nil, remaining, newloc
  end
end

libparse.ith = function(p, i)
  -- TODO: does not handle loc properly
  -- return parser that gets the ith element of the result of a parser
  return libparse.parser_map(p, function(x) return x[i] end)
end

libparse.parse_separated_by = function(parserf, separatorf)
  -- TODO: does not handle loc properly
  return libparse.parser_map(
    libparse.parse_sequence(
      parserf,
      libparse.parse_many(libparse.ith(
        libparse.parse_sequence(separatorf, parserf),
        2
      ))
    ),
    function(x)
      if #x < 2 then
        return x
      end
      return { x[1], unpack(x[2]) }
    end
  )
end

libparse.parse_between = function(beforep, innerp, afterp)
  -- TODO: does not handle loc properly
  return libparse.ith(
    libparse.parse_sequence(beforep, innerp, afterp),
    2
  )
end

libparse.parse_optional = function(p)
  return function(text, loc)
    local parsed, remaining, newloc = p(text, loc)
    if parsed == nil then
      return "", text, loc
    end
    return parsed, remaining, newloc
  end
end

libparse.tag = function(tagname, p)
  return function(text, loc)
    local parsed, remaining, newloc = p(text, loc)
    if parsed ~= nil then
      parsed.tag = tagname
    end
    return parsed, remaining, newloc
  end
end

libparse.whitespace_parser = libparse.parse_pattern("[ \n]+")

libparse.eof_parser = function(text, loc)
  if text == "" then
    return "", "", loc
  end
  return nil, text, loc
end

libparse.fail = function(msg)
  return function(text, loc)
    print("error on line:"..loc.line..", col:"..loc.col)
    print(msg)
    if text[1] ~= nil then
      print("got: " .. text[1])
    else
      print("got: EOF")
    end
    error()
  end
end

return libparse
