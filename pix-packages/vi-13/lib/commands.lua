function handle_command(command)
  local args = string.split(command, " ")
  local name = args[2]
  local line_num = string.match(args[1], "^(%d+)$")
  local edit = string.match(args[1], "^e")
  local write = string.match(args[1], "^wq?!?$")
  local force = string.match(args[1], "^w?q?!$")
  local quit = string.match(args[1], "^w?q!?$")
  local bprev = string.match(args[1], "^bp$")
  local bdelete = string.match(args[1], "^bd$")
  local bnext = string.match(args[1], "^bn$")
  if #args > 2 then
    return ("E492: Not an editor command: %s"):format(command)
  elseif edit and name then
    new_filename, err = read_file(name)
    if err then
      return err
    else 
      filename = new_filename
      return ("\"%s\" %dL"):format(filename, #buf().data)
    end
  elseif line_num then 
    local line_num = tonumber(line_num)
    buf().cursor_y = lim_y(line_num)
    buf().cursor_x = lim_x(buf().cursor_x, buf().cursor_y)
    return ""
  elseif bnext or bdelete or bprev then
    local keys = table.keys(buffers)
    if bdelete or bprev then
      local prev_filename = filename
      filename = keys[(table.indexOf(keys, filename)-2)%#keys+1]
      if bdelete then
        buffers[prev_filename] = nil
        if #keys == 1 then
          filename = read_file("")
          return ""
        end
      end
    elseif bnext then
      filename = keys[(table.indexOf(keys, filename))%#keys+1]
    end
    if filename == "" then
      return ""
    else
      return ("\"%s\" %dL"):format(filename, #buf().data)
    end
  elseif bnext then
  elseif write or quit then
    if name then
      local new_filename = "/"..shell.resolve(name)
      if fs.isDir(new_filename) then
        return "E69: \""..new_filename.."\" is a directory"
      else
        buffers[new_filename] = buffers[filename]
        buffers[filename] = nil
        filename = new_filename
      end
    end
    if write then
      if filename == "" then 
        return "E32: No file name"
      end
      write_file(filename)
      buffers[filename].changed = false
    end
    if quit then
      if buf().changed and not force then
        return ("E37: No write since last change (add ! to override)"):format(command)
      else
        term.clear()
        term.setCursorPos(1,1) 
        -- exit
        error() 
      end
    end
    if write then
      return ("\"%s\" %dL written"):format(filename, #buf().data)
    end
  end
end
