pix_require "movement"
pix_require "actions"
pix_require "timetravel"
pix_require "utils"
pix_require "commands"
pix_require "suggest"
pix_require "@stdlib"

SCREEN_WIDTH, SCREEN_HEIGHT = term.getSize()

-- throw away the key_up enter event from launching the program
os.pullEvent("key_up")

mode = "view"
local command = ""
local prev_key = ""
local prev_action = ""
clipboard = ""
local move = nil
local action = nil
local prev_move = nil
local prev_action = nil
local prev_modifier = nil
local action_key = nil
local modifier = ""
local is_dot = false
local ctrl_held = false
search = ""
local tf_move = nil
local edit_suggest = nil
local insert_suggest = nil

buffers = {}

filename, err = read_file(arg[1] or "")
if err then print(err) error() end
for i=2,#arg do 
  local _, err = read_file(arg[i])
  if err then print(err) error() end
end

function buf()
  return buffers[filename]
end

if filename ~= "" then
  status = string.format("\"%s\" %dL", filename, #buf().data)
end

local textColour = colours.white
local highlightColour = colours.yellow
local keywordColour = colours.yellow
local commentColour = colours.green
local stringColour = colours.red


-- Stolen from CCTweaked edit program
local function tryWrite(sLine, regex, colour)
  local match = string.match(sLine, regex)
  if match then
    if type(colour) == "number" then
        term.setTextColour(colour)
    else
        term.setTextColour(colour(match))
    end
    term.write(match)
    term.setTextColour(textColour)
    return string.sub(sLine, #match + 1)
  end
  return nil
end

local function writeHighlighted(sLine)
  while #sLine > 0 do
    sLine =
      tryWrite(sLine, "^%-%-%[%[.-%]%]", commentColour) or
      tryWrite(sLine, "^%-%-.*", commentColour) or
      tryWrite(sLine, "^\"\"", stringColour) or
      tryWrite(sLine, "^\".-[^\\]\"", stringColour) or
      tryWrite(sLine, "^\'\'", stringColour) or
      tryWrite(sLine, "^\'.-[^\\]\'", stringColour) or
      tryWrite(sLine, "^%[%[.-%]%]", stringColour) or
      tryWrite(sLine, "^[%w_]+", function(match)
      if tKeywords[match] then
          return keywordColour
      end
      return textColour
      end) or
      tryWrite(sLine, "^[^%w_]", textColour)
  end
end

function render()
  local line_num_width = math.max(countDigit(#buf().data)+1, 4)
  local max_line_len = SCREEN_WIDTH - line_num_width
  local max_lines = SCREEN_HEIGHT-1
  local render_line_nums = {}
  local render_lines = {}


  -- preprocess buf() lines to render lines
  -- this is to handle wrapping
  for i=1,#buf().data do
    line_num = i
    line_text = buf().data[line_num]
    table.insert(render_line_nums, line_num)
    while true do
      table.insert(render_lines, string.sub(line_text, 1, max_line_len))
      if string.len(line_text) > max_line_len then
        line_text = string.sub(line_text, max_line_len+1, -1)
        table.insert(render_line_nums, 0)
      else
        break
      end
    end
  end

  -- add max_wrap phantom lines
  for i=0,max_lines,1 do
    table.insert(render_line_nums, -1)
    table.insert(render_lines, "~")
  end

  -- handle scroll offset
  local line_wraps = math.floor((buf().cursor_x-1) / max_line_len)
  local render_lines_cursor_y = table.indexOf(render_line_nums, buf().cursor_y)+line_wraps
  if render_lines_cursor_y <= buf().scroll_offset then
    buf().scroll_offset = render_lines_cursor_y-1
  elseif render_lines_cursor_y > buf().scroll_offset+max_lines then
    buf().scroll_offset = render_lines_cursor_y-max_lines
  end

  term.clear()
  -- render lines
  for i=1,max_lines,1 do
    local line_num = render_line_nums[i+buf().scroll_offset]
    local line = render_lines[i+buf().scroll_offset]
    -- wrapped lines don't have a line number
    if line_num > 0 then
      -- normal line
      local num
      if buf().cursor_y == line_num then
        num = line_num
        term.setCursorPos(1, i)
      else
        num = math.abs(line_num-buf().cursor_y)
        term.setCursorPos(line_num_width-countDigit(num), i)
      end
      term.setTextColour(colors.yellow)
      write(("%d "):format(num or 0))
      term.setCursorPos(line_num_width+1, i)
      term.setTextColour(colors.white)  
    elseif(line_num == 0) then
      -- wrapped line
      term.setCursorPos(line_num_width+1, i)
    elseif(line_num == -1) then
      -- phantom line, render nothing
      term.setCursorPos(1, i)
    end
    writeHighlighted(line)
  end
  -- bottom bar
  term.setCursorPos(1, SCREEN_HEIGHT)
  term.setTextColour(colors.white)
  if mode == "insert" then write("-- insert --") end
  if mode == "command" then
    write(":" .. command)
    if edit_suggest then
      term.setBackgroundColor(colors.gray)
      write(edit_suggest)
      term.setBackgroundColor(colors.black)
    end
  end
  if mode == "regexsearch" then write("/" .. search) end
  if status then
    if status:sub(1,1) == "E" then
      term.setBackgroundColor(colors.red)
    end
    write(status)
    -- only show status for one render
    status = ""
    term.setBackgroundColor(colors.black)
  end
  -- cursor
  term.setCursorPos(line_num_width+buf().cursor_x-(line_wraps*(max_line_len)), render_lines_cursor_y-buf().scroll_offset)
  term.setCursorBlink(true)
end

function handle_input(event, key)
  if ctrl_held and key == "c" then
    buf().cursor_x = lim_x(buf().cursor_x-1,  buf().cursor_y)
    command = ""
    action = nil
    mode = "view"
    return
  end
  if mode == "r" then
    if event == "char" then
      action = vi_r(key)
      move = vi_nop
      mode = "view"
      key = ""
    end
  end
  if mode == "forward" then
    if event == "char" then
      move = tf_move(key)
      mode = "view"
      key = ""
    end
  end
  if mode == "regexsearch" then
    if event == "char" then
      search = search..key
    end
    if key == "backspace" then
      search = string.removeAt(search,string.len(search))
    end
    if key == "enter" then
      buf().cursor_x, buf().cursor_y = vi_n(buf().cursor_x, buf().cursor_y, 0, search)
      buf().max_cursor_x = buf().cursor_x
      mode = "view"
    end
  elseif mode == "command" then
    -- attempt to de-duplicate char events
    if event == "char" then
      command = command..key
    end
    if key == "tab" and edit_suggest then
      command = command..edit_suggest
    end
    if key == "backspace" then
      command = string.removeAt(command,string.len(command))
    end
    if key == "enter" then
      local result = handle_command(command)
      if not result then
        status = ("E492: Not an editor command: %s"):format(command)
      else
        status = result
      end
      command = ""
      mode = "view"
    end
    edit_suggest = find_edit_suggest(command)
  elseif mode == "view" then
    -- modes
    if key == ":" then mode = "command"
    elseif key == "/" then
      mode = "regexsearch"
      search = ""
    -- handle modifier
    elseif modifier == "" and key == "0" then move = vi_0 
    elseif string.match(key, "[0-9]") then
      modifier = modifier .. key
    -- handle dd, yy, cc and alike
    elseif key == action_key then move = vi_nop
    elseif ctrl_held and key == "r" then
      if buf().forwards then
        buffers[filename] = buf().forwards
      end
    -- movement
    elseif key == "$" then move = vi_end
    elseif key == "%" then move = vi_percent
    elseif key == "_" then move = vi__
    elseif key == "h" then move = vi_h
    elseif key == "j" then move = vi_j
    elseif key == "k" then move = vi_k
    elseif key == "l" then move = vi_l
    elseif key == "b" then move = vi_b
    elseif key == "B" then move = vi_B
    elseif key == "G" then move = vi_G
    elseif key == "g" and prev_key == "g" then move = vi_gg
    elseif key == "g" then prev_key = "g"
    elseif ctrl_held and (key == "d" or key == "u") then
      local dir
      if key == "u" then dir = -1 elseif key == "d" then dir = 1 end
      local diff = buf().cursor_y - buf().scroll_offset
      buf().cursor_y = lim_y(buf().cursor_y+math.floor((SCREEN_HEIGHT/2-1)*dir))
      buf().scroll_offset = math.max(1, buf().cursor_y - diff)
    elseif key == "z" and prev_key == "z" then
      buf().scroll_offset = math.max(0, buf().cursor_y - math.floor(SCREEN_HEIGHT/2)-1)
    elseif key == "u" then
      if buf().backwards then
        buffers[filename] = buf().backwards
      end
    elseif key == "z" then prev_key = "z" 
    elseif key == "b" then move = vi_b
    elseif key == "e" then move = vi_e
    elseif key == "n" then move = vi_n
    elseif key == "N" then move = vi_N
    elseif key == "W" then
      if action_key == "c" then
        move = vi_cW
      else
        move = vi_W
      end
    elseif key == "w" then
      if action_key == "c" then
        move = vi_cw
      else
        move = vi_w
      end
    elseif key == "J" then
      move = vi_nop
      action = vi_J
    elseif key == "f" then
      tf_move = vi_f
      mode = "forward"
    elseif key == "F" then
      tf_move = vi_F
      mode = "forward" 
    elseif key == "t" then
      tf_move = vi_t
      mode = "forward"
    elseif key == "T" then
      tf_move = vi_T
      mode = "forward"
    elseif key == "." then
      is_dot = true
      move = prev_move
      action = prev_action
    elseif key == "I" then
      move = vi_0
      action = vi_insert
    elseif key == "i" then
      move = vi_nop
      action = vi_insert
    elseif key == "a" then
      move = vi_a
      action = vi_insert
    elseif key == "A" then
      move = vi_A
      action = vi_insert
    elseif key == "o" then
      move = vi__
      action = vi_o
    elseif key == "O" then
      move = vi__
      action = vi_O
    elseif key == "p" then
      move = vi_nop
      action = vi_p
    elseif key == "P" then
      move = vi_nop
      action = vi_P
    elseif(key == "s") then
      move = vi_nop
      action = vi_s
    elseif(key == "X") then
      move = vi_h
      action = vi_x
    elseif(key == "x") then
      move = vi_nop
      action = vi_x
    elseif(key == "~") then
      move = vi_l
      action = vi_tilde
    elseif(key == "r") then
      mode = "r"
    elseif(key == "C") then
      action = vi_c
      move = vi_end
    elseif(key == "c") then
      action = vi_c
      action_key = "c"
    elseif(key == "y") then
      action = vi_y
      action_key = "y"
    elseif(key == "Y") then
      action = vi_y
      move = vi_end
    elseif(key == "d") then
      action = vi_d
      action_key = "d"
    elseif(key == "D") then
      action = vi_d
      move = vi_end
    elseif(key == "<") then
      action = vi_lt
      action_key = "<"
    elseif(key == ">") then
      action = vi_gt
      action_key = ">"
    end
    if move then
      local modifier_num = tonumber(modifier) or 1
      local repeats = 1
      if is_dot then
        repeats = modifier_num
        modifier_num = tonumber(prev_modifier) or 1
        modifier = prev_modifier
      end
      for i=1,repeats do
        if action then
          buffers[filename] = buf():snapshot()
          action(move, modifier_num)
        else
          for i=1,modifier_num do
            local cursor_x, cursor_y, no_set_max_cursor_x = move(buf().cursor_x, buf().cursor_y)
            buf().cursor_x, buf().cursor_y = cursor_x, cursor_y
            if not no_set_max_cursor_x then buf().max_cursor_x = cursor_x end
          end
        end
      end
      prev_action = action
      prev_move = move
      prev_modifier = modifier
      action = nil
      action_key = nil
      move = nil
      modifier = ""
      prev_key = ""
      is_dot = false
    end
  -- insert mode
  elseif mode == "insert" then
    if ctrl_held and (key == "n" or key == "p") then
      if insert_suggest then
        -- remove previous suggestion
        buf().data[buf().cursor_y] = string.removeRange(buf().data[buf().cursor_y], buf().cursor_x - insert_suggest:len(), buf().cursor_x)
        buf().cursor_x = buf().cursor_x - insert_suggest:len()
      end
      local from = vi_b(buf().cursor_x, buf().cursor_y)
      local word = buf().data[buf().cursor_y]:sub(from, buf().cursor_x-1)
      local dir
      if key == "n" then dir = 1 elseif key == "p" then dir = -1 end
      insert_suggest = find_insert_suggest(word, insert_suggest, dir)
      if insert_suggest and insert_suggest ~= "" then
        buf().data[buf().cursor_y] = string.insert(buf().data[buf().cursor_y], insert_suggest, buf().cursor_x)
        buf().cursor_x = buf().cursor_x+insert_suggest:len()
        buf().max_cursor_x = buf().cursor_x
      end
    elseif event == "char" then
      insert_suggest = nil
      buf().data[buf().cursor_y] = string.insert(buf().data[buf().cursor_y], key, buf().cursor_x)
      buf().cursor_x = buf().cursor_x+1
      buf().max_cursor_x = buf().cursor_x
    elseif key == "backspace" then
      insert_suggest = nil
      if buf().cursor_y == 1 and buf().cursor_x == 1 then
        -- do nothing
      elseif buf().data[buf().cursor_y] == ""  or buf().cursor_x == 1 then
        if #buf().data > 1 then
          local line = buf().data[buf().cursor_y] 
          table.remove(buf().data, buf().cursor_y)
          buf().cursor_x = vi_end(buf().cursor_x, buf().cursor_y-1, 1)
          buf().cursor_y = buf().cursor_y-1
          buf().data[buf().cursor_y] = buf().data[buf().cursor_y] .. line
          if buf().cursor_y > #buf().data then
            buf().cursor_y = #buf().data
          end
        end
      else
        local whitespace_len = leading_whitespace(buf().cursor_x, buf().cursor_y):len()
        local remove = 1
        if whitespace_len > 1 and whitespace_len+2 > buf().cursor_x and (buf().cursor_x-1) % 2 == 0 then
          remove = 2
        end
        buf().data[buf().cursor_y] = string.removeRange(buf().data[buf().cursor_y], buf().cursor_x - remove, buf().cursor_x)
        buf().cursor_x = buf().cursor_x - remove
      end
      buf().max_cursor_x = buf().cursor_x
    elseif key == "tab" then
      insert_suggest = nil
        buf().data[buf().cursor_y] = string.insert(buf().data[buf().cursor_y], "  ", buf().cursor_x)
        buf().cursor_x = buf().cursor_x+2
        buf().max_cursor_x = buf().cursor_x
    elseif key == "enter" then
      insert_suggest = nil
      line1, line2 = string.splitAt(buf().data[buf().cursor_y], buf().cursor_x)
      buf().data[buf().cursor_y] = line1
      local whitespace = leading_whitespace(buf().cursor_x, buf().cursor_y)
      table.insert(buf().data, buf().cursor_y+1, whitespace .. line2)
      buf().cursor_y = lim_y(buf().cursor_y+1)
      buf().cursor_x = vi__(buf().cursor_x, buf().cursor_y, 1)
      buf().max_cursor_x = buf().cursor_x
    end
  end
end

local event, key
while true do
  render()
  repeat 
    event, key = os.pullEvent()
    if event == "key_up" or event == "key" then
      local name = keys.getName(key)
      if name == "leftCtrl" then
        ctrl_held = (event == "key")
        event = nil
      end
      if name and name:len() == 1 then
        if ctrl_held and event == "key" then
          event = "char"
        else
          event = nil
        end
      end
      key = name
    end
  until key and event == "key" or event == "char"
  handle_input(event, key)
end 
