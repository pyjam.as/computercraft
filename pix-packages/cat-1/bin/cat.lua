if #arg == 0 then
  print("Usage: cat [FILE]..")
  error()
end

for _, filename in ipairs(arg) do
  local filepath, err = shell.resolve(filename)
  local f = fs.open(filepath, "r")
  write(f.readAll())
  f.close()
end

