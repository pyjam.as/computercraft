PROTOCOL = "storage"
host = 54

function main()
  local itemname = arg[1]

  rednet.open("back")
  if not host then
    print("Looking up rednet hosts...")
    host = rednet.lookup(PROTOCOL)
    if not host then
      error("could not find storage host", 1)
    end
  end

  local count = count(host, itemname)
  print(count .. " " .. itemname)
end


function details(host, itemname)
  print("Sent request. Waiting for response...")
  rednet.send(host, {action="details", item=itemname}, PROTOCOL)
  local _, details = rednet.receive(PROTOCOL)
  return details
end


function count(host, itemname)
  local details = details(host, itemname)
  return fold(
    details,
    function (acc, x) return acc + x.count end,
    0
  )
end


--- LIB


function fold(acc, f, initial)
  local result = initial
  for _,v in pairs(acc) do
    result = f(result, v)
  end
  return result
end


function map(list, f)
  local result = {}
  for k,v in pairs(list) do
    result[k] = f(v)
  end
  return result
end


function filter(list, f)
  local result = {}
  for _,v in pairs(list) do
    if f(v) then
      table.insert(result, v)
    end
  end
  return result
end


function pprint(e)
  print(textutils.serialize(e))
end


main()
