local std = pix_require("@stdlib")

local lib = {}

local count = 0
function lib.makeRemoteFunc(module, func_name, id)
  return function (...)
    local args = {...}
    rednet.send(id, {module=module, name=func_name, args=args}, "ssh")
    local id, msg = rednet.receive("ssh")
    local func = _G
    if module then
      func = func[module]
    end
    func[func_name](unpack(args))
    return unpack(msg)
  end
end

function lib.makeLoad(id)
  return function(x, name, mode, env) 
    env = lib.inject(env)
    return load(x, name, mode, env)
  end
end

local overwrites = {  "print", "write", "read", "printError" }

local textutils_overwrites = { "pagedTabulate", "slowWrite", "slowPrint", "pagedPrint", "tabulate" }

local term_overwrites = { "write", "scroll", "getCursorPos", "setCursorPos", "getCursorBlink", "setCursorBlink", "getSize", "clear", "clearLine", "getTextColour", "getTextColor", "setTextColor", "setTextColour", "getBackgroundColor", "getBackgroundColour", "setBackgroundColor", "setBackgroundColour", "isColour", "isColor", "blit" }


rednet.host("ssh", "server")
print("Waiting for connection..")
local client, msg = rednet.receive("ssh")
print(client.. " connected")

function lib.inject(env)
  env = env or {}
  -- Overwrite 
  for _, o in ipairs(overwrites) do
    env[o] = lib.makeRemoteFunc(nil, o, client)
  end
  -- overwrite textutils
  env.textutils = setmetatable({}, { __index = _G.textutils })
  for _, k in ipairs(textutils_overwrites) do
    env.textutils[k] = lib.makeRemoteFunc("textutils", k, client)
  end
  -- ovewrite term
  env.term = setmetatable({}, { __index = _G.term })
  for _, k in ipairs(term_overwrites) do
    env.term[k] = lib.makeRemoteFunc("term", k, client)
  end
  env.load = lib.makeLoad(client)
  return env
end

function handleRemoteEvents()
  while true do
    local _, event = rednet.receive("ssh_event")
    os.queueEvent(unpack(event))
  end
end

function runProgram()
  local env = lib.inject({}) 
  os.run(env, "/rom/programs/shell.lua")
end

parallel.waitForAny(runProgram, handleRemoteEvents)
