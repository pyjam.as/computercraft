-- If you pix_require("@this_package")
-- this file will be run
local function foo()
  return "bar"
end

-- This best-practice avoids polluting environments
return { foo = foo }

