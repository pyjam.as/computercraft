if #arg == 0 then
  print("This application displays how to use pix!")
  print("It has a minimal package.pix, a startup file")
  print("which adds the executable to the PATH and adds")
  print("autocomplete options, and a small executable.")
  print()
  print("Usage: cat [FILE]..")
  error()
end

for _, filename in ipairs(arg) do
  local filepath, err = shell.resolve(filename)
  local f = fs.open(filepath, "r")
  write(f.readAll())
  f.close()
end

