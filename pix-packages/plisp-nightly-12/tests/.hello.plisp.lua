

-- this just prints hello
print( "Hello, world!");

-- this prints 2 + 2 + 2
print( ( ( 2 + 2 + 2)
-- ... divided by 3
          / 3));
          
print(
  -- look mom
  -- i can have
  "whitespace anywhere"
  -- even here
);