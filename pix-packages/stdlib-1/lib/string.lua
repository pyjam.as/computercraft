function string.insert(str1, str2, pos)
    return str1:sub(1,pos)..str2..str1:sub(pos+1)
end

function string.removeAt(str, pos)
  return str:sub(1,pos-1)..str:sub(pos+1, -1)
end

function string.removeRange(str, pos1, pos2)
  return str:sub(1,pos1-1)..str:sub(pos2, -1)
end

function string.charAt(str, pos)
  return str:sub(pos, pos)
end

function string.splitAt(str, pos)
  return str:sub(1,pos-1), str:sub(pos,-1)
end

function string:split(delimiter)
  local result = { }
  local from  = 1
  local delim_from, delim_to = string.find( self, delimiter, from  )
  while delim_from do
    table.insert( result, string.sub( self, from , delim_from-1 ) )
    from  = delim_to + 1
    delim_from, delim_to = string.find( self, delimiter, from  )
  end
  table.insert( result, string.sub( self, from  ) )
  return result
end

function string.findAll(str, regexes)
  local t = {}                   -- table to store the indices
  local i = 0
  while true do
    i = string.find(s, "\n", i+1)    -- find 'next' newline
    if i == nil then break end
    table.insert(t, i)
  end
  return t
end

function table.keys(list)
  local keys = {}
  for k, _ in pairs(list) do
    table.insert(keys, k)
  end
  return keys
end

function table.contains(list, x)
  for _, v in pairs(list) do
    if v == x then return true end
  end
  return false
end

function table.indexOf(list, value)
    for i, v in ipairs(list) do
        if v == value then
            return i
        end
    end
    return nil
end
