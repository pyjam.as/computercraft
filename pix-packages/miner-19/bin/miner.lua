pix_require "angle"
pix_require "movement"
local storage = pix_require("@storage-client")

local PROTOCOL = "turtlePositions"

function reportStats(server)
  local x, y, z = gps.locate()
  local fuel = turtle.getFuelLevel()
  rednet.send(server, {type = "report", ores = ores, fuel = fuel, position = {x, y, z}, name = os.getComputerLabel()}, PROTOCOL)
end

function reportDoneChunk(server, chunk)
  rednet.send(server, {type="doneChunk", chunk=chunk, name=os.getComputerLabel()}, PROTOCOL)
end

function requestChunk(server)
  print("Requesting chunk from server...")
  rednet.send(server, {type="requestChunk", name=os.getComputerLabel()}, PROTOCOL)
  local sender, chunk = rednet.receive(PROTOCOL)
  return chunk
end

function movesToCoords(x, y, z)
  -- Make sure to orient(0) before 
  -- calling this
  local moves = {}
  
  local xCur, yCur, zCur = gps.locate()
  local xDiff, yDiff, zDiff = x - xCur, y - yCur, z - zCur
  
  while xDiff > 0 do
    table.insert(moves, "forward")
    xDiff = xDiff - 1
  end
  table.insert(moves, "right")
  while zDiff > 0 do
    table.insert(moves, "forward")
    zDiff = zDiff - 1
  end
  table.insert(moves, "right")
  while xDiff < 0 do
    table.insert(moves, "forward")
    xDiff = xDiff + 1
  end
  table.insert(moves, "right")
  while zDiff < 0 do
    table.insert(moves, "forward")
    zDiff = zDiff + 1
  end
  table.insert(moves, "right")
  while yDiff > 0 do
    table.insert(moves, "up")
    yDiff = yDiff - 1
  end
  while yDiff < 0 do
    table.insert(moves, "down")
    yDiff = yDiff + 1
  end
  return moves
end

function isOre(blockName)
  if not blockName then return false end
  return string.find(blockName, "ore") and not string.find(blockName, "iron")
end

function inventoryFull()
  for i = 1, 16 do
    if turtle.getItemCount(i) == 0 then 
      return false, i 
    end
  end
  return true
end 

function lookForOre() 
  local oreMoves = {}
  for _, move in pairs({"forward", "down", "up"}) do
    local hasBlock, block = inspect(move)
    if hasBlock and isOre(block.name) then
      table.insert(oreMoves, move)
    end
  end
  return oreMoves
end

function mineAll(server)
  -- It's important that back and turn operations are last.
  for _, move in pairs({"forward", "up", "down", "right", "back", "left"}) do
    if move == "right" or move == "back" or move == "left" then
      doMove("right")
      move = "forward"
    end
    local hasBlock, block = inspect(move)
    local mine = hasBlock and isOre(block.name)
    if mine then
      doMove(move)
      reportStats(server)
      mineAll(server)
      doMove(inverses[move])
    end
  end
  doMove("right")
end

function chunkCoords(x, z)
  return x * 16, z * 16
end

function movesToChunk(xChunk, zChunk)
  x, z = chunkCoords(xChunk, zChunk)
  _, y, _ = gps.locate()
  return movesToCoords(x,y,z)
end


function addLayerMoves(moves)
  for y=1,8 do
    for x=1,15 do  
      table.insert(moves, "forward")
    end
    local turn = "right"
    if y % 2 == 0 then turn = "left" end
    if y == 8 then 
      table.insert(moves, "right")
      for i=1,14 do 
        table.insert(moves, "forward")
      end
      table.insert(moves, "right")
    else
      toNextRow = {turn, "forward", "forward", turn}
      for _, move in pairs(toNextRow) do
        table.insert(moves, move)
      end
    end
  end
end

local BLACKLIST = {
  "cobbled_deepslate",
  "deepslate",
  "cobblestone",
  "andesite",
  "diorite",
  "granite",
  "gravel",
  "dirt",
  "tuff"
}

function isJunk(name)
  for _, black in pairs(BLACKLIST) do
    if "minecraft:"..black == name then 
      return true
    end
  end
  return false
end

function dropJunk()
  for i=1,16 do 
    local item = turtle.getItemDetail(i)
    if item and isJunk(item.name) then
      turtle.select(i)
      turtle.dropDown()
    end
  end
end

local LAYERS = 14

function waitForEmpty()
  while not checkInventorySpace do
    print("Sleeping!")
    os.sleep(30)
  end
end

function saveProgress(index, moves, angle, chunk)
  local x,y,z = gps.locate()
  if not x or not y or not z then 
    print("No GPS signal, skipping save.")
    return
  end
  local file = fs.open("progress", "w")
  file.writeLine(index)
  file.writeLine(angle)
  file.writeLine(("%d %d %d"):format(x, y, z))
  file.writeLine(("%d %d"):format(chunk.x, chunk.z))
  for _, move in pairs(moves) do
    file.writeLine(move)
  end
  file.close()
end

function loadProgress()
  if not fs.exists("progress") then return 1, {}, nil, 0 end
  local file = fs.open("progress", "r")
  local moves = {}
  local index = tonumber(file.readLine())
  local angle = tonumber(file.readLine())
  local x_s, y_s, z_s = string.match(file.readLine(), "(-?%d+) (-?%d+) (-?%d+)")
  local pos = {x=tonumber(x_s),y=tonumber(y_s),z=tonumber(z_s)}
  local chunk_x_s, chunk_z_s = string.match(file.readLine(), "(-?%d+) (-?%d+)")
  local chunk = {x=tonumber(chunk_x_s),z=tonumber(chunk_z_s)}
  while true do
    local line = file.readLine()
    if not line then break end
    table.insert(moves, line)
  end
  file.close()
  return index, moves, pos, angle, chunk
end

local function connect()
  -- make rednet connection
  rednet.open("left")
  local serverIp = nil
  local retries = 0
  repeat
    serverIp = rednet.lookup(PROTOCOL, "server")
    if not serverIp then
      local secs = math.pow(2, retries)
      print("Could not reach server, retrying in " .. secs .. " seconds")
      os.sleep(secs)
      retries = retries + 1
      -- Exponential backoff ;)
    end
  until serverIp
  
  return serverIp 
end

function find_shulker()
  -- Make sure we have a shulker box in slot 1
  for i=1,16 do
    local details = turtle.getItemDetail(i)
    if details and string.find(details.name, "shulker_box") then
      return i
    end
  end
  return nil
end

function dump_to_shulker()
  local shulker = find_shulker()
  -- make sure there is empty space
  -- in front of us
  doMove("forward")
  doMove("back")
  
  turtle.select(shulker)
  turtle.place()
  for i=1,16 do
    turtle.select(i)
    turtle.drop()
  end
  turtle.dig()
end

-- Turn on modem and find own id
function modem_connect()
  local modem = peripheral.wrap("top")
  if not modem then
    error("Is not underm modem")
  end
  local shulker = find_shulker()
  turtle.select(shulker)
  local nameLocal = modem.getNameLocal()
  if not nameLocal then
    turtle.placeUp()
    nameLocal = modem.getNameLocal()
    os.sleep(0.5)
  end
  return nameLocal
end

-- Assumes we are standing under a network
-- connected wired modem
function refuel()
  print("Refuelling..")
  local inventory = modem_connect()
  local _, empty_slot = inventoryFull()
  if not empty_slot then
    error("Cannot refuel with full inventory")
  end
  turtle.select(empty_slot) 
  while turtle.getFuelLevel() ~= turtle.getFuelLimit() do
    local amount = storage.get("lava_bucket", 1, inventory, empty_slot)
    if amount ~= 1 then
      print("Didn't get lava from storage. Are we out?")
      os.sleep(5)
    end
    turtle.refuel()
    storage.dump(empty_slot, inventory)
  end
end

function dump_to_storage()
  print("Dumping to storage..")
  local shulker = find_shulker()
  local inventory = modem_connect()
  turtle.select(shulker)
  turtle.place()
  while true do
    repeat
      local got_any = turtle.suck()
    until not got_any
    local had_stuff = false
    for i=1, 16 do
      if turtle.getItemCount(i) ~= 0 then 
        storage.dump(i, inventory)
        had_stuff = true
      end
    end
    if not had_stuff then break end
  end
  turtle.dig()
end

function main()
  -- get saved list of moves
  local shulker = find_shulker()
  if not shulker then 
    print("Shulker box missing, please place a shulker box in my inventory")
    error()
  end
  local index, moves, pos, angle, chunk = loadProgress()
  local server = connect()
  
  if #moves == 0 then  
    -- genereate mission ( list of moves )
    refuel()
    -- Leave booth
    chunk = requestChunk(server)
    print("Starting mission for chunk ".. chunk.x .. " " .. chunk.z )
    
    orient(0)
    -- out of booth
    doMove("down")
    doMove("down")
    doMove("forward")
    -- moves = movesToChunk(chunk.x, chunk.z)
    -- for i=1,LAYERS do
    --  addLayerMoves(moves)
    --  for down=1,4 do
    --    table.insert(moves, "down")
    --  end
    -- end
    -- for i=1,LAYERS*4 do
    -- go back up 
    --  table.insert(moves, "up")
    -- end
    -- go back to start
    -- for _, move in pairs(reverseMoves(movesToChunk(chunk.x, chunk.z))) do
    --  table.insert(moves, move)
    -- end
    -- Into booth
    table.insert(moves, "back")
    table.insert(moves, "up")
    table.insert(moves, "up")
    if turtle.getFuelLevel() < #moves+1000 then
      print("Not enough fuel for mission")
      error()
    end
  else
    print("Resuming mission for chunk ".. chunk.x .. " " .. chunk.z )
    print("at index ".. index)
    -- Make sure we are where we left off
    orient(0)
    for _, move in pairs(movesToCoords(pos.x, pos.y, pos.z)) do
      reportStats(server)
      doMove(move)
    end
    -- Orient to the same angle as before we stopped
    orient(angle, 0)
  end 
  
  -- execute the moves
  for i=index, #moves do 
    local move = moves[i]
    if move == "right" then angle = angle + 1 end
    if move == "left" then angle = angle - 1 end
    saveProgress(i, moves, angle, chunk)
    reportStats(server)
    if inventoryFull() then
      dropJunk()
      dump_to_shulker()
    end
    doMove(move)
    local oreMoves = lookForOre()
    if #oreMoves > 0 then
      mineAll(server)
    end
  end
  dump_to_storage()
  print("DONE")
  -- reportDoneChunk(server, chunk) 
  fs.delete("progress")
end

while true do
  main()
  os.sleep(5)
end
