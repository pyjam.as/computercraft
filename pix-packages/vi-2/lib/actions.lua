require "timetravel"

function leading_whitespace(cursor_x, cursor_y)
  local line = buf().data[cursor_y]
  local end_cursor_x, _ = vi__(cursor_x, cursor_y, 1)
  local whitespace  = string.sub(line, 1, end_cursor_x-1)
  return whitespace
end

function vi_insert(move, modifier)
  mode = "insert"
  buf().cursor_x, buf().cursor_y = move(buf().cursor_x, buf().cursor_y, 1)
end

function vi_p(move, modifier)
  vi_pP(move, modifier, 0)
end

function vi_P(move, modifier)
  vi_pP(move, modifier, 1)
end

function vi_pP(move, modifier, caps)
  local cursor_x, cursor_y = buf().cursor_x, buf().cursor_y
  for i=1,modifier do
    if string.find(clipboard, '\n') then
      if caps == 0 then cursor_y = buf().cursor_y+1 end
      lines = string.split(clipboard, "\n")
      for i=#lines-1,1,-1 do
        table.insert(buf().data, cursor_y, lines[i])
      end
      cursor_x = vi__(buf().cursor_x, cursor_y)
    else
      buf().data[buf().cursor_y] = string.insert(buf().data[buf().cursor_y], clipboard, buf().cursor_x-caps)
      buf().cursor_x = buf().cursor_x + string.len(clipboard)
    end
  end
  buf().cursor_x, buf().cursor_y = cursor_x, cursor_y
end

function vi_o(move, modifier)
  local whitespace = leading_whitespace(buf().cursor_x, buf().cursor_y)
  table.insert(buf().data, buf().cursor_y+1, whitespace)
  buf().cursor_y = buf().cursor_y + 1
  vi_insert(move)
end

function vi_O(move, modifier)
  local whitespace = leading_whitespace(buf().cursor_x, buf().cursor_y)
  table.insert(buf().data, buf().cursor_y, whitespace)
  buf().cursor_y = buf().cursor_y
  vi_insert(move)
end


function vi_d(move, modifier)
  vi_cyd(move, modifier, true, nil)
end

function vi_y(move, modifier)
  vi_cyd(move, modifier, nil, nil)
end

function vi_c(move, modifier)
  vi_cyd(move, modifier, true, true)
end

function vi_tilde(move, modifier)
    local cursor_x, cursor_y = buf().cursor_x, buf().cursor_y
    for i=1,modifier do
      local line = buf().data[cursor_y]
      local char = string.charAt(line, cursor_x)
      local is_lower = string.find(char, "[a-z]")
      local modify
      if is_lower then modify = string.upper end
      if not is_lower then modify = string.lower end
      local part1 = line:sub(1, cursor_x-1)
      local part2 = line:sub(cursor_x+1, line:len()+1)
      buf().data[cursor_y] = part1 .. modify(char) .. part2
      cursor_x, cursor_y = move(cursor_x, cursor_y)
    end
    buf().cursor_x, buf().cursor_y = cursor_x, cursor_y
end

function vi_r(char)
  return function(move, modifier) 
    local cursor_x, cursor_y = buf().cursor_x, buf().cursor_y
    for i=1,modifier do
      local line = buf().data[cursor_y]
      local part1 = line:sub(1, cursor_x-1)
      local part2 = line:sub(cursor_x+1, line:len()+1)
      buf().data[cursor_y] = part1 .. char .. part2
      cursor_x = lim_x(cursor_x + 1, cursor_y)
    end
  end
end
 
function vi_x(move, modifier)
  clipboard = ""
  for i=1,modifier do
    clipboard = clipboard..string.sub(buf().data[buf().cursor_y], buf().cursor_x, buf().cursor_x)

    buf().data[buf().cursor_y] = string.removeAt(buf().data[buf().cursor_y], buf().cursor_x)
    buf().cursor_x = lim_x(move(buf().cursor_x, buf().cursor_y))
  end
end

function vi_cyd(move, modifier, delete, insert)
  -- move cursor by move modifier times
  local cursor_x, cursor_y = buf().cursor_x, buf().cursor_y
  local copy_cursor_x, copy_cursor_y = cursor_x, cursor_y
  local in_place = false
  clipboard = ""
  for i=1,modifier do
    cursor_x, cursor_y, _, in_place = move(cursor_x, cursor_y, 1)
  end
  local y_diff = cursor_y-buf().cursor_y 
  local x_diff = cursor_x-buf().cursor_x
  local remove = {} -- might actually just yank
  if in_place then
    -- in place like dd handling
    if modifier == 1 then
      table.insert(remove, buf().cursor_y)
    else
      y_diff = math.min(modifier-1, #buf().data-buf().cursor_y)
      cursor_y = buf().cursor_y+y_diff
    end
  end
  dir = y_diff/math.abs(y_diff)
  if math.abs(y_diff) > 0 then
    for y=buf().cursor_y,cursor_y,dir do
      if insert and y == cursor_y then
        buf().data[y] = ""
      else
        table.insert(remove, y)
      end
    end
  elseif x_diff ~= 0 then
    clipboard = string.sub(buf().data[buf().cursor_y], math.min(buf().cursor_x, cursor_x), math.max(buf().cursor_x, cursor_x-1))
    if delete then
      buf().data[buf().cursor_y] = string.removeRange(buf().data[buf().cursor_y], math.min(buf().cursor_x, cursor_x), math.max(buf().cursor_x, cursor_x))
    end
  end
  table.sort(remove)
  for i=#remove,1,-1 do
    clipboard = buf().data[remove[i]]..'\n'..clipboard
    if delete then 
      if #buf().data > 1 then table.remove(buf().data, remove[i]) else buf().data[1] = "" end
    end
  end
  buf().cursor_y = lim_y(math.min(buf().cursor_y, cursor_y))
  if delete and #remove > 0 then 
    buf().cursor_x = vi__(buf().cursor_x, buf().cursor_y)
  else
    buf().cursor_x = math.min(buf().cursor_x, cursor_x), buf().cursor_y
  end
  if insert then mode = "insert" end
end
function vi_J(move, modifier)
  for i=1,modifier do 
    if buf().cursor_y == #buf().data then break end
    local after_whitespace = vi__(buf().cursor_x, buf().cursor_y+1) 
    line_after = buf().data[buf().cursor_y+1]:sub(after_whitespace, -1)
    buf().data[buf().cursor_y] = buf().data[buf().cursor_y] .. " " .. line_after
    table.remove(buf().data, buf().cursor_y+1)
  end
end

function vi_lt(move, modifier)
  vi_indent(move, modifier, true)
end

function vi_gt(move, modifier)
  vi_indent(move, modifier, false)
end

function vi_indent(move, modifier, unindent)
  local cursor_x, cursor_y = buf().cursor_x, buf().cursor_y
  for i=1,modifier do
    cursor_x, cursor_y, _, in_place = move(cursor_x, cursor_y)
  end
  local y_diff = cursor_y-buf().cursor_y 
  local x_diff = cursor_x-buf().cursor_x
  if y_diff == 0 then
    y_diff = math.min(modifier-1, #buf().data-buf().cursor_y)
    cursor_y = buf().cursor_y+y_diff
  end
  dir = y_diff/math.abs(y_diff)
  if y_diff == 0 then dir = 1 end
  for y=buf().cursor_y,cursor_y,dir do
    if unindent then
      if string.sub(buf().data[y], 1, 2) == "  " then
        buf().data[y] = string.removeRange(buf().data[y], 1, 3)
      end
    else
      buf().data[y] = "  "..buf().data[y]
    end
  end
  buf().cursor_y = math.min(buf().cursor_y, cursor_y)
  buf().cursor_x = vi__(cursor_x, buf().cursor_y)
end

function write_file(filename)
  local file = fs.open(filename, "w")
  for _, line in pairs(buf().data) do
    file.writeLine(line)
  end
  file.close()
  status = string.format("\"%s\" %dL, written", filename, #buf().data)
end


function write_file(name)
  local file = fs.open(name, "w")
  for _, line in pairs(buffers[name].data) do
    file.writeLine(line)
  end
  file.close()
  status = string.format("\"%s\" %dL, written", filename, #buffers[name].data)
end

function read_file(name)
  buffers[name] = TimeMachine.new({}, 1, 1, 1)
  if name and name ~= "" and fs.exists(name) then
    local file = fs.open(name, "r")
    while true do
      local line = file.readLine()
      if not line then break end
      buffers[name].data[#buffers[name].data + 1] = line
    end
    file.close()
  end
  if #buffers[name].data == 0 then table.insert(buffers[name].data, "") end
end
