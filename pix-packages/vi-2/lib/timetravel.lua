-- A simply mechanism for implementing undo and redo with an unbranching
-- history ie. if you undo and then make a new transaction you cannot redo and
-- get "forwards" to your old state anymore.
--
-- TimeMachine.new -- create a new TimeMachine object
-- TimeMachine:transaction -- call this before making undo-able changes
-- TimeMachine:undo -- go back to previous transaction
-- TimeMachine:redo -- go forwards to next transaction
--
-- Usage:
--   t = TimeMachine.new()
--   t.data[0] = "Hello"
--   t.data[1] = "world!"
--   t:transaction()
--   t.data[1] = "darkness"
--   -- t.data now holds {"Hello", "darkness"}
--   t = t:undo()
--   -- t.data now holds {"Hello", "world!"}
--   t = t:redo()
--   -- t.data now holds {"Hello", "darkness"}
--   t = t:redo()
--   -- t.data still holds {"Hello", "darkness"}, there was no newer revision


TimeMachine = {}
-- metamethod that defines where to look when a field isn't defined
TimeMachine.__index = TimeMachine


function TimeMachine.new(data, cursor_x, max_cursor_x, cursor_y)
    local object = {}
    setmetatable(object, TimeMachine)
    object.data = {}
    for key, value in pairs(data) do
        object.data[key] = value
    end
    object.max_cursor_x = max_cursor_x
    object.cursor_x = cursor_x
    object.cursor_y = cursor_y
    return object
end


function TimeMachine:snapshot()
    local new_state = TimeMachine.new(self.data, self.cursor_x, self.max_cursor_x, self.cursor_y) 

    self.forwards = new_state
    new_state.backwards = self

    return new_state
end


function TimeMachine:undo()
    return self.backwards or self
end


function TimeMachine:redo()
    return self.forwards or self
end
