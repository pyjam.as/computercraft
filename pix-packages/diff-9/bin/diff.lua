pix_require "init"
pix_require "@stdlib"

function diff_cli(file_a, file_b)
  local files = {file_a, file_b}
  local data = {}
  for _, file in ipairs(files) do
    local path = shell.resolve(file)
    if not fs.exists(path) then
      print(file.." does not exist")
      error()
    elseif fs.isDir(path) then
      print(file.." is a directory")
      error()
    end
    local f = fs.open(path, "r")
    local lines = string.split(f.readAll(), "\n")
    f.close()
    table.insert(data, lines)
  end
  print_diff(diff(data[1], data[2]))
end

if arg[1] and arg[2] then
  diff_cli(arg[1], arg[2])
else
  print("Usage: diff [FILE] [FILE]")
end
