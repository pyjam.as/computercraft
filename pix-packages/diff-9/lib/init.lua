pix_require "@stdlib"
pix_require "@libpager"

-- Special datastructure
-- Running diff on two list, a and b we get
-- {
--   {false, 1}
--   {false, 2}
--   {1, 3} exists in both lists, at a[1] and b[3]  
--   {false, 4} only exists in b at b[4]
--   {2, false} only exists in a at a[2]
-- }

local function append(list, element)
  return {previous=list, element=element} 
end

function lcs_diff(X, Y)
  local cache = {}
  local start = {}
  while true do
    local index = #start+1
    if not X[index] or not Y[index] then
      break
    elseif X[index] == Y[index] then
      table.insert(start, {index,index})
    else
      break
    end
  end
  local function LCS(i, j) 
    local cache_key = i..","..j
    local list, matches
    if cache[cache_key] then
      local cache_entry = cache[cache_key]
      list = cache_entry.list
      matches = cache_entry.matches
    elseif i == #start and j == #start then
      list = nil
      matches = 0
    elseif i == #start then
      list, matches = LCS(i, j-1)
      list = append(list, {false, j})
    elseif j == #start then
      list, matches = LCS(i-1, j)
      list = append(list, {i, false})
    elseif X[i] == Y[j] then 
      list, matches = LCS(i-1, j-1)
      matches = matches + 1
      list = append(list, {i, j})
    else
      local a, m_a = LCS(i-1, j)
      local b, m_b = LCS(i, j-1) 
      if m_a > m_b then
        list = append(a, {i, false})
        matches = m_a
      else
        list = append(b, {false, j})
        matches = m_b
      end
    end
    
    local result = {list=list, matches=matches}
    cache[cache_key] = result
    return result.list, result.matches
  end
  -- Flatten result
  local element, matches = LCS(#X, #Y)
  local result = {}
  while true do
    if not element then break end
    table.insert(result, 1, element.element) 
    element = element.previous
  end
  return table.merge(start, result), matches+#start
end

function diff(a, b)
  local diff = lcs_diff(a, b)
  local changed = {}
  local result = ""
  local changes = {}
  for i=1, #diff do
    if not diff[i][1] or not diff[i][2] then
      table.insert(changes, i)
    end
  end
  local groups = {}
  local group = {}
  for _, change in ipairs(changes) do
    if change-(group[#group] or change) <= 6 then
      table.insert(group, change)
    else
      table.insert(groups, group)
      group = {change}
    end
  end
  table.insert(groups, group)
  for _, group in ipairs(groups) do
    -- Disallow empty groups. should not happen
    if #group == 0 then break end
    -- print group header
    -- find something common between groups
    local index_a, index_b
    local first = math.max(1, group[1]-3)
    if first == 1 then
      index_a, index_b = 1, 1  
    else
      index_a, index_b = diff[first][1], diff[first][2]
    end
    result = result.."@@ "..index_a..", "..index_b.." @@".."\n"
    -- print lines
    for i=first, math.min(#diff, group[#group]+3) do
      local x, y = diff[i][1], diff[i][2]
      if x and y then 
        result = result.." "..a[x].."\n"
      elseif x then
        result = result.."-"..a[x].."\n"
      elseif y then
        result = result.."+"..b[y].."\n"
      end
    end
  end
  return result
end

local color_map = {
  ["@"] = colors.cyan,
  ["+"] = colors.green,
  ["-"] = colors.red,
  ["#"] = colors.lightBlue
}

function print_diff(text)
  local rows = {}
  for _, line in ipairs(string.split(text, "\n")) do
    local first = line:sub(1,1)
    local row = {}
    if color_map[first] then
      table.insert(row, color_map[first])
    else
      table.insert(row, colors.white)
    end
    table.insert(row, line)
    table.insert(rows, row)
  end
  pager(rows)
end
