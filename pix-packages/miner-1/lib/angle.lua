pix_require "movement"

function getAngle()
  -- Get angle in half pi's
  local x1, _, z1 = gps.locate()
  doMove("forward")
  local x2, _, z2 = gps.locate()
  doMove("back")
  return math.atan2(z2-z1, x2-x1) / (math.pi/2)
end

function diff(a, b)
  local diff = (a - b + 2) % 4 - 2
  if diff < -2 then
    return diff + 4
  end
  return diff
end

function orient(toAngle, fromAngle)
  fromAngle = fromAngle or getAngle()
  local rotations = diff(fromAngle, toAngle)
  if rotations < 0 then
    doMove("right", math.abs(rotations))
  elseif rotations > 0 then
    doMove("left", math.abs(rotations))
  end
end
