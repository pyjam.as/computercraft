-- regex `match(pattern, text)` function.
-- supports:
--   . (any character)
--   * (zero or more occurences)
--   $ (end of text)
--   ^ (beginning of text)

-- todo +, ? and escaping \


function match_star(char, pattern, text, offset)
  for i = offset, #text do
    local to = match_here(pattern, text, i)
    if to then
      return to
    end

    local next_char = text:sub(i+1, i+1)
    if text:sub(i, i) ~= char and char ~= "." then
      break
    end
  end
  return nil
end


function match_here(pattern, text, offset)
  -- match beginning of text
  --print(pattern, text, offset)
  if #pattern == 0 then
    return offset - 1
  elseif pattern:sub(2, 2) == "*" then
    return match_star(pattern:sub(1, 1), pattern:sub(3, -1), text, offset)
  elseif #pattern == 1 and pattern:sub(1, 1) == "$" then
    return offset -- - 1?
  elseif (pattern:sub(1, 1) == "." or pattern:sub(1, 1) == text:sub(offset, offset)) then
    return match_here(pattern:sub(2, -1), text, offset + 1)
  else
    return nil
  end
end


function match(pattern, text)
  -- return true if text is matched by pattern
  -- use ^pattern$ to match entire line
  if pattern:sub(1, 1) == "^" then
    a = match_here(pattern:sub(2, -1), text, 1)
    if a then
      return true
    else
      return false
    end
  end

  for i = 1, #text do
    if match_here(pattern, text, i) then
      return true
    end
  end

  return false
end


function find_all(pattern, text)
  matches = {}

  if pattern:sub(1, 1) == "^" then
    if match_here(pattern:sub(2, -1), text, 1) then
      table.insert(matches, { from = 1, to = #text })
    end
    return matches
  end

  local skip_until = 0
  for i = 1, #text do
    if i > skip_until then
      to = match_here(pattern, text, i)
      if to then
        skip_until = to
        table.insert(matches, { from = i, to = to })
      end
    end
  end

  return matches
end
