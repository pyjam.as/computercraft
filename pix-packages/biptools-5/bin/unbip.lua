pix_require "@biplib"

local bipfile = arg[1]
if not bipfile then 
  print("Usage: unbip <bipfile>")
  error()
end

if bipfile:sub(1,1) ~= "/" then
  bipfile = fs.combine(shell.dir(), arg[1])
end

if not fs.exists(bipfile) then
  print(bipfile .. " doesn't exist")
  error()
end

local f = fs.open(bipfile, "rb")
local bipdata = f.readAll()
f.close()
print("Unbipping...")
success, err = unbip(bipdata, shell.dir())
if success then
  print("Done!")
else
  print("Error: "..err)
end
