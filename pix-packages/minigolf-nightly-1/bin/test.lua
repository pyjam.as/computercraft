

local fp  = pix_require("@libfunc");
local std  = pix_require("@stdlib");
local im  = pix_require("@immutable");

local WIDTH, HEIGHT  = term.getSize();

local function draw_pixel (x, y, color)
  term.setCursorPos(x, y);
  term.setBackgroundColor(color);
  write(" ");
  return term.setBackgroundColor(colors.black)
 end;

local function draw_column (x, y1, y2, c)
  return (function()if (y1 == y2) then 
      return nil else 
      return (function (...) -- `do` would be nice
        draw_pixel(x, y1, (c or colors.red));
        return draw_column(x, (1 + y1), y2, c)
       end)()
   end end)()
 end;

local maxbarh  = HEIGHT;
local function draw_bar (barh, c)
  local top  = (1 + (maxbarh - barh));
  local bottom  = (1 + HEIGHT);
  return draw_column(2, top, bottom, c)
 end;

local function draw_background (...)
  term.setBackgroundColor(colors.green);
  return term.clear()
 end;

local function dist (x1, y1, x2, y2)
  return math.abs(math.sqrt((((y2 - y1) ^ 2)
                          + ((x2 - x1) ^ 2)))) end;

function line (x1, y1, x2, y2, acc)
  local dx  = (x2 - x1);
  local dy  = (y2 - y1);
  local stepx  = (dx / math.abs(dx));
  local stepy  = (dy / math.abs(dy));
  local dista  = dist(x1, y1, x2, y2);
  local distb  = dist((x1 + stepx), (y1 + stepy),
                   x2, y2);
  return (function()if (dista < distb) then 
    return acc else  -- return once we're not getting closer
    return line((x1 + stepx),
          (y1 + stepy),
          x2,
          y2,
          im.prepend(acc, {x1,y1})
    ) -- hopefully maybe tail recursive
   end end)()
 end;

local function draw (state)
  -- draw mouse
  (function()if state.mouse then 
    return draw_pixel(state.mouse[1], state.mouse[2], colors.blue) else 
    return nil
   end end)();
  (function()if state.mousedown then 
    return draw_pixel(state.mousedown[1], state.mousedown[2], colors.black) else 
    return nil
   end end)();
  -- clear one above
  draw_bar((1 + state.barh), colors.green);
  -- draw new red bar
  return draw_bar(state.barh, colors.red)
 end;

local function step (state, msg)
  -- set bar height
  state.barh
     = (function()if (msg.event == "tick") then 
      return (state.barh - 0) else 
      return (function()if (msg.event == "click") then 
        return 20 else 
        return state.barh
       end end)()
     end end)()
  ;
  -- set mouse pos
  state.mouse
     = (function()if ((msg.event == "click")
            or (msg.event == "drag")
        ) then 
        return msg else 
        return (function()if (msg.event == "unclick") then 
            return nil else 
            return state.mouse
         end end)()
     end end)()
  ;
  state.mousedown 
     = (function()if (msg.event == "click") then 
        return msg else 
        return (function()if (msg.event == "unclick") then 
            return nil else 
            return state.mousedown
         end end)()
     end end)()
  ;
  state.done  = (state.barh == 1);
  return state -- immutable table operations would be nice
 end;

local LEFT_BUTTON  = 1;
local function wait_for_click (...)
  local event, button, x, y  = os.pullEvent("mouse_click");
  return (function()if (button == LEFT_BUTTON) then 
      return {x,y,event="click"} else 
      return wait_for_click()
   end end)()
 end;

local function wait_for_unclick (...)
  local event, button, x, y  = os.pullEvent("mouse_up");
  return (function()if (button == LEFT_BUTTON) then 
      return {x,y,event="unclick"} else 
      return wait_for_unclick()
   end end)()
 end;

local function wait_for_drag (...)
  local event, button, x, y  = os.pullEvent("mouse_drag");
  return (function()if (button == LEFT_BUTTON) then 
      return {x,y,event="drag"} else 
      return wait_for_drag()
   end end)()
 end;

local function wait_for_tick (...)
  os.startTimer(0.5);
  local event  = os.pullEvent("timer");
  return {event="tick"}
 end;

local function wait_for_any (...)
  local result  = nil;
  local wrapped  = fp.map(
    {...},
    (function (x) return (function (_) result  = x(); return nil end) end)
  );
  parallel.waitForAny(unpack(wrapped));
  return result
 end;

local function wait_for_event (...)
  return wait_for_any(
    wait_for_click,
    wait_for_tick,
    wait_for_drag
  )
 end;

draw_background();

-- hmm it's difficult to do `{os.pullEvent()}`

local function run (f, state)
  return f(state,
     wait_for_event(),
     (function (newstate) return run(f, newstate) end)
  )
 end;

local function rungame (...)
  return run(
    (function (state, event, cnt)
      local newstate  = step(state, event);
      draw(newstate);
      return (function()if newstate.done then 
        return nil else 
        return cnt(newstate)
       end end)()
     end),
    {barh=20}
  )
 end;