
local fp  = pix_require("@libfunc");
local std  = pix_require("@stdlib");
local im  = pix_require("@immutable");

local WIDTH, HEIGHT  = term.getSize();

local function draw_pixel (x, y, color)
  term.setCursorPos(x, y);
  term.setBackgroundColor(color);
  write(" ");
  return term.setBackgroundColor(colors.black)
 end;

local function draw_column (x, y1, y2, c)
  return (function()if (y1 == y2) then 
      return nil else 
      return (function (...) -- `do` would be nice
        draw_pixel(x, y1, (c or colors.red));
        return draw_column(x, (1 + y1), y2, c)
       end)()
   end end)()
 end;

local maxbarh  = HEIGHT;
local function draw_bar (barh, c)
  local top  = (1 + (maxbarh - barh));
  local bottom  = (1 + HEIGHT);
  return draw_column(2, top, bottom, c)
 end;

local function draw_background (...)
  term.setBackgroundColor(colors.green);
  return term.clear()
 end;

local function dist (x1, y1, x2, y2)
  return math.abs(math.sqrt((((y2 - y1) ^ 2)
                          + ((x2 - x1) ^ 2)))) end;

local function round (x)
  return math.floor((0.5 + x)) end;

local steplen  = 1;
function line (x1, y1, x2, y2, acc)
  local d  = dist(x1, y1, x2, y2);
  local nsteps  = (d / steplen);
  local stepx  = ((x2 - x1) / nsteps);
  local stepy  = ((y2 - y1) / nsteps);
  local floatline 
     = fp.map(
      std.table.range(1, nsteps),
      (function (i) 
        local x  = (x1 + (i * stepx));
        local y  = (y1 + (i * stepy));
        return {x,y} end));
  -- round
  local intline 
     = fp.map(floatline, (function (p) return fp.map(p, round) end));
  -- remove duplicates
  return fp.fold(
    intline,
    (function (acc, e) 
      local last  = acc[#acc];
      (function()if (last
               and (last[1] == e[1])
               and (last[2] == e[2])) then 
        return nil else 
        return table.insert(acc, e) end end)();
      return acc end),
    {intline[1]}) end;

local function dbg (x)
  term.setCursorPos(0, 0);
  std.pprint(x);
  return x end;

local function colorfun (c)
  return (function (p) return draw_pixel(p[1], p[2], c) end) end;

local function draw (state)
  -- draw mouse
  return (function()if state.mouseline then 
    return (function (_) -- `do`
      fp.map(state.oldline, colorfun(colors.green));
      return fp.map(state.mouseline, colorfun(colors.blue))
     end)() else 
    return nil end end)() end;

local function step (state, msg)
  local mouse  = (function()if ((msg.event == "click")
                     or (msg.event == "drag")) then 
               return msg else 
               return (function()if (msg.event == "unclick") then 
                 return nil else 
                 return state.mouse end end)() end end)();
                 
  local click  = (function()if (msg.event == "click") then 
               return msg else 
               return (function()if (msg.event == "unclick") then 
                 return nil else 
                 return state.click end end)() end end)();
  local mouseline
     = (function()if (click and mouse) then 
      return line(click[1], click[2],
            mouse[1], mouse[2]) else 
      return nil end end)();
  local oldline
     = fp.filter(
      (state.mouseline or mouseline or {}),
      (function (e)
        -- get distinct
        return (not fp.any(
               (mouseline or {}),
               (function (p) return ((p[1] == e[1])
                            and (p[2] == e[2])) end))) end)
  );
  return im.set(state,
    "mouse", mouse,
    "click", click,
    "mouseline", mouseline,
    "oldline", oldline,
    "done", false
  )
 end;

local LEFT_BUTTON  = 1;
local function wait_for_click (...)
  local event, button, x, y  = os.pullEvent("mouse_click");
  return (function()if (button == LEFT_BUTTON) then 
      return {x,y,event="click"} else 
      return wait_for_click()
   end end)()
 end;

local function wait_for_unclick (...)
  local event, button, x, y  = os.pullEvent("mouse_up");
  return (function()if (button == LEFT_BUTTON) then 
      return {x,y,event="unclick"} else 
      return wait_for_unclick()
   end end)()
 end;

local function wait_for_drag (...)
  local event, button, x, y  = os.pullEvent("mouse_drag");
  return (function()if (button == LEFT_BUTTON) then 
      return {x,y,event="drag"} else 
      return wait_for_drag()
   end end)()
 end;

local function wait_for_tick (...)
  os.startTimer(0.02);
  local event  = os.pullEvent("timer");
  return {event="tick"}
 end;

local function wait_for_any (...)
  local result  = nil;
  local wrapped  = fp.map(
    {...},
    (function (x) return (function (_) result  = x(); return nil end) end)
  );
  parallel.waitForAny(unpack(wrapped));
  return result end;

local function wait_for_event (...)
  return wait_for_any(
    wait_for_click,
    wait_for_tick,
    wait_for_drag,
    wait_for_unclick) end;

local function run (f, state)
  return f(state,
     wait_for_event(),
     (function (newstate) return run(f, newstate) end)
  )
 end;

local function rungame (...)
  draw_background();
  return run(
    (function (state, event, cnt)
      local newstate  = step(state, event);
      draw(newstate);
      return (function()if newstate.done then 
        return nil else 
        return cnt(newstate)
       end end)()
     end),
    {}
  )
 end;

line(10, 10, 0, 0);
rungame();