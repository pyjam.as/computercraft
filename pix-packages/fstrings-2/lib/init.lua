local pretty = require("cc.pretty")

-- Input: format string
-- Output: {{string="123"}|{var="name"}, ... }
local function parse(format_string)
  local output = {}
  local index = 1
  
  local string_literal = ""
  while true do
    (function()
      local char = format_string:sub(index,index)
      if char == "{" then
        -- Special character
        index = index + 1
        local next_char = format_string:sub(index,index)
        if next_char == "{" then
          string_literal = string_literal.."{"
          return -- continue
        end
        
        -- This is a format string
        -- Save literal
        if string_literal ~= "" then
          table.insert(output, {string=string_literal})
          string_literal = ""
        end
        -- Collect variable name
        local variable_name = ""
        while true do
          if index > #format_string then
            print('Unclosed "{" in format string "'..format_string..'"')
            error()
          end
          local char = format_string:sub(index,index)
          if char == "}" then
            table.insert(output, {var=variable_name})
            return -- continue
          else
            variable_name = variable_name..char
          end
          
          index = index+1
        end
      elseif char == "}" then
        -- Special character
        index = index + 1
        local next_char = format_string:sub(index,index)
        if next_char == "}" then
          string_literal = string_literal.."}"
        else
          print('fstrings don\'t allow lone "}". Write "}}" to output a literal "}"')
          error()
        end
      else
        string_literal = string_literal..char
      end
    end)()
    index = index+1
    
    if index > #format_string then
      -- Save literal
      if string_literal ~= "" then
        table.insert(output, {string=string_literal})
        string_literal = ""
      end
      return output
    end
  end
end

-- local output = parse("{var1}, {var2}, {var3}, }} {{}}\\{var4}")
-- print(textutils.serialise(output))

-- f"{var1}, {var2}, {var3}, } {{}}\\{var4}"
-- "{var1}, {var2}, {var3}, } {{{}\{var4}"
-- value1, value2, value3, } {}\value4
function f(format_string)
  -- parse
  local parsed = parse(format_string)

  -- figure out which variable we need
  local variables = {}
  for _, part in ipairs(parsed) do
    if part.var then
      variables[part.var] = true
    end
  end
  
  local values = {}
  local i = 1
  while true do
    local name, value = debug.getlocal(2, i)
    if not name then break end
    -- check if we want variable
    if variables[name] then
      values[name] = value
    end
    i = i+1
  end
  
  local formatted_string = ""
  for _, part in ipairs(parsed) do
    if part.string then
      formatted_string = formatted_string..part.string
    else
      local formatted = values[part.var]
      if type(values[part.var]) ~= "string" then
        formatted = pretty.render(pretty.pretty(values[part.var]))
      end
      formatted_string = formatted_string..formatted
    end
  end
  
  return formatted_string
end
