-- concat was taken :shrug
-- note: doesn't deep copy.
function table.merge(...)
  local result = {}
  for _, list in ipairs(arg) do 
    for _, e in ipairs(list) do
      table.insert(result, e) 
    end
  end
  return result
end

function table.keys(list)
  local keys = {}
  for k, _ in pairs(list) do
    table.insert(keys, k)
  end
  return keys
end

function table.contains(list, x)
  for _, v in pairs(list) do
    if v == x then return true end
  end
  return false
end

function table.indexOf(list, value)
    for i, v in ipairs(list) do
        if v == value then
            return i
        end
    end
    return nil
end

function table.clone(orig)
  local orig_type = type(orig)
  local copy
  if orig_type == "table" then
    copy = {}
    for orig_key, orig_value in next, orig, nil do
      copy[table.clone(orig_key)] = table.clone(orig_value)
    end
    setmetatable(copy, table.clone(getmetatable(orig)))
  else
    copy = orig
  end
  return copy
end
