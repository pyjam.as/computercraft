local monitor = peripheral.find("monitor")
monitor.setTextScale(0.5)

local SCREEN_WIDTH, SCREEN_HEIGHT = monitor.getSize()
local GAP = 3
local PIPE_MODULO = 12
local PIPE_WIDTH = 3
local X_OFFSET = 6

local acc_y = -0.1

local pipes = { false }
local pos_x, pos_y, time, move_right_time, prev_y

function gen_pipe()
  table.insert(pipes, math.random(2, 8)*0.1)
end

function yield_noop()
  os.queueEvent("async_noop")
  os.pullEvent("async_noop")
end

function get_time()
  return os.epoch("local") / 1000
end

function write_centered(text, y)
  monitor.setCursorPos((SCREEN_WIDTH+1)/2-text:len()/2, y) 
  monitor.write(text)
end

function game_over()
  local f = fs.open("/highscore", "r")
  local highscore = math.max(textutils.unserialise(f.readAll()) or 0, score())
  f.close()
  f = fs.open("/highscore", "w")
  f.write(textutils.serialise(highscore))
  f.close()
  
  monitor.setTextColor(colors.white)
  monitor.setBackgroundColor(colors.black)
  write_centered("GAME OVER", SCREEN_HEIGHT/2-6)
  write_centered("HIGHSCORE: "..highscore, SCREEN_HEIGHT/2-2)
  write_centered("YOUR SCORE: "..score(), SCREEN_HEIGHT/2)
  write_centered("Press any button to try again", SCREEN_HEIGHT/2+4)
  os.sleep(0.5)
  os.pullEvent("monitor_touch") 
end
function score() 
  return tostring(math.max(0, math.floor((pos_x+X_OFFSET-2) / PIPE_MODULO) - 1))
end

local gameover = true
function game_loop()
  while true do
    if gameover then
      gameover = false
      time = get_time()
      move_right_time = get_time() - 0.3
      pos_x, pos_y = 0, SCREEN_HEIGHT/2
      prev_y = pos_y-1
    end
    -- render pipes
    if get_time() - move_right_time > 0.3 then 
      monitor.setBackgroundColor(colors.lightBlue)
      monitor.clear()
      pos_x = pos_x + 1
      move_right_time = get_time()
      for i=1, SCREEN_WIDTH do
        monitor.setBackgroundColor(colors.brown) 
        monitor.setCursorPos(i, SCREEN_HEIGHT)
        monitor.write("  ")
        if (i + pos_x) % PIPE_MODULO == 0 then
          local pipe_index = math.ceil((i + pos_x) / PIPE_MODULO)
          if not pipes[pipe_index] then gen_pipe() end
          local pipe = pipes[pipe_index]
          for y=1, SCREEN_HEIGHT-1 do
            if pipe and (y > pipe*SCREEN_HEIGHT+GAP or y < pipe*SCREEN_HEIGHT-GAP) then
              monitor.setBackgroundColor(colors.green)
              for w=0, PIPE_WIDTH-1 do
                if y == math.floor(pos_y) and i+w == X_OFFSET then
                  gameover = true
                end
                monitor.setBackgroundColor(colors.green)
                monitor.setCursorPos(i+w, y)
                monitor.write(" ")
              end
            end
          end
        end
      end
    end
    
    -- paint over previous render bird
    if gameover then
      monitor.setBackgroundColor(colors.orange)
    else
      monitor.setBackgroundColor(colors.lightBlue)
    end
    monitor.setCursorPos(X_OFFSET, math.floor(pos_y)) 
    monitor.write(" ")
    
    if pos_y > SCREEN_HEIGHT or pos_y < 1 then gameover = true end
    if gameover then game_over() end
    
    -- Handle physics
    local time_delta = get_time() - time 
    pos_y = pos_y + time_delta * acc_y
    acc_y = acc_y + 0.003
    time = get_time()
    
    -- Render bird
    monitor.setBackgroundColor(colors.orange)
    monitor.setCursorPos(6, math.floor(pos_y)) 
    monitor.write(" ")
    
    
    -- Render score
    monitor.setTextColor(colors.white)
    monitor.setBackgroundColor(colors.black)
    write_centered(score(), 4)
    
    -- async stuff
    yield_noop()
  end
end

function input_loop()
  while true do
    os.pullEvent("monitor_touch")
    acc_y = -15
  end
end

parallel.waitForAll(game_loop, input_loop)
