function string.insert(str1, str2, pos)
    return str1:sub(1,pos)..str2..str1:sub(pos+1)
end

function string.removeAt(str, pos)
  return str:sub(1,pos-1)..str:sub(pos+1, -1)
end

function string.removeRange(str, pos1, pos2)
  return str:sub(1,pos1-1)..str:sub(pos2, -1)
end

function string.charAt(str, pos)
  return str:sub(pos, pos)
end

function string.splitAt(str, pos)
  return str:sub(1,pos-1), str:sub(pos,-1)
end

function string:split(delimiter)
  local result = { }
  local from  = 1
  local delim_from, delim_to = string.find( self, delimiter, from  )
  while delim_from do
    table.insert( result, string.sub( self, from , delim_from-1 ) )
    from  = delim_to + 1
    delim_from, delim_to = string.find( self, delimiter, from  )
  end
  table.insert( result, string.sub( self, from  ) )
  return result
end

function string:join(...)
  local delimiter = self
  
  if #arg == 1 and type(arg[1]) == "table" then
    arg = unpack(arg) 
  end
  if #arg == 0 or #arg[1] == 0 then
    return ""
  end
  for _, v in ipairs(arg) do
    if type(v) ~= "string" then
      print("Error, got",v) 
      print("string:join must be called with a list of strings.")
      print('like this (","):join({"a","b","c"})')
      error()
    end
  end
  local result = arg[1]
  for i=2, #arg do
    result = result..delimiter..arg[i]
  end
  return result
end

function string.findAll(str, regexes)
  local t = {}                   -- table to store the indices
  local i = 0
  while true do
    i = string.find(s, "\n", i+1)    -- find 'next' newline
    if i == nil then break end
    table.insert(t, i)
  end
  return t
end
