-- concat was taken :shrug
-- note: doesn't deep copy.
function table.merge(...)
  local result = {}
  for _, list in ipairs(arg) do 
    for _, e in ipairs(list) do
      table.insert(result, e) 
    end
  end
  return result
end

function table.keys(list)
  local keys = {}
  for k, _ in pairs(list) do
    table.insert(keys, k)
  end
  return keys
end

function table.contains(list, x)
  for _, v in pairs(list) do
    if v == x then return true end
  end
  return false
end

function table.indexOf(list, value)
    for i, v in ipairs(list) do
        if v == value then
            return i
        end
    end
    return nil
end
