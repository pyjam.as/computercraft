-- This file is run by /startup/pix.lua
-- package path is given as first parameter
-- so you don't need to update this file when
-- releasing new version
-- fs.combine (as it removes leading slashes)
local bin_path = package_path .. "/bin"
shell.setPath(shell.path() .. ":" .. bin_path)

-- auto complete
local completion = require "cc.shell.completion"
-- Must be absolute path to the program, but without
-- leading slash :shrug:
local program_path = bin_path:sub(2,-1).."/diff.lua"
shell.setCompletionFunction(
  program_path,
  completion.build(
    completion.file,
    completion.file
  )
)
