local std = {}

std.table = pix_require "table"
std.string = pix_require "string"
std.func = pix_require "func"
std.pprint = require("cc.pretty").pretty_print

return std

