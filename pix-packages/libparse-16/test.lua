local l = require "lib/init"

local pprint = function(x) print(textutils.serialize(x)) end

startloc = {line=1, col=1}
a_parser = l.parse_pattern("a")
b_parser = l.parse_pattern("b")
nl_parser = l.parse_pattern("\n")

-- test parse_pattern
parsed, remaining, loc = a_parser("a aa", startloc)
if remaining ~= " aa" then
  error()
end
if loc.line ~= 1 or loc.col ~= 2 then
  error()
end
if parsed.loc.col ~= 1 or parsed.loc.line ~= 1 then
  error()
end

-- test parse_any
a_or_b_parser = l.parse_any(a_parser, b_parser)
parsed = a_or_b_parser("a", startloc)
if parsed == nil then
  error()
end
parsed = a_or_b_parser("b", startloc)
if parsed == nil then
  error()
end

-- test parse_many
many_ab_parser = l.parse_many(a_or_b_parser)
parsed, remaining = many_ab_parser("aababababbbbaaabbx", startloc)
if remaining ~= "x" then
  error()
end

-- test parse_sequence
paren_parser = l.parse_sequence(
  l.parse_pattern("%("),
  l.parse_pattern("[^%)]+"),
  l.parse_pattern("%)")
)
p, r, loc = paren_parser("(wow)", startloc)
if p == nil then
  error()
end
p, r, loc = paren_parser("()", startloc)
if p ~= nil then
  error()
end

print("passed all tests")
