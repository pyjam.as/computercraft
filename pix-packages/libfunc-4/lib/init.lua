local function fold(list, f, initial)
  local result = initial
  local iter = list
  if initial == nil then
    result = list[1]
    iter = {unpack(list, 2, #list)}
  end
  for _,v in ipairs(iter) do
    result = f(result, v)
  end
  return result
end


local function map(list, f)
  local result = {}
  for k,v in ipairs(list) do
    result[k] = f(v)
  end
  return result
end


local function filter(list, f)
  local result = {}
  for _,v in ipairs(list) do
    if f(v) then
      table.insert(result, v)
    end
  end
  return result
end


local function sort_by(list, f)
  table.sort(
    list,
    function(a, b) return f(a) < f(b) end
  )
  return list
end

local function any(list, f)
  for _,v in ipairs(list) do
    if f(v) then return true end
  end
  return false
end

local function all(list, f)
  for _,v in ipairs(list) do
    if not f(v) then return false end
  end
  return true
end

local function pack(...)
  return {...}
end

local function head(list)
  return list[1]
end

local function tail(list)
  return pack(unpack(list, 2, #list))
end

local function last(list)
  return list[#list]
end

return {
  map = map,
  filter = filter,
  fold = fold,
  sort_by = sort_by,
  any = any,
  all = all,
  pack = pack,
  head = head,
  tail = tail,
  last = last
}
