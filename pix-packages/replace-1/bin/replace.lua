require "/pix/regex-1/regex"

pattern = arg[1]
substitute = arg[2]
filename = arg[3]

if pattern == nil or substitute == nil or filename == nil then
  print("Usage: replace PATTERN SUBSTITUTE FILENAME")
  return
end

lines = {}
f_in = fs.open(shell.resolve(filename), "r")
while true do
  line = f_in.readLine()
  if line == nil then break end
  table.insert(lines, line)
end
f_in.close()

f_out = fs.open(filename, "w")
for _, line in pairs(lines) do
  matches = find_all(pattern, line)

  substituted_line = ""
  cursor = 1

  for _, match in pairs(matches) do
    substituted_line = substituted_line .. line:sub(cursor, match.from - 1) .. substitute
    cursor = match.to + 1
  end
  if cursor < #line then
    substituted_line = substituted_line .. line:sub(cursor, -1)
  end

  f_out.writeLine(substituted_line)
end
f_out.close()
