pix_require "index"

PROTOCOL = "storage"

local latest_index = index()

local chests = {peripheral.find("minecraft:chest")}
local chest_index = 1

function has_empty_slot(inventory) 
  local used_slots = 0
  for _ ,_ in pairs(inventory.list()) do
    used_slots = used_slots + 1 
  end
  return used_slots < inventory.size()
end

function dump(slot, inventory, amount)
  local i = 0
  while not has_empty_slot(chests[chest_index]) do
    if i > #chests then
      print("STORAGE IS FULL!!")
      os.sleep(100000)
      i = 0
    end
    chest_index = chest_index % #chests + 1
    i = i + 1
  end
  local amount = chests[chest_index].pullItems(inventory, slot, amount)
  print("Dumped "..amount.." to " .. peripheral.getName(chests[chest_index]))
  return amount
end

function api()
  rednet.open("left")
  rednet.open("right")
  rednet.host(PROTOCOL, "storagebot")

  print("Listening..")

  local clientId, message = rednet.receive(PROTOCOL)
  local index = latest_index
  
  local result = {}
  if message.action == "list" then
    result.list = list(index)
  elseif message.action == "details" then
    if not message.item then
      result.err = "detail request missing item"
    else 
      result.details = index[message.item]
    end
  elseif message.action == "dump" then 
    if not message.inventory then
      result.err = "dump request missing inventory"
    elseif not message.slot then
      result.err = "dump request missing slot"
    else
      result.amount = dump(message.slot, message.inventory, message.amount)
    end
  elseif message.action == "get" then 
    if not message.item then
      result.err = "get request missing item"
    elseif not message.amount then
      result.err = "get request missing amount"
    elseif not message.inventory then
      result.err = "get request missing inventory"
    else
      local inventory = peripheral.wrap(message.inventory)
      if not inventory then
        result.err = "inventory " .. message.inventory .. " not found"
      else
        result.amount = get(index, message.item, message.amount, inventory, message.to_slot)
      end
    end
  end

  rednet.send(clientId, result, PROTOCOL)
  print("Served Request")
end

function pprint(x)
  print(textutils.serialize(x))
end

function get(index, item_name, amount, to_chest, to_slot) 
  local pushed = 0
  while pushed < amount do
    local item_details = index[item_name]
    for index, data in pairs(item_details) do
      local chest = peripheral.wrap(data.chest)
      local new = chest.pushItems(peripheral.getName(to_chest), data.slot, amount-pushed, to_slot)
      if new == 0 then
        return pushed
      end 
      pushed = pushed + new
      -- Update index
      if data.count == new then
        table.remove(item_details, index)
      else
        data.count = data.count - new
        item_details[index] = data
      end
      if pushed >= amount then break end
    end
  end
  return pushed
end

function list(index)
  local item_names = {}
  for name, _ in pairs(index) do
    table.insert(item_names, name)
  end
  return item_names
end

function api_forever()
  while true do
    api()
  end
end

function index_forever()
  while true do
    os.sleep(60)
    latest_index = index()
  end
end

function dump_trashbins()
  while true do
    noop_yield()
    local bins = {peripheral.find("minecraft:hopper")}
    for _, bin in ipairs(bins) do
      local items = bin.list()
      if #items > 0 then
        for slot, _ in pairs(items) do
          dump(slot, peripheral.getName(bin))
        end
      end
    end
  end
end

parallel.waitForAll(api_forever, index_forever, dump_trashbins)
