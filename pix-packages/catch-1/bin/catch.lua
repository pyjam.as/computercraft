-- catch a yeeted file

local port = arg[1] 
local filename = arg[2]

if port == nil or filename == nil then
  print("catch PORT FILE")
end

port = tonumber(port)

if port == nil or port < 1 or port > 65535 then
  print("PORT must be between 1 and 65535")
end

local modem = peripheral.find("modem")
if modem == nil then
  print("no modem found")
  return
end

modem.open(port)
_, __, ___, ____, message, _____ = os.pullEvent("modem_message")
f = fs.open(shell.resolve(filename), "w")
f.write(message)
f.flush()
f.close()
