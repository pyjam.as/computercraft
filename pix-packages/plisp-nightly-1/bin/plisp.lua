pix_require "@stdlib"
local fp = pix_require "@libfunc"
local codegen = pix_require "codegen"

function stripcomments(plispcode)
  local nocomments = plispcode:gsub("%-%-[^\n]*\n", "\n")
  local noshebang = nocomments:gsub("#![^\n]+\n","")
  return noshebang
end

local function runplisp(plispcode)
  local luacode = codegen.plisp2lua(plispcode)
  local outpath = "/.gen.lua"
  local outfile = fs.open(outpath, "w")
  outfile.write(luacode)
  outfile.close()
  shell.run(outpath)
end

local function runplispfile(plispfilepath)
  local file = fs.open(plispfilepath, "r")
  local filecontent = file.readAll()
  local stripped = stripcomments(filecontent)
  runplisp(stripped)
end

local function main()
  if arg[1] == nil then
    -- TODO repl
    print("you need to provide plisp file")
  elseif fs.exists(arg[1]) then
    runplispfile(arg[1])
  elseif fs.exists(shell.resolve(arg[1])) then
    runplispfile(shell.resolve(arg[1]))
  else
    print("could not find file: " .. arg[1])
    error()
  end
end

main()

return {
  runplisp = runplisp
}
