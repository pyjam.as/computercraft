local parser = {}
local lp = pix_require "@libparse"
local fp = pix_require "@libfunc"

local pprint = function(...) print(textutils.serialize(...)) end

parser.whitespace = lp.parse_pattern("[ \n]+")
parser.comment = lp.parse_pattern("%-%-[^\n]*\n")

-- lol this whole thing could probably have been a pattern
parser.between = lp.parser_map(
  lp.parse_many(lp.parse_any(
    parser.whitespace,
    parser.comment
  )),
  function(thing)
    -- concat strings
    return fp.fold(
      fp.map(thing, function(x) return x.thing end),
      function(a, b) return a .. b end
    )
  end
)

-- any non-nested value
-- e.g. "1", "nil", "a.b.c[24]"
-- basically anything that's not whitespace or parens
parser.primitive = lp.parse_pattern("[^%(%) \n]+")

-- an s-expression is a
-- whitespace/comment-separated, parentheses-delimited,
-- list of expressions
parser.sexpr = function(...)
  local with_parens = lp.parse_sequence(
    lp.parse_pattern("%("),
    lp.parse_many(lp.parse_any(
      parser.between,
      parser.expr
    )),
    lp.parse_pattern("%)")
  )
  local without_parens = lp.ith(with_parens, 2)
  local string_list = lp.parser_map(
    without_parens,
    function(thing) -- why does this end up nested?
      return thing.thing
    end
  )
  return string_list(...)
end

-- an expression is either a primitive, or an s-expression
parser.expr = function(...)
  return lp.parse_any(
    parser.primitive,
    parser.sexpr
  )(...)
end

return parser
