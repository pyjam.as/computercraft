local std = pix_require("@stdlib")
local storage = pix_require("init")

local list = storage.list()
std.pprint(list)

local amount = storage.get("lava_bucket", 1, "turtle_16", 12)
print(amount)
