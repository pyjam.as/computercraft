local lib = {}

local PROTOCOL = "storage"

local server

local function connect()
  peripheral.find("modem", rednet.open)
  server = server or rednet.lookup(PROTOCOL, "storagebot")
  if not server then
    print("Couldn't reach storage server, is a modem connected?")
    error()
  end
  return server
end

-- Get <amount> of <item_name> to <inventory> from storage
function lib.get(item_name, amount, inventory_name, to_slot)
  local server = connect()
  rednet.send(server, { action="get", item=item_name, amount=amount, inventory=inventory_name, to_slot=to_slot }, PROTOCOL)
  _, result = rednet.receive()
  if result.err then
    print(result.err)
    error()
  end
  return result.amount 
end

-- Dump <amount?> items from <slot> in <inventory> to storage
function lib.dump(slot, inventory_name, amount)
  local server = connect()
  rednet.send(server, { action="dump", slot=slot, amount=amount, inventory=inventory_name }, PROTOCOL)
  _, result = rednet.receive()
  if result.err then
    print(result.err)
    error()
  end
  return result.amount 
end

-- List all item names in storage
function lib.list()
  local server = connect()
  rednet.send(server, { action="list" }, PROTOCOL)
  local _, result = rednet.receive()
  if result.err then
    print(result.err)
    error()
  end
  return result.list
end

-- Get details about an item
-- Where it is, and how many
function lib.details(item_name)
  local server = connect()
  rednet.send(server, { action="details", item=item_name }, PROTOCOL)
  local _, result = rednet.receive()
  if result.err then
    print(result.err)
    error()
  end
  return result.details
end

return lib
