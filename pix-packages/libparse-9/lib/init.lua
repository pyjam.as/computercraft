local function parse_many(parserf)
  return function(text)
    local remaining = text
    local results = {}
    while true do
      local parsed, newremaining = parserf(remaining)
      if parsed == nil then
        return results, remaining
      end
      remaining = newremaining
      table.insert(results, parsed)
    end
  end
end

local function parse_any(...)
  local parsers = {...}
  return function(text)
    for _,parserf in pairs(parsers) do
      local parsed, remaining = parserf(text)
      if parsed ~= nil then
        return parsed, remaining
      end
    end
    return nil, text
  end
end

local function parse_pattern(pattern)
  return function(text)
    local patternhere = pattern
    if string.sub(patternhere, 1, 1) ~= "^" then
      patternhere = "^" .. patternhere
    end
    local matched = text:match(patternhere)
    if matched == nil then
      return nil, text
    end
    return matched, string.sub(text, matched:len()+1, text:len())
  end
end

local function parse_sequence(...)
  local parsers = {...}
  return function(text)
    local results = {}
    local remaining = text
    for k, parser in pairs(parsers) do
      local parsed, newremaining = parser(remaining)
      if parsed == nil then
        return nil, text
      end
      remaining = newremaining
      table.insert(results, parsed)
    end
    return results, remaining
  end
end

local function parser_map(p, f)
  return function(text)
    local parsed, remaining = p(text)
    if parsed ~= nil then
      return f(parsed), remaining
    end
    return nil, remaining
  end
end

local function ith(p, i)
  return parser_map(p, function(x) return x[i] end)
end

local function tag(tagname, p)
  return parser_map(
    p,
    function(x)
      if type(x) ~= "table" then
        x = {x}
      end
      x.tag = tagname
      return x
    end
  )
end

local function parse_separated_by(parserf, separatorf)
  return parser_map(
    parse_sequence(
      parserf,
      parse_many(ith(parse_sequence(separatorf, parserf), 2))
    ),
    function(x)
      if #x < 2 then
        return x
      end
      return {x[1], unpack(x[2])}
    end
  )
end

local function parse_between(beforep, innerp, afterp)
  return ith(parse_sequence(beforep, innerp, afterp), 2)
end

local function parse_optional(p)
  return function(text)
    local parsed, remaining = p(text)
    if parsed == nil then
      return "", text
    end
    return parsed, remaining
  end
end

local whitespace_parser = parse_pattern("[ \n]+")

local function parse_with_whitespace(p)
  return parse_between(
    parse_optional(whitespace_parser),
    p,
    parse_optional(whitespace_parser)
  )
end

local int_parser = parser_map(parse_pattern("%d+"), tonumber)
local float_parser = parser_map(parse_pattern("%d+%.%d+"), tonumber)
local number_parser = parse_any(float_parser, int_parser)
local string_parser = parse_between(
  parse_pattern("\""),
  parse_pattern("[^\"]*"),
  parse_pattern("\"")
)
local openparen_parser = parse_with_whitespace(parse_pattern("%("))
local closeparen_parser = parse_with_whitespace(parse_pattern("%)"))
-- TODO identifier parser shouldnt parse keywords
local identifier_parser = parse_pattern("[%a_][%a%d_]*")
local operator_parser = parse_any(
  -- arithmetic operators:
  parse_pattern("%+"),
  parse_pattern("-"),
  parse_pattern("%*"),
  parse_pattern("//"),
  parse_pattern("/"),
  parse_pattern("%%"),
  parse_pattern("%^"),
  -- bitwise operators
  parse_pattern("&"),
  parse_pattern("|"),
  parse_pattern("~"),
  parse_pattern(">>"),
  parse_pattern("<<"),
  -- relational operators
  parse_pattern("=="),
  parse_pattern("~="),
  parse_pattern("<="),
  parse_pattern(">="),
  parse_pattern("<"),
  parse_pattern(">"),
  -- logical operators
  parse_pattern("and"),
  parse_pattern("not"),
  parse_pattern("or"),
  -- concat
  parse_pattern("%.%.")
)
local boolean_parser = parser_map(
  parse_any(
    parse_pattern("true"),
    parse_pattern("false")
  ),
  textutils.unserialize
)
local arglist_parser = parse_any(
  parse_between(
    parse_pattern("%("),
    parse_separated_by(identifier_parser, whitespace_parser),
    parse_pattern("%)")
  ),
  parse_pattern("%(%)")
)
local body_parser = function(text)
  return parse_separated_by(
    expr_parser,
    whitespace_parser
  )(text)
end
local fn_parser = parse_between(
  parse_pattern("%("),
  parse_sequence(
    parse_pattern("fn"),
    parse_between(
      whitespace_parser,
      arglist_parser,
      whitespace_parser
    ),
    body_parser
  ),
  parse_pattern("%)")
)
local parse_defx = function(keyword)
  return function(text)
    return parse_between(
      parse_pattern("%("),
      parse_sequence(
        parse_pattern(keyword),
        parse_between(
          whitespace_parser,
          identifier_parser,
          whitespace_parser
        ),
        expr_parser
      ),
      parse_pattern("%)")
    )(text)
  end
end
local def_parser = parse_defx("def")
local defg_parser = parse_defx("defg")
local if_parser = function(text)
  return parse_between(
    openparen_parser,
    parser_map(
      parse_sequence(
        parse_pattern("if"),
        whitespace_parser,
        expr_parser,
        whitespace_parser,
        expr_parser,
        whitespace_parser,
        expr_parser
      ),
      function(es)
        return {es[1], es[3], es[5], es[7]}
      end
    ),
    closeparen_parser
  )(text)
end 

-- these two can't be local
-- because they're mutually recursive
-- and late binding scope is weird
sexpr_parser = function(text)
  return tag("sexpr", parse_between(
    parse_pattern("%("),
    parse_separated_by(expr_parser, whitespace_parser),
    parse_pattern("%)")
  ))(text)
end
expr_parser = parse_any(
  tag("fn", fn_parser),
  tag("if", if_parser),
  tag("def", def_parser),
  tag("defg", defg_parser),
  sexpr_parser,
  tag("operator", operator_parser),
  number_parser,
  string_parser,
  boolean_parser,
  tag("identifier", identifier_parser)
)

return {
  parse_any = parse_any,
  parse_many = parse_many,
  parse_pattern = parse_pattern,
  parse_sequence = parse_sequence,
  parse_separated_by = parse_separated_by,
  parse_between = parse_between,
  parse_optional = parse_optional,
  parse_with_whitespace = parse_with_whitespace,
  parser_map = parser_map,
  ith = ith,
  whitespace_parser = whitespace_parser,
  int_parser = int_parser,
  float_parser = float_parser,
  number_parser = number_parser,
  string_parser = string_parser,
  identifier_parser = identifier_parser,
  operator_parser = operator_parser,
  boolean_parser = boolean_parser,
  openparen_parser = openparen_parser,
  closeparen_parser = closeparen_parser,
  fn_parser = fn_parser,
  body_parser = body_parser,
  arglist_parser = arglist_parser,
  def_parser = def_parser,
  defg_parser = defg_parser,
  if_parser = if_parser,
  sexpr_parser = sexpr_parser,
  expr_parser = expr_parser,
}

