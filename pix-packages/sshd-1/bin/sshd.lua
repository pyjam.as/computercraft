local renv = pix_require "remoteenv"

function main()
  rednet.open("top")
  rednet.host("ssh", "demohost")

  print("listening...")
  local clientid, msg = rednet.receive("ssh")
  print("got new connection from ", clientid)

  -- overriding stuff
  local env = renv.newRemoteEnv(clientid)
  
  -- TODO replace hello
  -- replace with shell once we've implemented everything
  local program = "/rom/programs/shell.lua"
  
  print("running", program)
  os.run(env, program)
end

while true do
  main()
end
