local fp = pix_require "@libfunc"
local rterm = pix_require "remoteterm"

local expose = {}

local function newRemotePullEvent(cid)
  -- returns a drop-in replacement for os.pullEvent
  -- TODO use local events if nil
  local remoteEvents = { "key", "key_up", "char" }
  local remotePullEvent = function(filter)
    local protocol = "ssh:" .. cid .. ":pullEvent:" .. filter
    print("LISTENING FOR:" .. protocol)
    local _, msg = rednet.receive(protocol)
    print("GOT REMOTE EVENT:", msg)
    return unpack(msg)
  end
  
  local wrappedPullEvent = function(filter)
    local isRemote = false
    for _,v in pairs(remoteEvents) do
      if v == filter then
        isRemote = true
      end
    end
  
    if isRemote then
      return remotePullEvent(filter)
    else
      return os.pullEvent(filter)
    end
  end
  return wrappedPullEvent
end

local function newRemoteWrite(cid)
  local send = function(msg) rednet.send(cid, msg, "ssh:remote") end
  local remoteWrite = function(...)
    send({ func="write", args={...} })
  end
  return remoteWrite
end

local function newRemoteRead(cid)
  local send = function(msg) rednet.send(cid, msg, "ssh:remote") end
  local receive = function() return ({rednet.receive("ssh:remote")})[2] end
  local remoteRead = function(...)
    send({ func="read", args={...} })
    return receive()
  end
  return remoteRead
end

local function newRemotePrint(cid)
  local send = function(msg) rednet.send(cid, msg, "ssh:remote") end
  local remotePrint = function(...)
    send({ func="print", args={...} })
  end
  return remotePrint
end

local function newRemotePrintError(cid)
  local send = function(msg) rednet.send(cid, msg, "ssh:remote") end
  local remotePrintError = function(...)
    send({ func="printError", args={...} })
  end
  return remotePrintError
end

local function newRemoteTextutils(cid)
  local prcl = "ssh:remotetextutils"
  local send = function(msg) rednet.send(cid, msg, prcl) end
  local receive = function() return ({rednet.receive(prcl)})[2] end
  local remoteTextutils = copy(textutils)
  remoteTextutils.slowWrite = function(...)
    send({ func="slowWrite", args={...} })
    return receive()
  end
  remoteTextutils.slowPrint = function(...)
    send({ func="slowPrint", args={...} })
    return receive()
  end
  remoteTextutils.pagedPrint = function(...)
    send({ func="pagedPrint", args={...} })
    return receive()
  end
  remoteTextutils.tabulate = function(...)
    send({ func="tabulate", args={...} })
    return receive()
  end
  remoteTextutils.pagedTabulate = function(...)
    send({ func="pagedTabulate", args={...} })
    return receive()
  end
  return remoteTextutils
end

local function newLoad(env)
  return function(chunk, chunkname, mode, _env)
    -- TODO: combine env?
    print("load")
    return load(chunk, chunkname, mode, env)
  end
end

function expose.newRemoteEnv(cid)
  -- TODO take a session ID as well
  -- to handle multiple sessions from one client
  local env = {
    term = rterm.newRemoteTerm(cid),
    write = newRemoteWrite(cid),
    print = newRemotePrint(cid),
    read = newRemoteRead(cid),
    printError = newRemotePrintError(cid)
  }
  
  local default_env = _ENV
  for k,v in pairs(default_env) do
    if not env[k] and type(_ENV[k]) == "function" then
      -- debug bullshit
      if k ~= "select" and k ~= "type" and k ~= "setmetatable" and k~= "assert" then
        env[k] = function(...)
          print("DEBUG: calling", k)
          return v(...)
        end
      end
    elseif not env[k] then
      env[k] = _ENV[k]
    end
  end
  
  -- OS
  env.os = copy(_ENV.os)
  env.os.pullEvent = newRemotePullEvent(cid)
  
  -- FS
  env.fs = copy(_ENV.fs)
  -- debug bullshit
  env.fs.open = function(...)
    -- print("DEBUG: FS.OPEN")
    -- pprint({...})
    print("DEBUG: fs.open", ({...})[1])
    assert(({...})[1] ~= io.stdout)
    return fs.open(...)
  end
  
  -- shell
  env.shell = copy(shell)
  
  -- textutils
  env.textutils = newRemoteTextutils(cid)
  
  -- load
  env.load = newLoad(env)
  return env
end

return expose
