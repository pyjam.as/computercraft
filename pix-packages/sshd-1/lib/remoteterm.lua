
local function newRemoteTerm(clientid)
  local send = function(msg) rednet.send(clientid, msg, "ssh:remoteterm") end
  local receive = function() return ({ rednet.receive("ssh:remoteterm") })[2] end
  local remoteterm = {
    write = function(text)
      send({ func="write", args={text} })
    end,
    scroll = function(y)
      send({ func="scroll", args={y} })
    end,
    getCursorPos = function()
      send({ func="getCursorPos", args={y} })
      return unpack(receive())
    end,
    setCursorPos = function(x, y)
      send({ func="setCursorPos", args={x, y} })
    end,
    getCursorBlink = function()
      send({ func="getCursorBlink", args={} })
      return receive()
    end,
    setCursorBlink = function(blink)
      send({ func="setCursorBlink", args={blink} })
    end,
    getSize = function()
      send({ func="getSize", args={} })
      return unpack( receive() )
    end,
    clear = function()
      send({ func="clear", args={} })
    end,
    clearLine = function()
      send({ func="clearLine", args={} })
    end,
    getTextColor = function()
      send({ func="getTextColor", args={} })
      return receive()
    end,
    getTextColour = function()
      send({ func="getTextColor", args={} })
      return receive()
    end,
    setTextColor = function(c)
      send({ func="setTextColor", args={c} })
    end,
    setTextColour = function(c)
      send({ func="setTextColor", args={c} })
    end,
    getBackgroundColor = function()
      send({ func="getBackgroundColor", args={} })
      return receive()
    end,
    getBackgroundColour = function()
      send({ func="getBackgroundColor", args={} })
      return receive()
    end,
    setBackgroundColor = function(c)
      send({ func="setBackgroundColor", args={c} })
    end,
    setBackgroundColour = function(c)
      send({ func="setBackgroundColor", args={c} })
    end,
    isColor = function()
      send({ func="isColor", args={} })
      return receive()
    end,
    isColour = function()
      send({ func="isColor", args={} })
      return receive()
    end,
    blit = function(text, textColor, backgroundColor)
      send({ func="blot", args={text, textColor, backgroundColor} })
      return receive()
    end,
    setPaletteColor = function(...)
      send({ func="setPaletteColor", args={...} })
    end,
    setPaletteColour = function(...)
      send({ func="setPaletteColor", args={...} })
    end,
    getPaletteColor = function(c)
      send({func="getPaletteColor", args={c}})
      return receive()
    end,
    getPaletteColour = function(c)
      send({func="getPaletteColor", args={c}})
      return receive()
    end,
    getPaletteColour = function(c)
      send({func="getPaletteColor", args={c}})
      return receive()
    end,
  }
  remoteterm.current = function()
    return remoteterm
  end
  remoteterm.redirect = function(target)
    return remoteterm
  end
  return remoteterm
end

return { newRemoteTerm = newRemoteTerm }
