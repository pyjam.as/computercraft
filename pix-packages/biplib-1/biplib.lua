-- First line always contains "BIP"
-- Every file has this head "BIP\nfilename\n1337"

-- relative and path have to both be absolute
function uncombine(relative, path)
  local relative = relative:split("/")
  local path = path:split("/")
  if #relative > #path then
    return nil, "not in directory"
  end
  
  for i=1,#path do
    if relative[i] ~= path[i] then
      local result = path[i]
      
      for j=i+1,#path do
        result = result .. "/" .. path[j] 
      end
      return result
    end
  end
end


-- Returns a bipfile bytes on succes, and (nil, err_msg) on failure.
-- files is a list of relative file names, no globbing allowed.
function bip(relative_to, files)
  local bip_bytes = "BIP\n"
  for _, filename in pairs(files) do
    local path = fs.combine(relative_to, filename)
    print("  " .. filename)
    local file = fs.open(path, "rb") 
    local data = file.readAll()
    bip_bytes = bip_bytes .. filename .. "\n" .. #data .. "\n" .. data
  end
  return bip_bytes
end

function readLine(buffer, index)
  local next_newline = buffer:find("\n", index) or #buffer
  return buffer:sub(index, next_newline-1), next_newline+1
end

-- Returns true on succes, and (nil, err_msg) on failure.
-- bipdata must be bytes from a bip file
function unbip(bipdata, destination)
  local header = bipdata:sub(1, 4)
  if header ~= "BIP\n" then
    return nil, "Not a BIP file"
  end

  local index = 5
  local filename, filsize
  while true do
    if index >= #bipdata then return true end
    
    filename, index = readLine(bipdata, index) 
    filesize, index = readLine(bipdata, index)
    -- TODO: Sanitize filename and filesize
    
    local filename = fs.combine(destination, filename)
    local filesize = tonumber(filesize)
    local data = bipdata:sub(index, index+filesize-1)
    index = index+filesize
    
    local f = fs.open(filename, "w")
    f.write(data)
    f.close()
  end
end
