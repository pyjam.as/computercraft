pix_require "@stdlib"
local fp = pix_require "@libfunc"
local codegen = pix_require "codegen"

local function compileplispfile(plisppath, outpath)
  local plispfile = fs.open(plisppath, "r")
  local plispcode = plispfile.readAll()
  
  local luacode = codegen.plisp2lua(plispcode, plisppath)

  local outfile = fs.open(outpath, "w")
  outfile.write(luacode)
  outfile.close()
end

local function runplispfile(plisppath)
  -- figure out where to save stuff
  local pattern = "[^/]+$"
  local filename = plisppath:match(pattern)
  local outpath = plisppath:gsub(pattern, "").."."..filename..".lua"
  
  -- do the thang
  compileplispfile(plisppath, outpath)
  shell.run("/" .. outpath)
end

local function repl()
  -- a very hacky repl
  while true do
    write(">>> ")
    local plispcode = read()
    
    -- gigahack: don't wrap in return if '(defg' is in there
    local is_defg = plispcode:match("%(defg") ~= nil
    if not is_defg then
      -- hack: wrap in fn to allow multiple expressions
      --       and still be able to prefix "return"
      plispcode = "((fn (...) " .. plispcode .."))"
    end
    local luacode = codegen.plisp2lua(plispcode)
    if not is_defg  then
      luacode = "return " .. luacode
    end
    print(luacode)
    
    -- TODO: require? pix_require?
    local result, err = load(luacode)
    if result == nil then
      print(err)
    else 
      pprint(result())
    end
  end
end

local function main()
  if arg[1] == nil then
    repl()
  elseif fs.exists(arg[1]) then
    runplispfile(arg[1])
  elseif fs.exists(shell.resolve(arg[1])) then
    runplispfile(shell.resolve(arg[1]))
  else
    print("could not find file: " .. arg[1])
    error()
  end
end

main()

return {
  runplisp = runplisp
}
