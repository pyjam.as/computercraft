require "/pix/biplib-1/biplib"

local bipfile = arg[1]
if not bipfile then 
  print("Usage: bip <bipfile> <file>...")
  error()
end

if bipfile:sub(1,1) ~= "/" then
  bipfile = fs.combine(shell.dir(), arg[1])
end

if fs.exists(bipfile) then
  print(bipfile .. " already exist")
  error()
end

local files = {}
for i=2,#arg do
  local filename = arg[i]
  if filename:sub(1,1) ~= "/" then
    filename = fs.combine(shell.dir(), filename)
  end
  for _, file in pairs(fs.find(filename)) do
    if not fs.isDir(file) then
      table.insert(files, uncombine(shell.dir(), file))
    end
  end
end

print("Bipping...")
local f = fs.open(bipfile, "w")
local bip_data = bip(shell.dir(), files)
f.write(bip_data)
f.close()
print("Done!")
