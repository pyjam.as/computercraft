local storage = pix_require("@storage-client")
pix_require("@diff")
local std = pix_require("@stdlib")

local output_chest_name = arg[1]
if not output_chest_name then
  print("Usage: storage-interface <output_chest_name>")
  error()
end

input = ""
amount = 0
search_result = {}
selected_item = ""
item_details = {}
mode = "search"
search_timer = nil
local output_chest = peripheral.wrap(output_chest_name)
search_time = 0

function sort(items, query)
  local query_table = std.string.split(query)
  function compare(a, b)
    local _, lcs_a = lcs_diff(std.string.split(a), query_table)
    local _, lcs_b = lcs_diff(std.string.split(b), query_table)
    return lcs_a > lcs_b
  end
  table.sort(items, compare)
  return items
end

function search()
  local before = os.epoch("local")
  local first_letter = input:sub(1,1)
  search_result = sort(get_items()[first_letter], input)
  local after = os.epoch("local")
  search_time = after-before
end

function get_items()
  rednet.send(server, {action = "list"}, PROTOCOL)
  local _, item_names = rednet.receive(PROTOCOL)
  local result = {}
  for _, item_name in pairs(item_names) do
    local parts = string.split(item_name, "_")
    for _, part in pairs(parts) do
      local first_letter = part:sub(1,1)
      if not result[first_letter] then 
        result[first_letter] = {}
      end
      table.insert(result[first_letter], item_name)
    end
  end
  return result
end

function get_item_count()
  local result = 0
  for name, data in pairs(item_details) do
    result = result + data.count
  end
  return result
end

function render()
  term.clear()
  term.setCursorPos(1,2)
  if mode == "amount" and selected_item ~= "" then
    print(string.format("How many %s do you want?\n", selected_item))
    write(string.format("?: %s\n\n", input))
    write(string.format("%d out of %d\n", amount, get_item_count()))
    write(string.format("You can write 10s to get 10 stacks\n", amount, get_item_count()))
  elseif mode == "search" then
    write(string.format("?: %s\n", input))
    if input ~= "" and #search_result > 0 then
      print("search took "..search_time.."ms\n")
      for i=1,9 do
        print(("%d: %s"):format(i, search_result[i]))
      end
    end
  end
  write("\nctrl: clear chest\n\n")
end

function handle_input()
  local event, key = nil, nil
  repeat
    event, key = os.pullEvent()
  until event == "key_up" or event == "char"
  if event == "key_up" then
    -- attempt do de-duplicate char events
    if string.len(keys.getName(key)) == 1 then
      return -- continue
    end
    key = keys.getName(key)
  end
  if key == 'leftCtrl' then
    -- clean chest	
    write("Clearing chest..")
    while #output_chest.list() > 0 do
      for slot in pairs(output_chest.list()) do
        storage.dump(slot, output_chest_name) 
      end
    end
    write("Done!")
    sleep(0.5)
  elseif mode == "search" and input ~= "" and string.match(key, "^[1-9]$") then
    local num = tonumber(key)
    selected_item = search_result[num]
    item_details = storage.details(selected_item)
    input = ""
    mode = "amount"
    search_result = {}
  elseif key == "backspace" then
    local len = string.len(input)
    input = string.sub(input,1,len-1)
    if mode == "amount" then
      local count = tonumber(string.match(input, "%d+")) or 0
      amount = math.min(count, get_item_count())
    else
      search_result = {}
    end
    local count = tonumber(string.match(input, "%d+"))
  elseif mode == "amount" and key == "enter" then
    -- get item
    write("Pulling items..")
    get_item()
    mode = "search"
    selected_item = ""
    input = ""
    item_details = nil
    amount = 0
    write("Done!")
    sleep(0.5)
  elseif event == "char" then
    if mode == "search" then
      input = input .. key
      search_result = {}
    elseif mode == "amount" and string.match(input..key, "^%d+s?$") then
      input = input .. key
      local count = tonumber(string.match(input, "%d+"))
      if string.match(input, "s") then
        count = count * 64
      end
      amount = math.min(count, get_item_count())
    end
  end
end


render()
while true do
  if mode == "search" and input ~= "" and #search_result == 0 then
    parallel.waitForAny(handle_input, search)
  else
    handle_input()
  end
  render()
end
