local completion = require "cc.shell.completion"

function find_edit_suggest(command)
  local args = string.split(command, " ")
  if command:match("^e ") then
    local suggest = completion.file(shell, args[2] or "") 
    return suggest[1]
  else
    return nil
  end
end

function words_in_buffer()
  local suggestion_table = {}
  for i=1, #buf().data do
    local line = buf().data[i]
    for match in line:gmatch("[%a_][%w_][%w_][%w_]+") do
      suggestion_table[match] = true
    end
  end
  return suggestion_table
end

function find_insert_suggest(word, previous, dir)
  local suggestions = textutils.complete(word, words_in_buffer())
  table.sort(suggestions)
  table.insert(suggestions, 1, "")
  if previous then
    local index = table.indexOf(suggestions, previous)
    if index then
      return suggestions[(index-1+dir%#suggestions)+1]
    end
  else
    return suggestions[2]
  end
end

local function sanitize(list)
  for i, e in ipairs(list) do
    if e == "" then
      table.remove(list, i)
    end
  end
end

function find_insert_suggest(word, previous, dir)
  local suggestions = textutils.complete(word, words_in_buffer())
  sanitize(suggestions)
  table.insert(suggestions, 1, "")
  if previous then
    local index = table.indexOf(suggestions, previous)
    if index then
      index = (index-1+dir)%#suggestions+1
      return suggestions[index]
    end
  else
    return suggestions[2]
  end
end
