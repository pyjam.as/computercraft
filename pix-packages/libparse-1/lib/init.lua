local function parse_many(parserf)
  return function(text)
    local remaining = text
    local results = {}
    while true do
      local parsed, newremaining = parserf(remaining)
      if parsed == nil then
        return results, remaining
      end
      remaining = newremaining
      table.insert(results, parsed)
    end
  end
end

local function parse_any(...)
  local parsers = {...}
  return function(text)
    for _,parserf in pairs(parsers) do
      local parsed, remaining = parserf(text)
      if parsed ~= nil then
        return parsed, remaining
      end
    end
    return nil, text
  end
end

local function parse_pattern(pattern)
  return function(text)
    local patternhere = pattern
    if string.sub(patternhere, 1, 1) ~= "^" then
      patternhere = "^" .. patternhere
    end
    local matched = text:match(patternhere)
    if matched == nil then
      return nil, text
    end
    return matched, string.sub(text, matched:len()+1, text:len())
  end
end

local whitespace_parser = parse_pattern(" +")
local int_parser = parse_pattern("%d+")
local float_parser = parse_pattern("%d+%.%d+")
local number_parser = parse_any(float_parser, int_parser)

return {
  parse_many = parse_many,
  parse_any = parse_any,
  parse_pattern = parse_pattern,
  whitespace_parser = whitespace_parser,
  int_parser = int_parser,
  float_parser = float_parser,
  number_parser = number_parser,
}
