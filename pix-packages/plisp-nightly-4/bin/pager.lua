require("/pix/libpager-1/libpager")

local raw = ""

-- overwrite print and write globally
local _write = write
write = function(str)
  raw = raw .. str
end

print = function(...)
  for i = 1, select('#', ...) do
    raw = raw .. tostring(select(i, ...)) .. '\t'
  end
  raw = raw .. "\n"
end

program_name = arg[1]

-- modify argv before loading other program
table.remove(arg, 1)

require(program_name)

-- TODO overwrite term / support colors (must work with grep)?
buffer = string.split(raw, "\n")
pager(buffer, true)
