mode = arg[1]
fileglob = arg[2]

if fileglob == nil or not (mode == "bytes" or mode == "lines") then
  print("Usage: count bytes|lines GLOB")
  return
end

files = fs.find(fileglob)
total = 0

for _, filename in pairs(files) do
  f = fs.open(filename, "r")

  if mode == "bytes" then
    read = f.read
  else
    read = f.readLine
  end

  result = 0
  while true do
    if read() == nil then break end
    result = result + 1
  end
  total = total + result
  term.setTextColor(colors.lightGray)
  write(filename)
  write("\t")
  term.setTextColor(colors.white)
  write(result)
  write("\n")
end

if #files > 1 then
  write("total\t")
  term.setTextColor(colors.pink)
  write(total)
  term.setTextColor(colors.white)
  write(" " .. mode)
  write("\n")
end
