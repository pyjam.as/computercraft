local lib = {}

function lib.sorted(t, func)
  -- iterator for non-array tables.
  --
  -- example usage:
  --   t1 = {one = 1, two = 2, three = 3}
  --
  --   -- will print "one 1\ntwo 2\nthree 3"
  --   for k, v in itertools.sorted(t1) do
  --     print(k, v)
  --   end
  --
  --   -- custom sort function, sort values by largest first
  --   cmp = function(a, b) return a[2] > b[2] end
  --   for k, v in itertools.sorted(t1, cmp) do
  --     print(k, v)
  --   end

  if not func then
    -- default: sort by values, ascending
    func = function(a, b) return a[2] < b[2] end
  end

  local sorted_table = {}
  for k, v in pairs(t) do
    table.insert(sorted_table, {k, v})
  end
  table.sort(sorted_table, func)

  local i = 1
  return function()
    if i > #sorted_table then
      return nil
    end

    local item = sorted_table[i]
    i = i + 1
    return item[1], item[2]
  end
end

return lib
