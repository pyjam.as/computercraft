local PROTOCOL = "pix"

peripheral.find("modem", rednet.open)

local pix_server = rednet.lookup(PROTOCOL, "server")

if not pix_server then
  print("Couldn't reach pix server, is a modem connected?")
  error()
end

function pull(package_name)
  rednet.send(pix_server, {action="pull", package_name=package_name} ,PROTOCOL)
  _, data = rednet.receive(protocol)
  if data.error then
    print("Failed to pull package: " .. package_name) 
    print("pix server replied: " .. data.error) 
    error()
  end
  return data
end

function push(package_name, content)
  rednet.send(pix_server, {action="push", package_name=package_name, content=content}, PROTOCOL) 
  local _, result = rednet.receive(PROTOCOL)
  return result
end

function get_all_packages()
  return get_packages(true)
end

-- get list of the latest version of packages on server.
-- or all packages
-- format: {[package_name-version] = {deserialised package.pix}, ... }
function get_packages(all)
  -- Only latest if not all
  local action = "list"
  if all then action = "list_all" end
  rednet.send(pix_server, {action=action}, PROTOCOL) 
  local _, result = rednet.receive(PROTOCOL)
  if result.error then
    print("failed to list: " .. result.error)
    return
  end
  return result.packages
end

function resolve_dependencies(package_name, deps)
  deps = deps or {}
  local packages = get_all_packages()
  if not packages[package_name] then
    print("Package "..package_name.." doesn't exist")
    error()
  end
  for _, dep in ipairs(packages[package_name].dependencies) do
    deps[dep] = true
    resolve_dependencies(dep, deps) 
  end
  return table.keys(deps)
end

function list_all(path, paths)
  for _, sub_path in ipairs(fs.list(path)) do
    sub_path = fs.combine(path, sub_path)
    if fs.isDir(sub_path) then
      list_all(sub_path, paths)
    else
      table.insert(paths, sub_path)
    end
  end
end
