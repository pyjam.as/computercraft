

local std =  pix_require( "@stdlib");
local fp =  pix_require( "@libfunc");

std.pprint(
  fp.map(
    {1,2,3},
    (function (x) return ( 2 * x) end)));