local std = pix_require("@stdlib")
local ip = rednet.lookup("ssh", "server")
rednet.send(ip, "connect", "ssh")

function worker()
  while true do
    id, msg = rednet.receive("ssh")
    local func = _G
    if msg.module then
      func = func[msg.module]
    end
    local result = {func[msg.name](unpack(msg.args))}
    rednet.send(id, result, "ssh")
  end
end

function eventWorker()
  while true do
    repeat 
      event = {os.pullEvent()}
    until event[1] == "key" or event[1] == "char" or event[1] == "key_up"
    -- term.setTextColor(colors.gray)
    -- print(event[1], event[2])
    -- term.setTextColor(colors.white)
    rednet.send(ip, event, "ssh_event")
  end
end

parallel.waitForAny(worker, eventWorker)
