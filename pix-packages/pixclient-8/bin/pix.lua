pix_require "@biplib"
pix_require "@libpager"
pix_require "@stdlib"
pix_require "@diff"
pix_require "init"

local PIX_STARTUP = "boot.lua"
local PIX_STARTUP_DST = "/startup/pix.lua"
local PIX_CONFIG = "/config.pix"
local PIX_DIR = "/pix"
local NEW_PIX_DIR = "/_pix"
local TEMPLATE_PATH = "/package_template"

-- List info about a single package.
function info(package)
  local packages
  if parse_package_name(package) then
     -- Get specific version
     packages = get_all_packages()
  else
     -- Get latest
     packages = get_packages()
  end
  local found
  for package_name, info in pairs(packages) do
    if package == package_name or package == info.name then
      print(package) 
      term.setTextColor(colors.lightGray)
      print("  "..info.description)
      print()
      term.setTextColor(colors.white)
      print("latest: ")
      term.setTextColor(colors.yellow)
      print("  "..package_name)
      print()  
      term.setTextColor(colors.white)
      print("dependencies: ")
      for _, dependency in ipairs(info.dependencies) do
        term.setTextColor(colors.red)
        print("  "..dependency)
      end
      if #info.dependencies == 0 then
        term.setTextColor(colors.gray)
        print("  none")
      end
      term.setTextColor(colors.white)
      found = true
      break
    end
  end
  if not found then
    print("Package not found")
  end
end

function list()
  local all_packages = get_packages()
  -- build set of installed packages
  local config = read_config(PIX_CONFIG)
  local installed = {}
  for _, package in ipairs(config.packages) do
     local name, version = parse_package_name(package)
     if not installed[name] then installed[name] = {} end
     if version ~= "latest" then
       table.insert(installed[name], version)
     end
  end
  
  local sorted_packages = {}
  for package, info in pairs(all_packages) do
    table.insert(sorted_packages, {package, info})
  end
  table.sort(sorted_packages, function(a, b) return a[2].name < b[2].name end)
  
  -- print packages
  local rows = {"All packages:"}
  local _, height = term.getSize()
  for _, sorted in ipairs(sorted_packages) do
    local package, info = sorted[1], sorted[2]
    local package_name = info.name
    local pkg = {
      colors.white,
      "  " .. package_name .. " ",
      colors.white,
    }
    local name, newest = parse_package_name(package)
    if installed[info.name] then
      table.insert(pkg, "[")
      table.insert(pkg, colors.green)
      local versions = installed[info.name]
      if #versions == 0 or #versions == 1 and versions[1] == newest then
        table.insert(pkg, "up-to-date")
      else
        local install_banner = ""
        table.sort(versions)
        for i, v in ipairs(versions) do
          if i == #versions then
            install_banner = install_banner .. "v" .. v
          else
            install_banner = install_banner .. "v" .. v .. ","
          end
        end
        table.insert(pkg, install_banner .. " installed")
        table.insert(pkg, colors.white)
        table.insert(pkg, ", ")
        table.insert(pkg, colors.yellow)
        table.insert(pkg, "v" .. tostring(newest) .. " available")
      end
      table.insert(pkg, colors.white)
      table.insert(pkg, "]")
    end
    local description = {
      colors.lightGray,
      "    " .. info.description,
      colors.white,
    }
    table.insert(rows, pkg)
    table.insert(rows, description)
  end
  pager(rows)
end

function pix_diff()
  local package_path = shell.dir()
  local config_path = fs.combine(package_path, "package.pix")
  if not fs.exists(config_path) then
    print("You can only run pix diff in the root of your package.")
    error()
  end
  local file = fs.open(config_path, "rb")
  local package_config = file.readAll()
  file.close()
  local package_config, err = textutils.unserialiseJSON(package_config) 
  if err then
    print("Critical: Error parsing package.nix")
    print(err)
    error()
  end
  if not package_config.name or not package_config.description or not package_config.dependencies then
    package_pix_error()
    error()
  end
  
  -- get latest version
  local packages = get_packages()
  local latest_package_name
  for package_name, info in pairs(packages) do
    if info.name == package_config.name then
      latest_package_name = package_name 
      break
    end
  end
  if not latest_package_name then
    print("Couldn't find a package called \""..package_config.name.."\"")
    error()
  end
  
  local package_data = pull(latest_package_name)
  
  local paths = {}
  list_all(package_path, paths)
  local long_diff = ""
  for _, p in ipairs(paths) do
    local relative_path =  uncombine(package_path, p)
    local f = fs.open(p, "r")
    local local_file = f.readAll()
    f.close()
    local local_lines = string.split(local_file, "\n")
    local remote_file, err = unbip_single(package_data.content, relative_path)
    if type(remote_file) == "boolean" then
      -- This is weird, but it means
      -- the file doesnt exist in the bipdata
      remote_file = " "
    end
    -- there is an extra newline in bipfile?!
    local remote_lines = string.split(remote_file:sub(1,-2), "\n")
    local dif = diff(remote_lines, local_lines)
    if dif:len() > 0 then
      long_diff = long_diff.."\n# "..relative_path.."\n"..dif
    end
  end
  print_diff(long_diff)
end

function new()
  local name
  local package_path
  local all_packages = get_packages()
  repeat
    (function ()  -- continue hack
      write("Package name: ")
      name = read()
      if name == "" then
        print("The package name cannot be an empty string.")
        name = nil
        return -- continue
      end
      if name:find("/") then
        print("The package name cannot contain \"/\"")
        name = nil
        return -- continue
      end
      package_path = shell.resolve(name)
    
      if fs.exists(package_path) then
        print("A folder with that name already exists in the current working directory")
        name = nil
        return -- continue
      end
      for package_name, info in pairs(all_packages) do
        if info.name == name then
          print("A package with that name already exists")
          name = nil
          return -- continue
        end
      end
    end)()
  until name
  print()
  
  local description
  repeat 
    write("Package description: ")
    description = read()
    if description == "" then
      print("The package description cannot be an empty string.")
      description = nil
    end
  until description
  print()
  
  local executable
  repeat  
    print("You have the following template options:")
    print("  e: Executable package")
    print("  l: Library package")
    write("Which one do you want? ")
    local answer = read()
    if answer:lower() == "e" then executable = true
    elseif answer:lower() == "l" then executable = false
    else 
      print("Pick either executable or library")
    end
  until executable ~= nil
  print()
  
  -- Create it
  local pix_path = fs.combine(shell.getRunningProgram(), "../..")
  local template_path = pix_path..TEMPLATE_PATH
  
  fs.makeDir(package_path)
  local file = fs.open(template_path.."/package.pix", "r")
  local pix_config = file.readAll()
  file.close()
  
  local file = fs.open(package_path.."/package.pix", "w")
  file.write(pix_config:format(name, description))
  file.close()
  
  if executable then
    fs.copy(template_path.."/bin/cat.lua", package_path.."/bin/cat.lua")
    fs.copy(template_path.."/startup.lua", package_path.."/startup.lua")
  else
    fs.copy(template_path.."/lib/init.lua", package_path.."/lib/init.lua")
  end
  print("Done!")
  print("You can add dependecies or edit name and description in package.pix")
end

function package_pix_error()
  print("Your package.pix must have the following format:")
  print('{')
  print('  "name": "my_package",')
  print('  "description": "my cool package",')
  print('  "dependencies": [ "mydep-1" ]')
  print('}')
  print("dependencies list may be empty")
end

function publish()
  local package_path = shell.dir()
  
  -- Sanity checks
  local config_path = fs.combine(package_path, "package.pix")
  if not fs.exists(config_path) then
    print("Missing package.pix")
    package_pix_error()
    error()
  end
  local file = fs.open(config_path, "rb")
  local package_config = file.readAll()
  file.close()
  
  local package_config, err = textutils.unserialiseJSON(package_config) 
  if err then
    print("Critical: Error parsing package.nix")
    print(err)
    error()
  end
  if not package_config.name or not package_config.description or not package_config.dependencies then
    package_pix_error()
    error()
  end
  -- TODO: Check that these dependencies exist
  local all_packages = get_packages()
  
  for _, dependency in ipairs(package_config.dependencies) do
    if dependency:match("^/") then
      print("Dependency \""..dependency.."\" is a local path, please change this before publishing.")
      error()
    end
    if dependency:match("%-latest$") then
      print('You may not use "-latest" in your dependency list')
      error()
    end
    local dependency_name, dependency_version = dependency:match("^([%w%-]+)%-(%d+)$")
    dependency_version = tonumber(dependency_version)
    if not dependency_version then
      print("Invalid version for dependency \""..dependency.."\"")
      error()
    end
    local found = false
    for package, info in pairs(all_packages) do
      if info.name == dependency_name then
        local version = package:match("%-(%d+)$")
        version = tonumber(version)
        if dependency_version < 1 or dependency_version > version then
          print("The specified version of dependency \""..dependency.."\" doesn't exist")
          print("The newest version of that package is \"".. package.."\"")
          error()
        end
        found = true
      end
    end
    if not found then
      print("Dependency \""..dependency.."\" doesn't exist")
      error()
    end
  end
  
  -- Probably a sane package
  local paths = {}
  list_all(package_path, paths)
  
  local relative_paths = {}
  for _, path in ipairs(paths) do
    table.insert(relative_paths, uncombine(package_path, path))
  end
  
  print("Wrapping your package..")
  local bip_data = bip(package_path, relative_paths)
  local result = push(package_config.name, bip_data)
  if result.error then
    print("Got error while pushing package")
    print(result.error)
    error()
  end
  
  print("Published \""..result.package_name.."\"!")
end

function install_package(package, installed)
  -- split by last "-" to get version
  local version
  if package:match("%-latest$") then
    version = "latest"
  else
    _, version = parse_package_name(package)
  end
  
  if not version then
    print("Invalid version for package \""..package.."\"")
    error()
  end
  
  -- Download apropriate version
  local package_data = pull(package)
  -- Extract package
  local package_path = fs.combine(NEW_PIX_DIR, package_data.package_name)
  unbip(package_data.content, package_path)
  installed[package_data.package_name] = true
  
  local package_config, err = read_package(package_path)
  if err then
    print(err)
    error()
  end
  
  for _, dependency_name in ipairs(package_config.dependencies) do
    -- install dependency
    if not installed[dependency_name] then
      install_package(dependency_name, installed)
    end
  end
end

-- Cleanup whatever setup did
function cleanup()
  fs.delete(PIX_STARTUP_DST)
end

-- Setup whatever pix needs to function
function setup()
  -- Own path
  local path = shell.getRunningProgram()
  -- Running program is /bin/pix.lua so we step 2 levels up
  local pixclient_path = fs.getDir(fs.getDir(path))
  
  local pix_startup_path = "/"..pixclient_path.."/"..PIX_STARTUP
  -- Deploy /startup/pix.lua
  local file = fs.open(PIX_STARTUP_DST, "w")
  file.write('shell.run("'..pix_startup_path..'")')
  file.close()
  -- Setup the new packages.
  shell.run(pix_startup_path)
end

-- pix build
function build()
  fs.delete(NEW_PIX_DIR)
  -- read config
  local config = read_config(PIX_CONFIG)
  
  -- Sanity checks
  local has_pixclient = false
  for _, package in ipairs(config.packages) do
    if package:match("^pixclient%-%d+$") or package == "pixclient-latest" then
      has_pixclient = true
    end
  end
  if not has_pixclient then
    print("Your package.nix must contain pixclient")
    error()
  end
  
  local installed = {}
  -- Build new system 
  for _, package in ipairs(config.packages) do
    install_package(package, installed)
  end
  
  local dev_installed = {}
  -- Dev folder dependecies
  if config.dev_folder then
    local dev_packages = fs.find(config.dev_folder.."/*")
    for _, package_path in ipairs(dev_packages) do
      package_path = "/"..package_path
      if fs.isDir(package_path) then
        local package_config, err = read_package(package_path)
        if err then
          print(err)
          error()
        else
          for _, package in ipairs(package_config.dependencies) do
            if package:sub(1,1) ~= "/" then
              install_package(package, dev_installed)
            end
          end
        end
      end
    end
  end
  
  local was_installed = {}
  for _, path in ipairs(fs.find(PIX_DIR.."/*")) do
    was_installed[path:sub(5,-1)] = true
  end
  local printable = {} 
  local new_pixclient
  print("Build changes:")
  for p, _ in pairs(dev_installed) do
    if not was_installed[p] then 
      table.insert(printable, {p, "dev_added"})
    end
  end
  for p, _ in pairs(installed) do
    if p:match("^pixclient%-%d+$") then
      new_pixclient = p
    end
    if not was_installed[p] then 
      table.insert(printable, {p, "added"})
    end
  end
  for p, _ in pairs(was_installed) do
    if not installed[p] and not dev_installed[p] then
      table.insert(printable, {p, "removed"})
    end
  end
  local color_map = {
    added = colors.green,
    removed = colors.red,
    dev_added = colors.cyan,
  }
  table.sort(printable, function(a,b) return a[1] < b[1] end)
  for _, p in ipairs(printable) do
    local package, color = p[1], p[2]
    term.setTextColor(color_map[color])  
    print("  "..package)
  end
  if #printable == 0 then
    term.setTextColor(colors.gray)
    print("  none")
  end
  term.setTextColor(colors.white)  
  
  -- Replace old system with new system
  fs.delete(PIX_DIR)
  fs.move(NEW_PIX_DIR, PIX_DIR)
  
  -- Cleanup before letting the new pix setup
  cleanup()
  
  -- Run the setup function of newly installed pix
  local pix_path = PIX_DIR.."/"..new_pixclient.."/bin/pix.lua"
  shell.run(pix_path, "setup")
  
  print("Done!")
end

function printUsage()
  print("Usage: pix [COMMAND]")
  print("Commands:")
  print("  build           Make packages reflect what's in /config.pix")
  print("  new             Interactive package boostrap tool")
  print("  publish         Take current directory and publish it as a pix package")
  print("  list            Lists all published packages")
  print("  info [package]  Lists all published packages")
  print("  diff            Compare local changes to latest on server")
end

if arg[1] == "build" or arg[1] == "rebuild" then
  build()
elseif arg[1] == "publish" then
  publish()
elseif arg[1] == "list" then
  list()
elseif arg[1] == "new" then
  new()
elseif arg[1] == "info" and arg[2] then
  info(arg[2])
elseif arg[1] == "diff" then
  pix_diff()
elseif arg[1] == "setup" then
  setup()
else
  printUsage()
end
