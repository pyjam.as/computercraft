

local im  = pix_require("@immutable");
local std  = pix_require("@stdlib");
local fp  = pix_require("@libfunc");

local fname  = "input.txt";
local file  = fs.open(shell.resolve(fname), "r");
local input  = file.readAll();


local digitnums  = im.set({},
  "0", 0,
  "1", 1,
  "2", 2,
  "3", 3,
  "4", 4,
  "5", 5,
  "6", 6,
  "7", 7,
  "8", 8,
  "9", 9);
  
local function rest (line)
  return string.sub(line, 2) end;

local function digitfromhead (line)
  local firstchar  = string.sub(line, 1, 1);
  return digitnums[firstchar]
 end;

local function firstdigit (line)
  local num  = digitfromhead(line);
  return (function()if num then 
    return num else 
    return firstdigit(rest(line)) end end)() end;
    
local function lastdigit (line)
  return (function()if (line == "") then 
    return nil else 
    return (function (_)
      local r  = lastdigit(rest(line));
      return (function()if r then 
        return r else 
        return digitfromhead(line)
       end end)()
     end)()
   end end)()
 end;

local function line2num (line)
  local fd  = firstdigit(line);
  local ld  = lastdigit(line);
  return ((10 * fd) + ld) end;

local function part1 (data)
  local lines  = std.string.split(data, "\n");
  local nums  = fp.map(lines, line2num);
  return fp.fold(
    nums,
    (function (a, b) return (a + b) end),
    0
  )
 end;

print(part1(input));