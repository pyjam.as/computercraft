

local im  = pix_require("@immutable");
local std  = pix_require("@stdlib");
local fp  = pix_require("@libfunc");

local fname  = "input.txt";
local file  = fs.open(shell.resolve(fname), "r");
local input  = file.readAll();

local function dbg (x) std.pprint(x); return x end;

local digitnums  = im.set({},
  "0", 0,
  "1", 1,
  "2", 2,
  "3", 3,
  "4", 4,
  "5", 5,
  "6", 6,
  "7", 7,
  "8", 8,
  "9", 9);
  
local function rest (line)
  return string.sub(line, 2) end;

local function digitfromhead (line)
  local firstchar  = string.sub(line, 1, 1);
  return digitnums[firstchar] end;
  
local function matchfirst (f)
  local function innerf (line)
    local num  = f(line);
    return (function()if num then 
      return num else 
      return innerf(rest(line)) end end)() end;
  return innerf end;

    
local function matchlast (f)
  local function innerf (line)
    return (function()if (line == "") then 
      return nil else 
      return (function (_)
        local r  = innerf(rest(line));
        return (function()if r then 
          return r else 
          return f(line)
         end end)()
       end)()
     end end)()
   end;
  return innerf
 end;

local firstdigit  = matchfirst(digitfromhead);

local lastdigit  = matchlast(digitfromhead);

local function line2num (line)
  local fd  = firstdigit(line);
  local ld  = lastdigit(line);
  return ((10 * fd) + ld) end;
  
local function sum (list)
  return fp.fold(list, (function (a, b) return (a + b) end), 0) end;

local function part1 (data)
  local lines  = std.string.split(data, "\n");
  return sum(fp.map(lines, line2num)) end;

local p1  = part1(input);

(function()if (p1 ~= 55488) then 
    return print("day 1 err", p1) else 
    return nil end end)();
print(p1);
    
-- part two
local digitwords  = im.set({},
  "one", 1,
  "two", 2,
  "three", 3,
  "four", 4,
  "five", 5,
  "six", 6,
  "seven", 7,
  "eight", 8,
  "nine", 9);

local function startswith (line, target)
  return (string.sub(line, 1, #target)
      == target) end;
  
local function wordfromhead (line)
  return fp.fold(
    std.table.keys(digitwords),
    (function (result, dw)
      return (function()if ((not result)
               and startswith(line, dw)
          ) then 
          return digitwords[dw] else 
          return result
       end end)()
     end),
    nil) end;
    
local function digit_or_word_from_head (line)
  return (digitfromhead(line) or wordfromhead(line)) end;
    
local firstdigit2  = matchfirst(digit_or_word_from_head);
local lastdigit2  = matchlast(digit_or_word_from_head);

local function line2num2 (line)
  local fd  = firstdigit2(line);
  local ld  = lastdigit2(line);
  return ((10 * fd) + ld) end;

local function part2 (data)
  local lines  = std.string.split(data, "\n");
  return sum(fp.map(lines, line2num2)) end;
  
local p2  = part2(input);
(function()if (p2 ~= 55614) then 
  return print("bad p2", p2) else 
  return nil end end)();
print(p2);