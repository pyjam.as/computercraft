
local test  = pix_require("@unittest");
local fp  = require("lib/init");

test("test map", (function (_) 
  local t  = {1,2,3};
  local mapped  = fp.map(t, (function (x) return (x * 2) end));
  test.assert_eq(2, mapped[1]);
  test.assert_eq(4, mapped[2]);
  return test.assert_eq(6, mapped[3]) end));
  
test("test all", (function (_) 
  local t  = {1,2,3};
  test.assert(fp.all(t, (function (e) return (e < 5) end)));
  return test.assert_not(fp.all(t, (function (e) return (e == 2) end))) end));
  
test("pack", (function (_)
  local r  = fp.pack("a", "b");
  test.assert_eq("a", r[1]);
  return test.assert_eq("b", r[2]) end));

test("pack unpack", (function (_)
  local t  = fp.pack("a", "b");
  local r  = fp.pack(unpack(fp.pack(unpack(t))));
  test.assert_eq(t[1], r[1]);
  return test.assert_eq(t[2], r[2]) end));
  
test("fold sum", (function (_)
  local t  = {1,2,3};
  local r  = fp.fold(t, (function (a, b) return (a + b) end), 0);
  return test.assert_eq(r, 6) end));
  
test:print_result();