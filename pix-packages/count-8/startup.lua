-- This file is run by /startup/pix.lua
-- package path is given as first parameter
-- so you don't need to update this file when
-- releasing new version
local package_path = arg[1]
local bin_path = package_path .. "/bin"
shell.setPath(shell.path() .. ":" .. bin_path)

local completion = require "cc.shell.completion"
local count_path = bin_path:sub(2,-1).."/count.lua"
shell.setCompletionFunction(
  count_path,
  completion.build(
    {completion.choice, {"lines", "bytes"}},
    {completion.file, many = true}
  )
)
