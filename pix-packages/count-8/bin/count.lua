mode = arg[1]

if arg[2] == nil or not (mode == "bytes" or mode == "lines") then
  print("Usage: count bytes|lines FILE...")
  return
end

files = {}
for i=2,#arg do
  local glob = shell.resolve(arg[i])
  for _, filename in pairs(fs.find(glob)) do
    if not fs.isDir(filename) then
      files[filename] = 1 -- deduplicate
    end
  end
end

sorted_files = {}
for filename, _ in pairs(files) do
  table.insert(sorted_files, filename)
end
table.sort(sorted_files, function(a, b) return a < b end)

total = 0
for _, filename in ipairs(sorted_files) do
  f = fs.open(filename, "r")

  if mode == "bytes" then
    read = f.read
  else
    read = f.readLine
  end

  result = 0
  while true do
    if read() == nil then break end
    result = result + 1
  end
  total = total + result
  term.setTextColor(colors.lightGray)
  write(filename)
  write("\t")
  term.setTextColor(colors.white)
  write(result)
  write("\n")
end

if #sorted_files > 1 then
  term.setTextColor(colors.pink)
  write(total)
  term.setTextColor(colors.white)
  write(" " .. mode .. " total\n")
end
