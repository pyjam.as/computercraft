-- This file is run by /startup/pix.lua
-- package path is given as first parameter
-- so you don't need to update this file when
-- releasing new version
local package_path = arg[1]
-- This path MUST have a leading slash, so we can't use
-- fs.combine (as it removes leading slashes)
local bin_path = package_path .. "/bin"
shell.setPath(shell.path() .. ":" .. bin_path)

-- auto complete
local completion = require "cc.shell.completion"
-- Must be absolute path to the program, but without
-- leading slash :shrug:
local vi_path = bin_path:sub(2,-1).."/vi.lua"
shell.setCompletionFunction(vi_path, completion.build({ completion.file, many = true }))
