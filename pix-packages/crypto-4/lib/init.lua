local function enc_vig(pt, k)
  local ct = ""
  for i = 1, pt:len() do
    local ik = (i % k:len()) + 1
    local key_byte = string.byte(k:sub(ik, ik))
    local plaintext_byte = string.byte(pt:sub(i,i))
    local out_char = string.char((key_byte + plaintext_byte) % 256)
    ct = ct .. out_char  
  end
  sleep(0.5)
  return ct
end

local function dec_vig(ct, k)
  local pt = ""
  for i = 1, ct:len() do
    local ik = (i % k:len()) + 1
    local key_byte = string.byte(k:sub(ik, ik))
    local ciphertext_byte = string.byte(ct:sub(i,i))
    local out_char = string.char((ciphertext_byte - key_byte) % 256)
    pt = pt .. out_char  
  end
  return pt
end

local function test_vig()
  key = "p4ss)"
  plaintext = "secret"
  ciphertext = enc_vig(plaintext, key)
  plaintext_dec = dec_vig(ciphertext, key)
  print(plaintext == plaintext_dec)
end

local function vigenere(key)
  return {
    encrypt = function (pt) return enc_vig(pt, key) end,
    decrypt = function (ct) return dec_vig(ct, key) end
    }

return { vigenere = vigenere }


