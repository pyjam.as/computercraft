local monitor = peripheral.find("monitor")
monitor.setTextScale(0.5)
monitor.setBackgroundColor(colors.black)
monitor.clear()

p2_sprites = {
  rook = {
    0, 0, 0, 0,
    0, 1, 1, 0,
    0, 1, 1, 0,
  },
  pawn = {
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 1, 1, 0,
  },
  king = {
    0, 0, 0, 0,
    0, 1, 0, 1,
    0, 1, 1, 1,
  },
  bishop = {
    0, 0, 0, 0,
    0, 0, 1, 0,
    0, 1, 1, 0,
  },
  queen = {
    0, 0, 0, 0,
    0, 1, 0, 1,
    0, 0, 1, 0,
  },
  knight = {
    0, 0, 0, 0,
    0, 1, 1, 0,
    0, 1, 0, 0,
  }
}

p1_sprites = {}
for name, sprite in pairs(p2_sprites) do
  local flipped = {}
  for i, v in pairs(sprite) do
    flipped[13 - i] = v
  end
  p1_sprites[name] = flipped
end

p1 = "player1"
p2 = "player2"

local board = {
  {"rook", p1}, {"knight", p1}, {"bishop", p1}, {"king", p1},
  {"queen", p1}, {"bishop", p1}, {"knight", p1}, {"rook", p1},
  {"pawn", p1}, {"pawn", p1}, {"pawn", p1}, {"pawn", p1},
  {"pawn", p1}, {"pawn", p1}, {"pawn", p1}, {"pawn", p1},
  
  {}, {}, {}, {}, {}, {}, {}, {},
  {}, {}, {}, {}, {}, {}, {}, {},
  {}, {}, {}, {}, {}, {}, {}, {},
  {}, {}, {}, {}, {}, {}, {}, {},

  {"pawn", p2}, {"pawn", p2}, {"pawn", p2}, {"pawn", p2},
  {"pawn", p2}, {"pawn", p2}, {"pawn", p2}, {"pawn", p2},
  {"rook", p2}, {"knight", p2}, {"bishop", p2}, {"king", p2},
  {"queen", p2}, {"bishop", p2}, {"knight", p2}, {"rook", p2}
}

function get(x, y)
  return board[y*8 + 1 + x]
end

function set(x, y, piece)
  board[y*8 + 1 + x] = piece
end

function pawn_moves(x,y,player)
  local dir = 1
  if player == p2 then
    dir = -1
  end
  local opponent = p1
  if player == p1 then
    opponent = p2
  end
  local moves = {}
  if (player == p1 and y == 7) or (player == p2 and y == 0) then
    return {}
  end
  if #get(x, y+dir) == 0 then
    --forward
    moves[x.." "..(y + dir)] = {x, y + dir}
  end
  if get(x+1, y+dir)[2] == opponent then
    --forward-left (black right)
    moves[(x + 1).." "..(y + dir)] = {x+1, y + dir}
  end
  if get(x-1, y+dir)[2] == opponent then
    --forward-right (black left)
    moves[(x-1).." "..(y + dir)] = {x-1, y + dir}
  end
  return moves
end

function king_moves(x,y,player)
  moves = {}
  for i=-1,1,1 do
    for j =-1,1,1 do
      if i~=0 or j~=0 then
        if not outside_board(x+i, y+j) and get(x+i, y+j)[2]~=player then
          moves[(x+i).." "..(y+j)] = {x+i, y+j}
        end
      end
    end
  end
  return moves
end

function knight_moves(x,y,player)
  print("knight selected")
  local moves = {}
  for x_dir=-1,1,2 do
    for y_dir=-1,1,2 do
      for _, extra in pairs({{1,0}, {0,1}}) do
        dest_x = x + x_dir * (1 + extra[1])
        dest_y = y + y_dir * (1 + extra[2])
        local within_board = (0 <= dest_x) and (dest_x <= 7) and (0 <= dest_y) and (dest_y <= 7)
        if within_board and get(dest_x, dest_y)[2] ~= player then
          moves[dest_x.." "..dest_y] = {dest_x, dest_y}
        end
      end
    end
  end
  return moves
end

function outside_board(x,y)
  return (x < 0 or x > 7 or y < 0 or y > 7)
end

function trace_dir(start_x, start_y, dir_x, dir_y, moves, player)
  print(start_x, start_y, dir_x, dir_y)
  local pos_x = start_x
  local pos_y = start_y
  repeat
    pos_x = pos_x + dir_x
    pos_y = pos_y + dir_y
    print("checking", pos_x, pos_y)
    if (not outside_board(pos_x, pos_y)) and get(pos_x, pos_y)[2]~=player then
      moves[pos_x.." "..pos_y] = {pos_x, pos_y}
    end
  until outside_board(pos_x+dir_x, pos_y+dir_y) or #get(pos_x, pos_y) ~= 0 
end

function legal_moves(x, y)
  print(x, type(x))
  print(y, type(y))
  local piece, player = unpack(get(x, y))
  local moves = {}
  

  if piece == "pawn" then
    return pawn_moves(x, y, player)
  elseif piece == "knight" then
    return knight_moves(x,y, player)
  elseif piece == "rook" then
    moves = {}
    dirs = {{1,0},{0,1},{-1,0},{0,-1}}
    for _, dir in pairs(dirs) do
      trace_dir(x, y, dir[1], dir[2], moves, player)
    end
    return moves
  elseif piece == "bishop" then
    moves = {}
    dirs = {{1,1},{-1,1},{1,-1},{-1,-1}}
    for _, dir in pairs(dirs) do
      trace_dir(x, y, dir[1], dir[2], moves, player)
    end
    return moves
  elseif piece == "queen" then
    moves = {}
    dirs = {{1,1},{-1,1},{1,-1},{-1,-1}, {1,0},{0,1},{-1,0},{0,-1}}
    for _, dir in pairs(dirs) do
      trace_dir(x, y, dir[1], dir[2], moves, player)
    end
    return moves
  elseif piece == "king" then
    return king_moves(x,y,player) 
  else
    print("legal_moves not implemented for", piece)
    return nil
  end
  
  
  return moves
end

function minmax(n)
  return math.min(math.max(0, n), 7)
end

function get_input()
  _, _, x, y = os.pullEvent("monitor_touch")
  x = math.floor((x - 3) / 4)
  y = math.floor((y - 1) / 3)
  print("clicked "..x..", "..y)
  
  return minmax(x), minmax(y)
end

function render(green_moves)
  for x=0, 7 do
    for y=0,7 do
      local current = board[y*8+x+1]
      bg = colors.lightGray
      if green_moves[x.." "..y] ~= nil then
        bg = colors.lime
      end
      if (x + y) % 2 == 1 then
        if bg == colors.lime then
          bg = colors.green
        else
          bg = colors.gray
        end
      end
      for i=1,4 do
        for j=1,3 do
          local screen_x = x * 4 + i + 2
          local screen_y = y * 3 + j
          local color = bg
          
          if #current ~= 0 then
            local sprites = p1_sprites
            if current[2] == p2 then
              sprites = p2_sprites
            end
            if sprites[current[1]][(j - 1) * 4 + i] == 1 then
              if current[2] == p1 then
                color = colors.white
              else
                color = colors.black
              end
            end
          end
          monitor.setBackgroundColor(color)
          monitor.setCursorPos(screen_x, screen_y)
          monitor.write(" ")
        end
      end
    end
  end
end

render({})
while true do
  render({})
  local x, y = get_input()
  print("got", x, y)
  local moves = legal_moves(x,y)
  print("moves: "..textutils.serialise(moves))
  if moves ~= nil then
    render(moves)
    local dest_x, dest_y = get_input()
    if moves[dest_x.." "..dest_y] ~= nil then
      print("moving pawn to "..dest_x.." "..dest_y)
      piece = get(x,y)
      set(dest_x, dest_y, piece)
      set(x,y,{})
    end
  end
end
