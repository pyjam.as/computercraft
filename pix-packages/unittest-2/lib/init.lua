-- unittest framework
-- ~~~~~~~~~~~~~~~~~~
--
-- For now, there is no "runner", so you will have to make it up yourself.
-- Here's an example of how it might look:
-- 
--   local test = pix_require("@unittest")
--
--   test("simple matching", function()
--     test.assert(match("aaa", "aaa"))
--   end)
--
--   test("simple matching fail", function()
--     test.assert_not(match("aaa", "aba"))
--   end)
--
--   test("find all simple", function()
--     test.assert_eq({{from=1, to=1}}, find_all("a", "a"))
--   end)
--
--   test:print_result()


local function internal_eq(a, b)
  if type(a) ~= type(b) then return false end
  if type(a) == "table" then
    for k, v in pairs(a) do
      if not b[k] then return false end
      if not internal_eq(v, b[k]) then return false end
    end
    return true 
  end
  return a == b
end


return setmetatable({
  successful = 0,
  failed = 0,
  
  -- Test functions:
  assert = function(bool)
    if bool ~= true then
       error("expected true, got " .. bool) 
    end
  end,
  assert_not = function(bool)
    if bool ~= false then
       error("expected false, got " .. bool) 
    end
  end,
  assert_not_nil = function(obj)
    if obj == nil then
       error("expected other than nil") 
    end
  end,
  assert_len = function(len, t)
    if #t ~= len then
       error("expected #t == " .. len .. ", but #t == " .. #t)
    end
  end,
  assert_in = function(t, k)
    if t[k] == nil then
       error(k .. " not in table, did you mean assert_contains?")
    end
  end,
  assert_contains = function(t, k)
    for i, v in ipairs(t) do
      if v == k then
        return
      end
    end
    error(k .. " not in table, did you mean assert_in?")
  end,
  assert_eq = function(expected, result)
    if not internal_eq(result, expected) then
      error("unexpected: " .. textutils.serialize(result))
    end
  end,
  assert_neq = function(a, b)
    if internal_eq(a, b) then
      error("unexpected equality")
    end
  end,

  print_result = function(self)
    term.setTextColor(colors.green)
    if self.failed == 0 then
      print("All tests passed!")
    else
      write(self.successful)
      term.setTextColor(colors.white)
      write(" tests ran successfully, ")
      term.setTextColor(colors.red)
      write(self.failed)
      term.setTextColor(colors.white)
      print(" tests failed!")
    end
  end
}, {
  __call = function(self, name, func)
    local status, error = pcall(func)
    if status then self.successful = self.successful + 1
    else
      self.failed = self.failed + 1
      term.setTextColor(colors.red)
      write(name .. " failed")
      term.setTextColor(colors.white)
      print(": " .. error)
    end
  end
})
