-- vi bindings for movement
-- does not implement multiline movement e.g. pressing "w" at the end of a line
pix_require "@regex"


WORD_DELIMITERS = {
  " ",
  ",",
  ".",
  "!",
  "?",
  "(",
  ")",
  "-",
  "+",
  "*",
  "/",
  "[",
  "]",
}

function is_delimiter(char)
  for _, delimiter in pairs(WORD_DELIMITERS) do
    if delimiter == char then
      return true
    end
  end
  return false
end


function next_word(cursor_pos, line)
  local rest_of_line = string.sub(line, cursor_pos, -1)
  for offset = 1, string.len(rest_of_line) do
    if not is_delimiter(string.sub(rest_of_line, offset, offset)) then
      return cursor_pos + offset - 1
    end
  end
end


function matcher(cursor_pos, line)
  local best = nil
  local next_delimiter = nil

  if is_delimiter(string.sub(line, cursor_pos, cursor_pos)) then
    return cursor_pos + 1
  end

  local line = string.sub(line, cursor_pos, -1)

  for _, delimiter in pairs(WORD_DELIMITERS) do
    local offset = string.find(line, delimiter, nil, true)

    if offset ~= nil and (best == nil or offset < best) then
      best = offset
      next_delimiter = delimiter
    end
  end

  return best, next_delimiter
end

function vi_w(cursor_x, cursor_y, offset)
  offset = offset or 0
  local line = buf().data[cursor_y]
  if is_delimiter(string.sub(line, cursor_x, cursor_x)) then
    if next_word(cursor_x, line) then
      return lim_x(next_word(cursor_x, line), cursor_y), cursor_y
    else
      return vi_end(cursor_x, cursor_y, offset)
    end
  end

  local best, delimiter = matcher(cursor_x, line)

  if best == nil then
    return vi_end(cursor_x, cursor_y, offset)
  end

  if delimiter ~= " " then
    best = best - 1
  end

  return lim_x(cursor_x + best, cursor_y), cursor_y
end


function vi_e(cursor_x, cursor_y, offset)
  line = buf().data[cursor_y]
  if string.len(line) == cursor_x then
    return cursor_x, cursor_y
  end

  if is_delimiter(string.sub(line, cursor_x, cursor_x)) or is_delimiter(string.sub(line, cursor_x+1, cursor_x+1)) then
    local word = next_word(cursor_x+1, line)
    if word == nil then
      return string.len(line), cursor_y
    end
    return lim_x(word, cursor_y), cursor_y
  end

  local best, delimiter = matcher(cursor_x, line)

  if best == nil then
    return string.len(line), cursor_y
  end

  return lim_x(cursor_x+best-2, cursor_y), cursor_y
end


function vi_b(cursor_x, cursor_y, offset)
  line = buf().data[cursor_y]
  if cursor_x == 1 then
    return 1, cursor_y
  end

  start = cursor_x - 1 

  while string.sub(line, start, start) == " " do
    start = start - 1
  end

  for i = start, 1, -1 do
    current = string.sub(line, i, i)
    if is_delimiter(current) then
      if current == " " then
        return lim_x(i + 1, cursor_y), cursor_y
      end
      return lim_x(i, cursor_y), cursor_y
    end
  end

  return 1, cursor_y
end

function lim_y(n)
  return math.min(math.max(1, n), #buf().data)
end

function lim_x(cursor_x, cursor_y)
  return math.max(1, math.min(cursor_x, string.len(buf().data[cursor_y])))
end

function vi_j(cursor_x, cursor_y, offset)
  local cursor_y = lim_y(cursor_y+1)
  return lim_x(buf().max_cursor_x, cursor_y), cursor_y, true
end

function vi_k(cursor_x, cursor_y, offset)
  local cursor_y = lim_y(cursor_y-1)
  return lim_x(buf().max_cursor_x, cursor_y), cursor_y, true
end

function vi_h(cursor_x, cursor_y, offset)
  return lim_x(cursor_x-1, cursor_y), cursor_y
end

function vi_l(cursor_x, cursor_y, offset)
  return lim_x(cursor_x+1, cursor_y), cursor_y
end

function vi_a(cursor_x, cursor_y, offset)
  -- like l but without normal limits
  return cursor_x+1, cursor_y, cursor_y
end

function vi_A(cursor_x, cursor_y, offset)
  local cursor_x, cursor_y =  vi_end(cursor_x, cursor_y)
  return cursor_x+1, cursor_y 
end

function vi_0(cursor_x, cursor_y, offset)
  return 1, cursor_y
end

function vi_end(cursor_x, cursor_y, offset)
  offset = offset or 0
  return math.max(1, string.len(buf().data[cursor_y])+offset), cursor_y
end

function vi_nop(cursor_x, cursor_y, offset)
  return cursor_x, cursor_y, nil, true
end

function vi__(cursor_x, cursor_y, offset)
  offset = offset or 0
  local line = buf().data[cursor_y]
  for i=1,#line do
    if string.charAt(line, i) ~= " " then return i, cursor_y end
  end
  return math.max(1, string.len(line)+offset), cursor_y
end

function vi_gg(cursor_x, cursor_y, offset)
  cursor_x = lim_x(cursor_x, 1)
  return cursor_x, 1
end

function vi_G(cursor_x, cursor_y, offset)
  cursor_x = lim_x(cursor_x, #buf().data)
  return cursor_x, #buf().data
end

function vi_n(cursor_x, cursor_y, offset, query)
  offset = offset or 0
  query = query or search
  for i=cursor_y, #buf().data do
    local adjust = cursor_x+1
    if i ~= cursor_y then
      adjust = 1
    end
    local matches = find_all(query, string.sub(buf().data[i], adjust, -1))
    for _, match in pairs(matches) do
      return adjust + match.from - 1 + offset, i
    end
  end
  return cursor_x, cursor_y
end

function vi_N(cursor_x, cursor_y, offset, query)
  query = query or search
  for i=cursor_y, 1, -1 do
    local adjust = cursor_x-1
    if i ~= cursor_y then
      adjust = #buf().data[i]
    end
    local matches = find_all(query, string.sub(buf().data[i], 1, adjust))
    for j=#matches,1,-1  do
      local match = matches[j]
      return match.from, i
    end
  end
  return cursor_x, cursor_y
end

function vi_t(char)
  return vi_tf(char, 1, vi_n) 
end

function vi_f(char)
  return vi_tf(char, 0, vi_n) 
end

function vi_T(char)
  return vi_tf(char, -1, vi_N) 
end

function vi_F(char)
  return vi_tf(char, 0, vi_N) 
end

function vi_tf(char, adjust, n_func)
  local func = function(cursor_x, cursor_y, offset)
    offset = offset or 0
    local new_cursor_x, new_cursor_y = n_func(cursor_x+adjust, cursor_y, offset, char)
    if new_cursor_y ~= cursor_y then
      return cursor_x, cursor_y
    else
      return new_cursor_x-adjust, new_cursor_y
    end
  end
  return func
end

