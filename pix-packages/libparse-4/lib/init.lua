local function parse_many(parserf)
  return function(text)
    local remaining = text
    local results = {}
    while true do
      local parsed, newremaining = parserf(remaining)
      if parsed == nil then
        return results, remaining
      end
      remaining = newremaining
      table.insert(results, parsed)
    end
  end
end

local function parse_any(...)
  local parsers = {...}
  return function(text)
    for _,parserf in pairs(parsers) do
      local parsed, remaining = parserf(text)
      if parsed ~= nil then
        return parsed, remaining
      end
    end
    return nil, text
  end
end

local function parse_pattern(pattern)
  return function(text)
    local patternhere = pattern
    if string.sub(patternhere, 1, 1) ~= "^" then
      patternhere = "^" .. patternhere
    end
    local matched = text:match(patternhere)
    if matched == nil then
      return nil, text
    end
    return matched, string.sub(text, matched:len()+1, text:len())
  end
end

local function parse_sequence(...)
  local parsers = {...}
  return function(text)
    local results = {}
    local remaining = text
    for k, parser in pairs(parsers) do
      local parsed, newremaining = parser(remaining)
      if parsed == nil then
        return nil, text
      end
      remaining = newremaining
      table.insert(results, parsed)
    end
    return results, remaining
  end
end

local function parser_map(p, f)
  return function(text)
    local parsed, remaining = p(text)
    if parsed ~= nil then
      return f(parsed), remaining
    end
    return nil, remaining
  end
end

local function ith(p, i)
  return parser_map(p, function(x) return x[i] end)
end

local function parse_separated_by(parserf, separatorf)
  return parser_map(
    parse_sequence(
      parserf,
      parse_many(ith(parse_sequence(separatorf, parserf), 2))
    ),
    function(x)
      if #x < 2 then
        return x
      end
      return {x[1], unpack(x[2])}
    end
  )
end

local whitespace_parser = parse_pattern(" +")
local int_parser = parse_pattern("%d+")
local float_parser = parse_pattern("%d+%.%d+")
local number_parser = parse_any(float_parser, int_parser)

-- these two can't be local
-- because they're mutually recursive
-- and late binding scope is weird
sexpr_parser = function(text)
  return parse_sequence(
    parse_pattern("%("),
    parse_separated_by(expr_parser, whitespace_parser),
    parse_pattern("%)")
  )(text)
end
expr_parser = parse_any(sexpr_parser, number_parser)

return {
  parse_many = parse_many,
  parse_any = parse_any,
  parse_pattern = parse_pattern,
  parse_sequence = parse_sequence,
  parse_separated_by = parse_separated_by,
  whitespace_parser = whitespace_parser,
  int_parser = int_parser,
  float_parser = float_parser,
  number_parser = number_parser,
  sexpr_parser = sexpr_parser,
}
