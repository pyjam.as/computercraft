#!/pix/plisp-19/bin/plisp

(def fp (pix_require "@libfunc"))
(def im (pix_require "@immutable"))
(def std (pix_require "@stdlib"))

(defn dbg (x) (std.pprint x) x)

(def monitor (peripheral.find "monitor"))

(def WIDTH HEIGHT (monitor.getSize))

(defn clear (_)
  (monitor.setBackgroundColor colors.brown)
  (monitor.clear))

(defn wait_for_any (...)
  -- wrapped parallel.waitForAny that keeps the result
  (def result nil)
  (def wrapped (fp.map
    {...}
    (fn (x) (fn (_) (defg result (x)) nil))
  ))
  (parallel.waitForAny (unpack wrapped))
  result)
  
(defn wait_for_tick (_)
  (sleep 0.02)
  {"tick"})
  
(defn wait_for_touch (_)
  (fp.pack (os.pullEvent "monitor_touch")))

(defn wait_for_event (_)
  (wait_for_any
    wait_for_tick
    wait_for_touch))
    
(defn ball_hit_paddle (ball paddle) 
  (and (== (math.floor ball.x) paddle.x)
       (and (<= (- paddle.y 3) ball.y)
            (<= ball.y (+ 3 paddle.y)))))
    
(defn bounce (ball paddle1 paddle2)
  (if (<= ball.x 2)
    (im.set ball "dx" (* -1 ball.dx))
  (if (>= ball.x WIDTH)
    (im.set ball "dx" (* -1 ball.dx))
  (if (<= ball.y 2)
    (im.set ball "dy" (* -1 ball.dy))
  (if (>= ball.y HEIGHT)
    (im.set ball "dy" (* -1 ball.dy))
  (if (or (ball_hit_paddle ball paddle1)
          (ball_hit_paddle ball paddle2))
    (im.set ball "dx" (* -1 ball.dx))
    ball))))))

(defn step_ball (ball dt)
  (im.set ball
    "x" (+ ball.x (* dt ball.dx))
    "y" (+ ball.y (* dt ball.dy))))

(defn event_for_player (event)
  -- returns player num (1 or 2) of touch event
  (def x event[3])
  (if x
      (if (< x (/ WIDTH 2))
          1
          2)
      nil))

(defn step_paddle (paddle event)
  (im.set paddle "y" event[4]))
      
(defn step_player (num paddle event)
  (if (== num (event_for_player event))
              (step_paddle paddle event)
              paddle))

(defn step (state event dt)
  (im.set state
    "ball" (step_ball (bounce state.ball state.paddle1 state.paddle2) dt)
    "paddle1" (step_player 1 state.paddle1 event)
    "paddle2" (step_player 2 state.paddle2 event)
    ))
  
(defn draw_ball (ball)
  (monitor.setCursorPos ball.x ball.y)
  (monitor.setBackgroundColor colors.white)
  (monitor.write " "))
  
(defn draw_paddle (paddle)
  (fp.map
    (std.table.range (- paddle.y 2) (+ paddle.y 2))
    (fn (y) 
      (monitor.setCursorPos paddle.x y)
      (monitor.setBackgroundColor colors.orange)
      (monitor.write " "))))

(defn draw (state)
  (clear)
  (draw_ball state.ball)
  (draw_paddle state.paddle1)
  (draw_paddle state.paddle2))

(defn loop (state before)
  (def event (wait_for_event))
  (def now (os.epoch "local"))
  (def dt (- now before))
  (def newstate (step state event dt))
  (draw newstate)
  (loop newstate now))

(clear)
 
(loop (im.set {}
       "ball" {dx=0.02,dy=0.01,x=5,y=5}
       "paddle1" (im.set {}
                   "x" 2 
                   "y" (/ HEIGHT 2))
       "paddle2" (im.set {} 
                   "x" (- WIDTH 2)
                   "y" (/ HEIGHT 2)))
      (os.epoch "local"))
