

local fp  = pix_require("@libfunc");
local im  = pix_require("@immutable");
local std  = pix_require("@stdlib");

local function dbg (x) std.pprint(x); return x end;

local monitor  = peripheral.find("monitor");

local WIDTH, HEIGHT  = monitor.getSize();

local function clear (_)
  monitor.setBackgroundColor(colors.brown);
  return monitor.clear() end;

local function wait_for_any (...)
  -- wrapped parallel.waitForAny that keeps the result
  local result  = nil;
  local wrapped  = fp.map(
    {...},
    (function (x) return (function (_) result  = x(); return nil end) end)
  );
  parallel.waitForAny(unpack(wrapped));
  return result end;
  
local function wait_for_tick (_)
  sleep(0.02);
  return {"tick"} end;
  
local function wait_for_touch (_)
  return fp.pack(os.pullEvent("monitor_touch")) end;

local function wait_for_event (_)
  return wait_for_any(
    wait_for_tick,
    wait_for_touch) end;
    
local function ball_hit_paddle (ball, paddle) 
  return ((math.floor(ball.x) == paddle.x)
       and (((paddle.y - 3) <= ball.y)
            and (ball.y <= (3 + paddle.y)))) end;
    
local function bounce (ball, paddle1, paddle2)
  return (function()if (ball.x <= 2) then 
    return im.set(ball, "dx", (-1 * ball.dx)) else 
  return (function()if (ball.x >= WIDTH) then 
    return im.set(ball, "dx", (-1 * ball.dx)) else 
  return (function()if (ball.y <= 2) then 
    return im.set(ball, "dy", (-1 * ball.dy)) else 
  return (function()if (ball.y >= HEIGHT) then 
    return im.set(ball, "dy", (-1 * ball.dy)) else 
  return (function()if (ball_hit_paddle(ball, paddle1)
          or ball_hit_paddle(ball, paddle2)) then 
    return im.set(ball, "dx", (-1 * ball.dx)) else 
    return ball end end)() end end)() end end)() end end)() end end)() end;

local function step_ball (ball, dt)
  return im.set(ball,
    "x", (ball.x + (dt * ball.dx)),
    "y", (ball.y + (dt * ball.dy))) end;

local function event_for_player (event)
  -- returns player num (1 or 2) of touch event
  local x  = event[3];
  return (function()if x then 
      return (function()if (x < (WIDTH / 2)) then 
          return 1 else 
          return 2 end end)() else 
      return nil end end)() end;

local function step_paddle (paddle, event)
  return im.set(paddle, "y", event[4]) end;
      
local function step_player (num, paddle, event)
  return (function()if (num == event_for_player(event)) then 
              return step_paddle(paddle, event) else 
              return paddle end end)() end;

local function step (state, event, dt)
  return im.set(state,
    "ball", step_ball(bounce(state.ball, state.paddle1, state.paddle2), dt),
    "paddle1", step_player(1, state.paddle1, event),
    "paddle2", step_player(2, state.paddle2, event)
    ) end;
  
local function draw_ball (ball)
  monitor.setCursorPos(ball.x, ball.y);
  monitor.setBackgroundColor(colors.white);
  return monitor.write(" ") end;
  
local function draw_paddle (paddle)
  return fp.map(
    std.table.range((paddle.y - 2), (paddle.y + 2)),
    (function (y) 
      monitor.setCursorPos(paddle.x, y);
      monitor.setBackgroundColor(colors.orange);
      return monitor.write(" ") end)) end;

local function draw (state)
  clear();
  draw_ball(state.ball);
  draw_paddle(state.paddle1);
  return draw_paddle(state.paddle2) end;

local function loop (state, before)
  local event  = wait_for_event();
  local now  = os.epoch("local");
  local dt  = (now - before);
  local newstate  = step(state, event, dt);
  draw(newstate);
  return loop(newstate, now) end;

clear();
 
loop(im.set({},
       "ball", {dx=0.02,dy=0.01,x=5,y=5},
       "paddle1", im.set({},
                   "x", 2, 
                   "y", (HEIGHT / 2)),
       "paddle2", im.set({}, 
                   "x", (WIDTH - 2),
                   "y", (HEIGHT / 2))),
      os.epoch("local"));