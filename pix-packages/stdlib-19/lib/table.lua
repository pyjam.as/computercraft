local lib = {}
-- concat was taken :shrug
-- note: doesn't deep copy.
function lib.merge(...)
  local result = {}
  for _, list in ipairs(arg) do 
    for _, e in ipairs(list) do
      table.insert(result, e) 
    end
  end
  return result
end

function lib.keys(list)
  local keys = {}
  for k, _ in pairs(list) do
    table.insert(keys, k)
  end
  return keys
end

function lib.contains(list, x)
  for _, v in pairs(list) do
    if v == x then return true end
  end
  return false
end

function lib.indexOf(list, value)
    for i, v in ipairs(list) do
        if v == value then
            return i
        end
    end
    return nil
end

function lib.clone(orig, copies)
  copies = copies or {}
  local orig_type = type(orig)
  local copy
  if orig_type == "table" then
    if copies[orig] then
      copy = copies[orig]
    else
      copy = {}
      copies[orig] = copy
      for orig_key, orig_value in next, orig, nil do
        copy[lib.clone(orig_key, copies)] = lib.clone(orig_value, copies)
      end
      setmetatable(copy, lib.clone(getmetatable(orig), copies))
    end
  else
    copy = orig
  end
  return copy
end

function lib.range(...)
  local args = {...}
  local result = {}
  local keep = function(i) table.insert(result, i) end
  if #args == 1 then
    for i=1,args[1] do keep(i) end
  elseif #args >= 2 then
    local step = args[3] or 1
    for i=args[1],args[2],step do keep(i) end
  end
  return result
end

return lib
