

local fp =  pix_require( "@libfunc");
pix_require( "@stdlib");

pprint(
  fp.map(
    {1,2,3},
    (function (x) return ( 2 * x)end)));