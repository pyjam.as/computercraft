pix_require "@stdlib"
local fp = pix_require "@libfunc"
local codegen = pix_require "codegen"


local function runplisp(plispcode, out)
  local luacode = codegen.plisp2lua(plispcode)
  local outpath = "/.gen.lua"
  if out ~= nil then
    outpath = out
  end
 
  local outfile = fs.open(outpath, "w")
  outfile.write(luacode)
  outfile.close()
  shell.run("/" .. outpath)
end

local function runplispfile(plispfilepath)
  local file = fs.open(plispfilepath, "r")
  local filecontent = file.readAll()
  
  -- figure out where to save stuff
  local pattern = "[^/]+$"
  local filename = plispfilepath:match(pattern)
  local outpath = plispfilepath:gsub(pattern, "").."."..filename..".lua"
  
  -- do the thang
  runplisp(filecontent, outpath)
end

local function main()
  if arg[1] == nil then
    -- TODO repl
    print("you need to provide plisp file")
  elseif fs.exists(arg[1]) then
    runplispfile(arg[1])
  elseif fs.exists(shell.resolve(arg[1])) then
    runplispfile(shell.resolve(arg[1]))
  else
    print("could not find file: " .. arg[1])
    error()
  end
end

main()

return {
  runplisp = runplisp
}
