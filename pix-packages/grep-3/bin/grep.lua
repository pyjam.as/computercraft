pix_require "@regex"

local pattern = arg[1]

if not pattern or not arg[2] then
  print("Usage: grep PATTERN FILE...")
  return
end

function grep(filename)
  f = fs.open(filename, "r")
  line_number = 1
  printed_filename = false
  while true do
    line = f.readLine()
    if line == nil then break end
    
    cursor = 1
    matches = find_all(pattern, line)  
    if #matches ~= 0 then
      if not printed_filename then
        term.setTextColor(colors.purple)
        print(filename)
        printed_filename = true
      end
      term.setTextColor(colors.green)
      write(("%d"):format(line_number)) 
      term.setTextColor(colors.white)
      write(": ")
    end
    for _, match in pairs(matches) do
      write(line:sub(cursor, match.from - 1))
      term.setTextColor(colors.red)
      write(line:sub(match.from, match.to))
      term.setTextColor(colors.white)
      cursor = match.to + 1
    end
    if #matches ~= 0 then
      write(line:sub(cursor, -1))
      print()
    end
  
    line_number = line_number + 1
  end
  f.close()
end

for i=2,#arg do
  local filename = shell.resolve(arg[i])
  for _, file in pairs(fs.find(filename)) do
    if not fs.isDir(file) then
      grep(file)
    end
  end
end
