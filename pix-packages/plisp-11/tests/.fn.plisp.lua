

local f = 
  (function (a, b, c) return print( a, b, c)end)
;

local g = 
  (function (a, b, c)
    print( a);
    print( b);
    print( c);
    return ( a .. b .. c)
  end)
;

f( "hi", "there", "buddy");

print( g(
  -- one
  "1",
  -- two
  "2",
  -- three
  "3"
));

local h =  (function (a) return unpack( a)end);
print( ( h( {1,2}) == 1));