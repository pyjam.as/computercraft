mode = arg[1]
filename = arg[2]

if filename == nil or not (mode == "bytes" or mode == "lines") then
  print("Usage: count bytes|lines FILENAME")
  return
end

f = fs.open(shell.resolve(filename), "r")

if mode == "bytes" then
  read = f.read
else
  read = f.readLine
end

result = 0
while true do
  if read() == nil then break end
  result = result + 1
end
print(result)
