
local test  = pix_require("@unittest");
local fp  = require("lib/init");

test("test map", (function (_) 
  local t  = {1,2,3};
  local mapped  = fp.map(t, (function (x) return (x * 2) end));
  test.assert_eq(2, mapped[1]);
  test.assert_eq(4, mapped[2]);
  return test.assert_eq(6, mapped[3]) end));
  
test("test all", (function (_) 
  local t  = {1,2,3};
  test.assert(fp.all(t, (function (e) return (e < 5) end)));
  return test.assert_not(fp.all(t, (function (e) return (e == 2) end))) end));

test:print_result();