function table.contains(list, x)
  for _, v in pairs(list) do
    if v == x then return true end
  end
  return false
end

function table.indexOf(list, value)
    for i, v in ipairs(list) do
        if v == value then
            return i
        end
    end
    return nil
end
