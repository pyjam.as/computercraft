pix_require "@stdlib"
fp = pix_require "@libfunc" 
lp = pix_require "@libparse"

local function is(x, t)
  if type(x) == t then
    return true
  elseif type(x) == "table" and x.tag == t then
    return true
  end
end

local function operator2lua(x, indent)
  local op = x[1][1]
  local args = fp.map(
    {unpack(x, 2)},
    function(e) return parsed_plisp2lua(e, indent) end
  )
  if #args == 0 then
    error("found 0 args in op expression: " .. textutils.serialize(x))
  elseif #args == 1 then
    return "(" .. op .. args[1] .. ")"
  elseif #args > 1 then
    return "(" .. op:join(args) .. ")"
  end
end

local function sexpr2lua(x, indent)
  local func = parsed_plisp2lua(x[1], indent)
  local args = fp.map(
    {unpack(x, 2)},
    function(e) return parsed_plisp2lua(e, indent) end
  )
  return func .. "(" .. (", "):join(args) .. ")"
end

local function fn2lua(x, indent)
  local tab = string.rep(" ", indent)
  local arglist = x[2]
  local body = fp.map(
    x[3],
    function(b) return parsed_plisp2lua(b, indent+2) end
  )
  local noreturn = {unpack(body, 1, #body - 1)}
  local returnval = body[#body]
  local first_nl = "\n"
  if #noreturn == 0 then
    -- one line function
    first_nl = ""
  end
  return (
    tab .. "function(" .. (", "):join(arglist) .. ")"
        .. first_nl .. ("\n"):join(noreturn)
        .. "\n" .. tab .. "  return " .. returnval
        .. "\n" .. tab .. "end")
end

local function def2lua(x, indent)
  local identifier = x[2]
  local expr = parsed_plisp2lua(x[3], indent)
  return string.rep(" ", indent) .. "local " .. identifier .. " = " .. expr
end

local function defg2lua(x, indent)
  local identifier = x[2]
  local expr = parsed_plisp2lua(x[3], indent)
  return string.rep(" ", indent) .. identifier .. " = " .. expr
end

-- must be global because of mutual recursion
function parsed_plisp2lua(x, indent)
  if is(x, "fn") then
    return fn2lua(x, indent)
  elseif is(x, "sexpr") and is(x[1], "operator") then
    return operator2lua(x, indent)
  elseif is(x, "identifier") then
    return x[1]
  elseif is(x, "def") then
    return def2lua(x, indent)
  elseif is(x, "defg") then
    return defg2lua(x, indent)
  elseif is(x, "sexpr") then
    return sexpr2lua(x, indent)
  elseif is(x, "number") then
    return textutils.serialize(x)
  elseif is(x, "string") then
    return textutils.serialize(x)
  else
    print("cannot parse", x)
    error()
  end
end

local function plisp2lua(plispcode)
  -- parse any plisp expression to a lua string
  local parsed, remaining = lp.expr_parser(plispcode)
  if remaining ~= "" then
    error("failed parsing plisp code")
  end
  local luacode = parsed_plisp2lua(parsed, 0)
  return luacode
end

local function runplisp(plispcode)
  local luacode = plisp2lua(plispcode)
  local outpath = "/.gen.lua"
  local outfile = fs.open(outpath, "w")
  outfile.write(luacode)
  outfile.close()
  shell.run(outpath)
end

local function runplispfile(plispfilepath)
  local file = fs.open(plispfilepath, "r")
  local filecontent = file.readAll()
  runplisp(filecontent)
end

local function main()
  if arg[1] ~= nil then
    runplispfile(shell.resolve(arg[1]))
  end
end

main()

return {
  plisp2lua = plisp2lua,
  runplisp = runplisp
}
