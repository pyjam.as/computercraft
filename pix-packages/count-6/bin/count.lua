mode = arg[1]

if arg[2] == nil or not (mode == "bytes" or mode == "lines") then
  print("Usage: count bytes|lines FILE...")
  return
end

files = {}
for i=2,#arg do
  local glob = shell.resolve(arg[i])
  for _, filename in pairs(fs.find(glob)) do
    if not fs.isDir(filename) then
      table.insert(files, filename)
    end
  end
end

total = 0
for _, filename in pairs(files) do
  f = fs.open(filename, "r")

  if mode == "bytes" then
    read = f.read
  else
    read = f.readLine
  end

  result = 0
  while true do
    if read() == nil then break end
    result = result + 1
  end
  total = total + result
  term.setTextColor(colors.lightGray)
  write(filename)
  write("\t")
  term.setTextColor(colors.white)
  write(result)
  write("\n")
end

if #files > 1 then
  term.setTextColor(colors.pink)
  write(total)
  term.setTextColor(colors.white)
  write(" " .. mode .. " total\n")
end
