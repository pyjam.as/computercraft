local std = pix_require "@stdlib"
local fp = pix_require "@libfunc"
local codegen = pix_require "codegen"

local DEBUG = true

local function compileplispfile(plisppath, outpath)
  local plispfile = fs.open(plisppath, "r")
  local plispcode = plispfile.readAll()
  
  local luacode = codegen.plisp2lua(plispcode, plisppath)

  local outfile = fs.open(outpath, "w")
  outfile.write(luacode)
  outfile.close()
end

local function runplispfile(plisppath)
  -- figure out where to save stuff
  local pattern = "[^/]+$"
  local filename = plisppath:match(pattern)
  local outpath = plisppath:gsub(pattern, "").."."..filename..".lua"
  
  -- do the thang
  compileplispfile(plisppath, outpath)
  shell.run("/" .. outpath)
end

local function run_repl_line(plispcode)
  local luacode = codegen.plisp2lua(plispcode)
  if DEBUG then 
    print("luacode: ", luacode)
  end
  
  -- this fails if the plisp compiles to a lua statement
  local result, err = load("return " .. luacode)
  
  -- because `return a = 2` is bad lua syntax
  
  -- ...so if the code is a statement, just drop the value
  if result == nil then
    result, err = load(luacode)
  end
  
  -- TODO: require? pix_require?
  if result == nil then
    print(err)
    error()
  else 
    std.pprint(result())
  end
end

local function repl()
  -- quick and dirty repl
  while true do
    write("plisp> ")
    local plispcode = read()
    local ok, err = pcall(run_repl_line, plispcode)
  end
end

local function main()
  if arg[1] == nil then
    repl()
  elseif fs.exists(arg[1]) then
    runplispfile(arg[1])
  elseif fs.exists(shell.resolve(arg[1])) then
    runplispfile(shell.resolve(arg[1]))
  else
    print("could not find file: " .. arg[1])
    error()
  end
end

main()

return {
  runplisp = runplisp
}
