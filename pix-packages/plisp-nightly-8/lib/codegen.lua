local codegen = {}
local std = pix_require "@stdlib"
local fp = pix_require "@libfunc" 
local lp = pix_require "@libparse"
local parser = pix_require "parser"
pix_require "@libpager"

local scroll = function(x)
  pager(std.string.split(
    textutils.serialize(x, {allow_repetitions=true}),
    "\n"
  ))
end

local locline = function(loc)
  local content
  if loc.file ~= nil then
    local file = fs.open(loc.file, "r")
    content = file.readAll()
  elseif loc.code ~= nil then
    content = loc.code
  else
    return nil
  end
  local lines = std.string.split(content, "\n")
  return lines[loc.line]
end

local printlocblock = function(loc)
  local line = locline(loc)
  if line == nil then
    return
  end
  
  term.setTextColor(colors.cyan)
  write("|Line " .. loc.line .. "\n")
  
  write("|")
  term.setTextColor(colors.lightGray)
  print(line)
  term.setTextColor(colors.white)
  
  term.setTextColor(colors.cyan)
  write("|")
  term.setTextColor(colors.white)
  write(string.rep(" ", loc.col))
  term.setTextColor(colors.red)
  write("^\n")
  term.setTextColor(colors.white)
end

local compileerror = function(x, msg)
  local loc = x.loc
  print(msg)
  write("\n")
  printlocblock(loc)
  error()
end

local operators = {
  -- arithmetic operators
  "+", "-", "*", "//", "/", "%", "^",
  -- bitwise operators
  "&", "|", "~", ">>", "<<",
  -- relational operators
  "==", "~=", "<=", ">=", "<", ">",
  -- logical operators
  "and", "not", "or",
  -- concat
  ".."
}

local is_operator = function(x)
  return x.tag == "sexpr"
     and table.contains(operators, x.thing[1].thing[2].thing)
end

local unaryop2lua = function(x)
  compileerror(x, "unary operators not implemented")
end

local op2lua = function(x)
  local op = x.thing[1]
  local opstr = op.thing[2].thing
  local args = {unpack(x.thing, 2)}
  if #args == 1 then
    return unaryop2lua(x)
  end
  local firstarg = args[1]
  local restargs = {unpack(args, 2)}
  local firstargstr = firstarg.thing[1].thing
                      .. codegen._plisp2lua(firstarg.thing[2])
                      .. firstarg.thing[3].thing
                      
  local restargstrs = fp.map(
    restargs,
    function(a)
      return a.thing[1].thing
             .. opstr .. " " .. codegen._plisp2lua(a.thing[2])
             .. a.thing[3].thing
    end
  )
  local allargstr = firstargstr .. fp.fold(
    restargstrs,
    function(a,b) return a..b end,
    ""
  )
  return "(" .. op.thing[1].thing .. op.thing[3].thing
             .. allargstr .. ")"
end

local funcis = function(thing)
  return function(x)
    return x.tag == "sexpr" and x.thing[1].thing[2].thing == thing
  end
end

local is_if = funcis("if")

local if2lua = function(x)
  if #(x.thing) ~= 4 then
    compileerror(x,
      "if must contain 3 expressions:\n" ..
      "(if <condition> <if-true> <if-false>)"
    )
  end
  local ifb = x.thing[1]
  local cond = x.thing[2]
  local trueb = x.thing[3]
  local falseb = x.thing[4]
  return "(function()" .. ifb.thing[1].thing .. "if" .. ifb.thing[3].thing
         .. cond.thing[1].thing
         .. codegen._plisp2lua(cond.thing[2])
         .. " then "
         .. cond.thing[3].thing
         .. trueb.thing[1].thing
         .. "return " .. codegen._plisp2lua(trueb.thing[2])
         .. " else "
         .. trueb.thing[3].thing
         .. falseb.thing[1].thing
         .. "return " .. codegen._plisp2lua(falseb.thing[2])
         .. falseb.thing[3].thing .. "end end)()"
end

local is_def = funcis("def")
local is_defg = funcis("defg")

local makedef = function(keywordname, prefix)
  return function(x)
    if (#x.thing) ~= 3 then
      compileerror(x,
        "`" .. keywordname .. "` takes two arguments\n" ..
        "(" .. keywordname .. " <name> <value>)"
      )
    end
    local def = x.thing[1]
    local name = x.thing[2]
    local value = x.thing[3]
    if name.thing[2].tag ~= "primitive" then
      compileerror(name,
        "first argument to `"..keywordname.."` must be an identifier\n"..
        "got " .. textutils.serialize(name.tag)
      )
    end
    return prefix .. name.thing[1].thing ..
           codegen._plisp2lua(name.thing[2]) ..
           " = " ..
           name.thing[3].thing ..
           value.thing[1].thing ..
           codegen._plisp2lua(value.thing[2]) ..
           value.thing[3].thing
  end
end

local def2lua = makedef("def", "local ")
local defg2lua = makedef("defg", "")

local is_fn = funcis("fn")

local fn2lua = function(x)
  if #(x.thing) < 3 then
    compileerror(x,
      "`fn` must have parameters and at least one body\n"..
      "(fn <params> ... <body>)\n" ..
      "example: (fn (a b) (+ a b))"
    )
  end
  local fn = x.thing[1]
  local args = x.thing[2]
  local bodies = {unpack(x.thing, 3, #(x.thing) - 1)}
  local returnbody = x.thing[#(x.thing)]
  
  local argstrlist = fp.map(
    {unpack(args.thing[2].thing, 1, #(args.thing[2].thing)-1)},
    function(a)
      return a.thing[1].thing ..
             codegen._plisp2lua(a.thing[2]) .. "," ..
             a.thing[3].thing
    end
  )
  local argstr = fp.fold(argstrlist, function(a,b) return a..b end, "")
  local lastarg = args.thing[2].thing[#(args.thing[2].thing)]
  argstr = argstr
           .. lastarg.thing[1].thing
           .. codegen._plisp2lua(lastarg.thing[2])
           .. lastarg.thing[3].thing
  
  local bodiesstrlist = fp.map(
    bodies,
    function(b)
      return b.thing[1].thing ..
             codegen._plisp2lua(b.thing[2]) .. ";" ..
             b.thing[3].thing
    end
  )
  local bodiesstr = ""
  if #bodiesstrlist > 0 then
    bodiesstr = fp.fold(bodiesstrlist,function(a,b)return a..b end, "")
  end
  
  local returnbodystr = returnbody.thing[1].thing
                        .. codegen._plisp2lua(returnbody.thing[2])
                        .. returnbody.thing[3].thing
  
  return "(" .. fn.thing[1].thing
         .. "function" .. fn.thing[3].thing
         .. args.thing[1].thing
         .. "("..argstr..")"
         .. args.thing[3].thing
         .. bodiesstr
         .. "return " .. returnbodystr 
         .. " end)"
end


local sexpr2lua = function(x)
  local func = x.thing[1]
  local args = {unpack(x.thing, 2)}
  local firstargs = {unpack(args, 1, #args-1)}
  local lastarg = args[#args]
  local argstrs = fp.map(
    firstargs,
    function(a)
      return a.thing[1].thing
          .. codegen._plisp2lua(a.thing[2])
          .. ","
          .. a.thing[3].thing
    end
  ) 
  local lastargstr = ""
  if lastarg ~= nil then
    lastargstr = lastarg.thing[1].thing 
                 .. codegen._plisp2lua(lastarg.thing[2])
                 .. lastarg.thing[3].thing
  end
  local argstr = fp.fold(
    argstrs,
    function(a,b) return a .. b end,
    ""
  ) .. lastargstr
  return func.thing[1].thing
         .. codegen._plisp2lua(func.thing[2])
         .. "("
         .. func.thing[3].thing 
         .. argstr .. ")"
end

local module2lua = function(x)
  return string.join(
    "",
    fp.map(
      x.thing,
      function(e)
        return e.thing[1].thing
               .. codegen._plisp2lua(e.thing[2]) .. ";"
               .. e.thing[3].thing
      end
    )
  )
end

codegen._plisp2lua = function(x)
  if x.tag == "primitive" then
    return x.thing
  elseif is_operator(x) then
    return op2lua(x)
  elseif is_if(x) then
    return if2lua(x)
  elseif is_def(x) then
    return def2lua(x)
  elseif is_fn(x) then
    return fn2lua(x)
  elseif is_defg(x) then
    return defg2lua(x)
  elseif x.tag == "sexpr" then
    return sexpr2lua(x)
  elseif x.tag == "module" then
    return module2lua(x)
  elseif x.tag == "noncode" then
    return x.thing
  else
    print("Unknown type: " .. textutils.serialize(x.tag))
    error()
  end
end

local parseerror = function(loc, msg, remaining)
  -- this error is still very shoddy
  local gotchar = string.sub(remaining,1,1)
  local codeline = locline(loc)
  print(
    "Failed parsing.\n" ..
    "It's probably mismatched parens...\n" ..
    "Error happened near here:\n"
  )
  printlocblock(loc)
  error()
end

local function plisp2lua(plispcode, filename)
  -- parse any plisp string to a lua string
  
  -- init location tracker (for error stuff)
  local initloc =  {col=1,line=1}
  if filename ~= nil then
    initloc.file = filename
  else
    initloc.code = plispcode
  end
  
  -- do the parse
  local parsed, remaining, loc = parser.module(plispcode, initloc)
  if remaining ~= "" then
    parseerror(loc, msg, remaining)
  end
  
  -- make lua code from parsed plisp
  return codegen._plisp2lua(parsed)
end

return {
  plisp2lua = plisp2lua
}
