function noop_yield()
  os.queueEvent("async-noop")
  os.pullEvent("async-noop")
end

function after_colon(str)
  local start = str:find(":") or 0
  return str:sub(start + 1, -1) 
end

function index()
  local items = {}
  local chests = { peripheral.find("minecraft:chest") }
  local count = 0
  
  print("Indexing chests..")
  for _, chest in pairs(chests) do
    noop_yield()
    local chest_name = peripheral.getName(chest)
    for slot, item in pairs(chest.list()) do
      local item_name = after_colon(item.name)
      if not items[item_name] then
        items[item_name] = {}
      end
      table.insert(
        items[item_name],
        {  chest=chest_name,
           slot=slot,
           count=item.count
        }
      )
    end
    count = count + 1
  end
  return items
end
