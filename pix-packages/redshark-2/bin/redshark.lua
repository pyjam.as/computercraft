local pretty = require "cc.pretty"

local modem = peripheral.find("modem")

if (not modem) then
  print("You must have a modem to use redshark!")
  error()
end

function is_normal_rednet(to, from, msg)
  if not msg.nMessageID or type(msg.nMessageID) ~= "number" then
    return false
  elseif not msg.nRecipient then
    return false
  elseif not msg.nSender then
    return false
  elseif not msg.message then
    return false
  end
  
  if msg.nRecipient ~= to then
    return false
  elseif msg.nSender ~= from then
    return false
  end
  
  return true
end

-- FIXME: this is a hack
modem.closeAll()
-- By default we listen on ports 0..=100
for i = 0, 100 do
  modem.open(i)
end

while true do
  _event, _side, to, from, msg, distance = os.pullEvent("modem_message")
  
  if is_normal_rednet(to, from, msg) then
    if msg.sProtocol then
      term.setTextColor(colors.red)
      write("["..from.."->"..to.."] ")
      term.setTextColor(colors.orange)
      print(msg.sProtocol)
      term.setTextColor(colors.white)
      pretty.pretty_print(msg.message)
    else
      term.setTextColor(colors.red)
      write("["..from.."->"..to.."] ")
      term.setTextColor(colors.white)
      pretty.pretty_print(msg.message)
    end
  else
    term.setTextColor(colors.yellow)
    write("["..from.."->"..to.."] ")
    term.setTextColor(colors.white)
    pretty.pretty_print(msg.message)
  end
  -- TODO: log distance
end

