local codegen = {}
pix_require "@stdlib"
local fp = pix_require "@libfunc" 
local lp = pix_require "@libparse"
local parser = require "parser"

local operators = {
  -- arithmetic operators
  "+", "-", "*", "//", "/", "%", "^",
  -- bitwise operators
  "&", "|", "~", ">>", "<<",
  -- relational operators
  "==", "~=", "<=", ">=", "<", ">",
  -- logical operators
  "and", "not", "or",
  -- concat
  ".."
}

local is_operator = function(x)
  return x.tag == "sexpr"
     and table.contains(operators, x.thing[1].thing[2].thing)
end

local unaryop2lua = function(x)
  print("Fatal: unary operators not implemented")
  error()
end

local op2lua = function(x)
  local op = x.thing[1]
  local opstr = op.thing[2].thing
  local args = {unpack(x.thing, 2)}
  if #args == 1 then
    return unaryop2lua(x)
  end
  local firstarg = args[1]
  local restargs = {unpack(args, 2)}
  local firstargstr = firstarg.thing[1].thing
                      .. codegen._plisp2lua(firstarg.thing[2])
                      .. firstarg.thing[3].thing
                      
  local restargstrs = fp.map(
    restargs,
    function(a)
      return a.thing[1].thing
             .. opstr .. codegen._plisp2lua(a.thing[2])
             .. a.thing[3].thing
    end
  )
  local allargstr = firstargstr .. fp.fold(
    restargstrs,
    function(a,b) return a..b end,
    ""
  )
  return "(" .. op.thing[1].thing .. op.thing[3].thing
             .. allargstr .. ")"
end

local sexpr2lua = function(x)
  local func = x.thing[1]
  local args = {unpack(x.thing, 2)}
  local firstargs = {unpack(args, 1, #args-1)}
  local lastarg = args[#args]
  local argstrs = fp.map(
    firstargs,
    function(a)
      return a.thing[1].thing
          .. codegen._plisp2lua(a.thing[2])
          .. ","
          .. a.thing[3].thing
    end
  ) 
  local lastargstr = lastarg.thing[1].thing 
                     .. codegen._plisp2lua(lastarg.thing[2])
                     .. lastarg.thing[3].thing
  local argstr = fp.fold(
    argstrs,
    function(a,b) return a .. b end,
    ""
  ) .. lastargstr
  return "(" .. func.thing[1].thing
             .. codegen._plisp2lua(func.thing[2])
             .. func.thing[3].thing 
             .. "(" .. argstr .. "))"
end

local module2lua = function(x)
  return string.join(
    "",
    fp.map(
      x.thing,
      function(e)
        return e.thing[1].thing
               .. codegen._plisp2lua(e.thing[2])
               .. e.thing[3].thing
      end
    )
  )
end

codegen._plisp2lua = function(x)
  if x.tag == "primitive" then
    return x.thing
  elseif is_operator(x) then
    return op2lua(x)
  elseif x.tag == "sexpr" then
    return sexpr2lua(x)
  elseif x.tag == "module" then
    return module2lua(x)
  elseif x.tag == "noncode" then
    return x.thing
  else
    print("Unknown type: " .. textutils.serialize(x.tag))
    error()
  end
end

local function plisp2lua(plispcode)
  -- parse any plisp string to a lua string
  local parsed, remaining, loc = parser.module(plispcode, {col=1,line=1})
  if remaining ~= "" then
    local gotchar = string.sub(remaining,1,1)
    print(
      "Failed parsing.\n"
      .. "Syntax error near line:"..loc.line.." col:"..loc.col.."\n"
      .. "Got unexpected: " .. gotchar
    )
    error()
  end
  return codegen._plisp2lua(parsed)
end

return {
  plisp2lua = plisp2lua
}
