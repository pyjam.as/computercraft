local PROTOCOL = "pix"


function connect()
  peripheral.find("modem", rednet.open)
  local pix_server = rednet.lookup(PROTOCOL, "server")
  if not pix_server then
    print("Couldn't reach pix server, is a modem connected?")
    error()
  end
  return pix_server
end

function pull(package_name)
  local pix_server = connect()
  rednet.send(pix_server, {action="pull", package_name=package_name} ,PROTOCOL)
  _, data = rednet.receive(protocol)
  if data.error then
    print("Failed to pull package: " .. package_name) 
    print("pix server replied: " .. data.error) 
    error()
  end
  return data
end

function push(package_name, content)
  local pix_server = connect()
  rednet.send(pix_server, {action="push", package_name=package_name, content=content}, PROTOCOL) 
  local _, result = rednet.receive(PROTOCOL)
  return result
end

function get_all_packages()
  return get_packages(true)
end

-- get list of the latest version of packages on server.
-- or all packages
-- format: {[package_name-version] = {deserialised package.pix}, ... }
function get_packages(all)
  local pix_server = connect()
  -- Only latest if not all
  local action = "list"
  if all then action = "list_all" end
  rednet.send(pix_server, {action=action}, PROTOCOL) 
  local _, result = rednet.receive(PROTOCOL)
  if result.error then
    print("failed to list: " .. result.error)
    return
  end
  return result.packages
end

function resolve_dependencies(package_name, deps)
  deps = deps or {}
  local packages = get_all_packages()
  if not packages[package_name] then
    print("Package "..package_name.." doesn't exist")
    error()
  end
  for _, dep in ipairs(packages[package_name].dependencies) do
    deps[dep] = true
    resolve_dependencies(dep, deps) 
  end
  return table.keys(deps)
end

function list_all(path, paths)
  for _, sub_path in ipairs(fs.list(path)) do
    sub_path = fs.combine(path, sub_path)
    if fs.isDir(sub_path) then
      list_all(sub_path, paths)
    else
      table.insert(paths, sub_path)
    end
  end
end

function read_config(config_path)
  if not fs.exists(config_path) or fs.isDir(config_path) then
    print(config_path .. " doesn't exist. Please create it.")
  end
  local file = fs.open(config_path, "r")
  local config = file.readAll()
  file.close()
  local config, err = textutils.unserialiseJSON(config)
  if not config then
    print("Error parsing "..config_path..": "..err)
    error()
  end
  return config
end

function read_package(package_path)
  local config_path = fs.combine(package_path, "package.pix")
  if not fs.exists(config_path) then
    return nil, "Package at \"" .. package_path .. "\" has no package.pix file"
  end
  
  local file = fs.open(config_path, "r")
  local package_config = file.readAll()
  file.close()
  local package_config, err = textutils.unserialiseJSON(package_config) 
  if err then
    return nil, "Package at \"" .. package_path .. "\" has an invalid package.nix\n"..err
  end
  return package_config
end

function parse_package_name(package_name)
  local name, version = package_name:match("^([%w%-]+)%-(%w+)$")
  if version ~= "latest" then
    version = tonumber(version)
  end
  return name, version
end
