-- bad pager function
-- forhåbenligt gør nogen den bedre

local SCREEN_WIDTH, SCREEN_HEIGHT = term.getSize()

local function countDigit(n) 
  if n == 0 then
    return 1
  end
  local count = 0
  while n ~= 0 do
    n = math.floor(n / 10);
    count = count + 1
  end
  return count; 
end


local function render_with_line_numbers(buffer, scroll_offset)
  local line_num_width = countDigit(#buffer)+1
  local max_line_len = SCREEN_WIDTH - line_num_width
  local max_lines = SCREEN_HEIGHT-1
  local render_line_nums = {}
  local render_lines = {}
  
  -- preprocess to handle wrapping
  for i=1, #buffer do
    local line_num = i
    local line_text = buffer[line_num]
    table.insert(render_line_nums, line_num)
    while true do
      table.insert(render_lines, string.sub(line_text, 1, max_line_len))
      if string.len(line_text) > max_line_len then
        line_text = string.sub(line_text, max_line_len + 1, -1)
        table.insert(render_line_nums, 0)
      else
        break
      end
    end
  end

  -- add max_wrap phantom lines
  for i=0,max_lines,1 do
    table.insert(render_line_nums, -1)
    table.insert(render_lines, "~")
  end

  term.clear()
  -- render lines
  for i=1,max_lines,1 do
    local line_num = render_line_nums[i+scroll_offset]
    local line = render_lines[i+scroll_offset]
    -- wrapped lines don't have a line number
    if line_num > 0 then
      -- normal line
      term.setCursorPos(line_num_width-countDigit(line_num), i)
      term.setTextColour(colors.yellow)
      _write(("%d "):format(line_num or 0))
      term.setTextColour(colors.white)  
    elseif(line_num == 0) then
      -- wrapped line
      term.setCursorPos(line_num_width+1, i)
    elseif(line_num == -1) then
      -- phantom line, render nothing
      term.setCursorPos(1, i)
    end
    term.write(line)
  end
end


local function render_simple(buffer, scroll_offset)
  local SCREEN_WIDTH, SCREEN_HEIGHT = term.getSize()
  local max_lines = SCREEN_HEIGHT-1

  -- render lines
  term.clear()
  for i=1,max_lines,1 do
    term.setCursorPos(1, i)
    local line = buffer[i+scroll_offset]
    if type(line) == "string" then
      term.write(line .. "\n")
    else
      for _, v in pairs(line) do
        if type(v) == "number" then
          term.setTextColor(v)
        else
          term.write(v)
        end
      end
      term.write("\n")
    end
  end
end


function pager(buffer, show_numbers)
  -- buffer is a list of string
  -- when rendering without line numbers, the buffer can also
  --   contain tables with strings and colors
  
  local scroll_offset = 0

  while true do
    if show_numbers then
      render_with_line_numbers(buffer, scroll_offset)
    else
      render_simple(buffer, scroll_offset)
    end
    local event, key
    repeat 
      event, key = os.pullEvent()
      if event == "key_up" then
        local name = keys.getName(key)
        -- attempt do de-duplicate char events
        if not name or string.len(name) == 1 then
          event = nil
        end
        key = name
      end
    until event == "key_up" or event == "char"
    
    -- handle input
    if key == "j" then
      scroll_offset = math.min(scroll_offset + 1, #buffer - SCREEN_HEIGHT + 1)
    elseif key == "k" then
      scroll_offset = math.max(scroll_offset - 1, 0)
    elseif key == "q" then
      term.setCursorPos(1, 1)
      term.clear()
      error()
    end
  end
end
