-- regex functions
--
--   match(pattern, text) -- return whether beginning of text matches pattern
--   find_all(pattern, text) -- find all non-overlapping matches in text
--
-- supports:
--   . (any character)
--   * (zero or more occurences)
--   + (one or more occurences)
--   ? (zero or one occurences)
--   $ (end of text)
--   ^ (beginning of text)


function _match_star(char, pattern, text, offset)
  for i = offset, #text do
    local to = _match_here(pattern, text, i)
    if to then
      return to
    end

    local next_char = text:sub(i+1, i+1)
    if text:sub(i, i) ~= char and char ~= "." then
      break
    end
  end
  return nil
end


function _match_here(pattern, text, offset)
  -- match beginning of text from offset
  -- return nil or index of end of match
  if #pattern == 0 then
    return offset - 1
  elseif pattern:sub(2, 2) == "*" then
    return _match_star(pattern:sub(1, 1), pattern:sub(3, -1), text, offset)
  elseif pattern:sub(2, 2) == "+" then
    local char = pattern:sub(1, 1)
    if text:sub(offset, offset) == char or char == "." then
      return _match_star(char, pattern:sub(3, -1), text, offset + 1)
    end
    return nil
  elseif pattern:sub(2, 2) == "?" then
    if text:sub(offset, offset) == "." or text:sub(offset, offset) == pattern:sub(1, 1) then
      local greedy_pattern = pattern:sub(3, -1)
      local greedy = _match_here(greedy_pattern, text, offset + 1)
      if greedy then
        return greedy
      end
    end
    return _match_here(pattern:sub(3, -1), text, offset)
  elseif #pattern == 1 and pattern:sub(1, 1) == "$" then
    if offset == #text + 1 then
      return offset
    end
    return nil
  elseif (pattern:sub(1, 1) == "." or pattern:sub(1, 1) == text:sub(offset, offset)) then
    return _match_here(pattern:sub(2, -1), text, offset + 1)
  else
    return nil
  end
end


function match(pattern, text)
  -- return true iff beginning of text matches pattern
  if pattern:sub(1, 1) == "^" then
    pattern = pattern:sub(2, -1)
  end

  if _match_here(pattern, text, 1) then
    return true
  else
    return false
  end
end


function find_all(pattern, text)
  -- return all non-overlapping matches in a text
  -- the returned item is a list where all elements have a "from" and "to"
  matches = {}

  if pattern:sub(1, 1) == "^" then
    if _match_here(pattern:sub(2, -1), text, 1) then
      table.insert(matches, { from = 1, to = #text })
    end
    return matches
  end

  local skip_until = 0
  for i = 1, #text do
    if i > skip_until then
      to = _match_here(pattern, text, i)
      if to then
        skip_until = to
        table.insert(matches, { from = i, to = to })
      end
    end
  end

  return matches
end
