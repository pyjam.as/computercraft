local function rotl(value, count)
  return bit.bor(bit.blshift(value, count), bit.brshift(value, (32 - count)))
end


function scha1(message)
  -- broken sha1 implementation :-)
  -- https://en.wikipedia.org/wiki/SHA-1
  local ml = #message * 8

  message = message .. string.char(0x80) .. string.rep(string.char(0), -(#message + 9) % 64) .. string.char(0, 0, 0, 0, bit.brshift(bit.band(ml, 0xff000000), 24), bit.brshift(bit.band(ml, 0xff0000), 16), bit.brshift(bit.band(ml, 0xff00), 8), bit.band(ml, 0xff))

  local h0 = 0x67452301
  local h1 = 0xEFCDAB89
  local h2 = 0x98BADCFE
  local h3 = 0x10325476
  local h4 = 0xC3D2E1F0

  local w = {}

  for chunk_i = 1, #message, 64 do
    local offset = chunk_i
    for i=0, 15 do
      local b0, b1, b2, b3 = string.byte(message, offset, offset + 3)
      w[i] =  b0 * 0x1000000 + b1 * 0x10000 + b2 * 0x100 + b3
      offset = offset + 4
    end

    for i=16, 79 do
      w[i] = rotl(bit.bxor(w[i - 3], w[i - 8], w[i - 14], w[i - 16]), 1)
    end

    local a = h0
    local b = h1
    local c = h2
    local d = h3
    local e = h4

    for i=0, 79 do
      local f, k
      if i <= 19 then
          f = bit.bor(bit.band(b, c), bit.band(bit.bnot(b), d))
	  k = 0x5A827999
      elseif i <= 39 then
	  f = bit.bxor(b, c, d)
	  k = 0x6ED9EBA1
      elseif i <= 59 then
	  f = bit.bor(bit.band(b, c), bit.band(b, d), bit.band(c, d))
	  k = 0x8F1BBCDC
      else
	  f = bit.bxor(b, c, d)
	  k = 0xCA62C1D6
      end

      local temp = (rotl(a, 5) + f + e + k + w[i]) % 4294967296
      e = d
      d = c
      c = rotl(b, 30)
      b = a
      a = temp
    end

    h0 = (h0 + a) % 4294967296
    h1 = (h1 + b) % 4294967296
    h2 = (h2 + c) % 4294967296
    h3 = (h3 + d) % 4294967296
    h4 = (h4 + e) % 4294967296
  end

  return string.format("%08x%08x%08x%08x%08x", h0, h1, h2, h3, h4):lower()
end
