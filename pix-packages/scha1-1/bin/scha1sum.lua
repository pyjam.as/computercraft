pix_require("init")

filename = arg[1]

if filename == nil then
  print("Usage: scha1sum FILENAME")
  return
end

content = ""
f = fs.open(shell.resolve(filename), "r")
while true do
  line = f.readLine()
  if line == nil then break end
  content = content .. line
end
f.close()

print(scha1(content))
