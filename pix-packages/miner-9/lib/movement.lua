inverses = {
  back="forward",
  forward="back",
  left="right",
  right="left",
  up="down",
  down="up"
}

ores = {diamond=0, gold=0, copper=0, redstone=0, lapis=0}

function inspect(move)
  local inspectFunc = nil
  if move == "forward" then
    inspectFunc = turtle.inspect
  elseif move == "up" then
    inspectFunc = turtle.inspectUp
  elseif move == "down" then
    inspectFunc = turtle.inspectDown
  else
    error("Invalid dig: " .. move)
  end
  return inspectFunc()
end

function isBlocked(move)
  hasBlock, block = inspect(move)
  if not hasBlock then return false end
  if block.name == "minecraft:water" or block.name == "minecraft:lava" then
    return false
  end
  -- ore stats
  if string.find(block.name, "diamond") then
    ores.diamond = ores.diamond + 1
  elseif string.find(block.name, "gold") then
    ores.gold = ores.gold + 1
  elseif string.find(block.name, "lapis") then
    ores.lapis = ores.lapis + 1
  elseif string.find(block.name, "redstone") then
    ores.redstone = ores.redstone + 1
  elseif string.find(block.name, "copper") then
    ores.copper = ores.copper + 1
  end
  return true
end    

function dig(move)
  if move == "forward" then
    turtle.dig()
  elseif move == "up" then
    turtle.digUp()
  elseif move == "down" then
    turtle.digDown()
  else
    error("Invalid dig: " .. move)
  end
end

function doMove(move, times)
  times = times or 1
  for _=1,times do
    if move == "forward" or move == "down" or move == "up" then
      -- handle blocked
      while isBlocked(move) do
        turtle.select(1)
        dig(move)
      end
    end
    local moveFunc = nil
    if move == "forward" then
      moveFunc = turtle.forward  
    elseif move == "back" then
      moveFunc = turtle.back 
    elseif move == "left" then
      moveFunc = turtle.turnLeft 
    elseif move == "right" then
      moveFunc = turtle.turnRight 
    elseif move == "up" then
      moveFunc = turtle.up 
    elseif move == "down" then
      moveFunc = turtle.down 
    else
      error("Invalid move: " .. move)
    end
    local ok = moveFunc()
    if not ok then error("Failed to move: " .. move) end
  end
end

function reverseMoves(moves)
  local reverse = {}
  table.insert(reverse, "right")
  table.insert(reverse, "right")
  for i=#moves,1,-1 do
    local move = moves[i]
    if move == "forward" or move == "back" then
      table.insert(reverse, move)
    else
      table.insert(reverse, inverses[move])
    end
  end
  table.insert(reverse, "right")
  table.insert(reverse, "right")
  return reverse
end
