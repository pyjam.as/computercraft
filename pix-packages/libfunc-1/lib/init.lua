local function fold(acc, f, initial)
  local result = initial
  for _,v in ipairs(acc) do
    result = f(result, v)
  end
  return result
end


local function map(list, f)
  local result = {}
  for k,v in ipairs(list) do
    result[k] = f(v)
  end
  return result
end


local function filter(list, f)
  local result = {}
  for _,v in ipairs(list) do
    if f(v) then
      table.insert(result, v)
    end
  end
  return result
end


local function sort_by(list, f)
  table.sort(
    list,
    function(a, b) return f(a) < f(b) end
  )
  return list
end

return {
  map = map,
  filter = filter,
  fold = fold,
  sort_by = sort_by
}
