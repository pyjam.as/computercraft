local parser = {}
local lp = pix_require "@libparse"
local fp = pix_require "@libfunc"

local pprint = function(...) print(textutils.serialize(...)) end

parser.whitespace = lp.parse_pattern("[ \n]+")
parser.comment = lp.parse_pattern("%-%-[^\n]*\n")
parser.shebang = lp.parser_map(
  lp.parse_pattern("#![^\n]+\n"),
  function() return "\n" end
)
parser.nothing = lp.parse_pattern("")

-- lol this whole thing could probably have been a pattern
local _noncode = lp.parser_map(
  lp.parse_many(lp.parse_any(
    parser.whitespace,
    parser.comment,
    parser.shebang
  )),
  function(thing)
    -- concat strings
    return fp.fold(
      fp.map(thing, function(x) return x.thing end),
      function(a, b) return a .. b end
    )
  end
)
parser.noncode = lp.tag("noncode", lp.parse_any(_noncode, parser.nothing))

local mapconcat = function(p)
  return lp.parser_map(
    p,
    function(e)
      return fp.fold(e, function(a,b) return a..b.thing end, "")
    end
  )
end

parser.string = mapconcat(lp.parse_sequence(
  lp.parse_pattern("\""),
  mapconcat(lp.parse_many(lp.parse_any(
    lp.parse_pattern("\\."),  -- an escaped character
    lp.parse_pattern("[^\"\\]") -- anything but a quote or backslash
  ))),
  lp.parse_pattern("\"")
))

-- any non-nested value
-- e.g. "1", "nil", "a.b.c[24]"
-- basically anything that's not whitespace or parens
parser.primitive = lp.tag(
  "primitive",
  lp.parse_any(
    parser.string,
    lp.parse_pattern("[^%(%) \n]+")
  )
)

-- an s-expression is a
-- whitespace/comment-separated, parentheses-delimited,
-- list of expressions
parser.sexpr = function(...)
  local with_parens = lp.parse_sequence(
    lp.parse_pattern("%("),
    lp.parse_many(lp.parse_sequence(
      parser.noncode,
      parser.expr,
      parser.noncode
    )),
    lp.parse_pattern("%)")
  )
  local without_parens = lp.ith(with_parens, 2)
  local string_list = lp.parser_map(
    without_parens,
    function(thing) -- why does this end up nested?
      return thing.thing
    end
  )
  return lp.tag("sexpr", string_list)(...)
end

-- an expression is either a primitive, or an s-expression
parser.expr = lp.parse_any(
  parser.primitive,
  parser.sexpr
)

-- a code file
parser.module = lp.tag("module", lp.parse_many(
  lp.parse_sequence(
    parser.noncode,
    parser.expr,
    parser.noncode
  )
))

return parser
