DISTANCE = 64

function placeBehind()
    turtle.turnLeft()
    turtle.turnLeft()
    turtle.place()
    turtle.turnLeft()
    turtle.turnLeft()
end

function moveForward()
    while (not turtle.forward()) do
        turtle.dig()
    end
end

for i=1,DISTANCE do
    moveForward()
end

turtle.turnLeft()
turtle.turnLeft()
for i=1,DISTANCE do
    if (i % 5) == 0 then
        placeBehind()
    end 
    moveForward()
end
