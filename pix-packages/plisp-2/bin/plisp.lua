pix_require "@stdlib"
fp = pix_require "@libfunc" 
lp = pix_require "@libparse"

local function is(x, t)
  if type(x) == t then
    return true
  elseif type(x) == "table" and x.tag == t then
    return true
  end
end

local function operator2lua(x)
  local op = x[1][1]
  local args = fp.map({unpack(x, 2)}, parsed_plisp2lua)
  if #args == 0 then
    error("found 0 args in op expression: " .. textutils.serialize(x))
  elseif #args == 1 then
    return "(" .. op .. args[1] .. ")"
  elseif #args > 1 then
    return "(" .. op:join(args) .. ")"
  end
end

local function sexpr2lua(x)
  local func = parsed_plisp2lua(x[1])
  local args = fp.map({unpack(x, 2)}, parsed_plisp2lua)
  return func .. "(" .. (", "):join(args) .. ")"
end

-- must be global because of mutual recursion
function parsed_plisp2lua(x)
  if is(x, "sexpr") and is(x[1], "operator") then
    return operator2lua(x)
  elseif is(x, "identifier") then
    return x[1]
  elseif is(x, "sexpr") then
    return sexpr2lua(x)
  elseif is(x, "number") then
    return textutils.serialize(x)
  elseif is(x, "string") then
    return textutils.serialize(x)
  end
end

local function plisp2lua(plispcode)
  -- parse any plisp expression to a lua string
  local parsed, remaining = lp.expr_parser(plispcode)
  if remaining ~= "" then
    error("failed parsing plisp code")
  end
  local luacode = parsed_plisp2lua(parsed)
  return luacode
end

local function runplisp(plispcode)
  local luacode = plisp2lua(plispcode)
  local outpath = "/.gen.lua"
  local outfile = fs.open(outpath, "w")
  outfile.write(luacode)
  outfile.close()
  shell.run(outpath)
end

local function runplispfile(plispfilepath)
  local file = fs.open(plispfilepath, "r")
  local filecontent = file.readAll()
  runplisp(filecontent)
end

local function main()
  if arg[1] ~= nil then
    runplispfile(arg[1])
  end
end

main()

return {
  plisp2lua = plisp2lua,
  runplisp = runplisp
}
