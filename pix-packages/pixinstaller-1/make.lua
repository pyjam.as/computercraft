require "/pix/biplib-3/biplib"
pretty = require "cc.pretty"

local install_dst = shell.resolve("install-pix.lua")
fs.delete(install_dst)

-- get a copy of biplib
-- must have no dependencies..
local f = fs.open("/pix/biplib-3/biplib.lua", "r")
local biplib = f.readAll()
f.close()

function concat(...) 
  local result = {}
  for _, list in ipairs(arg) do
    for _, e in ipairs(list) do
      table.insert(result, e)
    end
  end
  return result
end

-- Bip /pix/pixclient-9

local things = concat(
                fs.find("/pix/pixclient-1/*"), 
                fs.find("/pix/pixclient-1/*/*"), 
                fs.find("/pix/biplib-4/*"),
                fs.find("/pix/biplib-4/*/*"),
                fs.find("/pix/stdlib-2/*"),
                fs.find("/pix/stdlib-2/*/*"),
                fs.find("/pix/libpager-3/*"),
                fs.find("/pix/libpager-3/*/*")
               )
 
local files = {}
for _, thing in ipairs(things) do
  if not fs.isDir(thing) then
    table.insert(files, thing)
  end
end
              
local bipdata = bip("/", files)
local pix_config = '{\n  "packages": [\n    "pixclient-latest"\n  ]\n}'
bipdata = bipdata.."/config.pix\n"..pix_config:len().."\n"..pix_config

local formatted_bipdata = pretty.render(pretty.pretty(bipdata))

local code = biplib.."\n"
code = code..'unbip('..formatted_bipdata..', "/")\n'
code = code..'shell.run("/pix/pixclient-1/boot.lua")\n'
code = code..'shell.run("/pix/pixclient-1/bin/pix.lua", "build")'

local f = fs.open(install_dst, "wb")
f.write(code)
f.close()
