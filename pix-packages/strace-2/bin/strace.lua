local function render_object(object)
  if type(object) == "string" then
    term.setTextColor(colors.red)
    term.write(object)
  elseif type(object) == "number" then
    term.setTextColor(colors.red)
    term.write(object)
  elseif type(object) == "table" then
    term.setTextColor(colors.white)
    term.write("{")
    for name, value in pairs(object) do
      term.setTextColor(colors.lightGray)
      term.write(name .. "=")
      term.setTextColor(colors.white)
      render_object(value)
      term.write(",")
    end
    term.setTextColor(colors.white)
    term.write("}")
  elseif type(object) == "function" then
    term.setTextColor(colors.purple)
    term.write(object)
  else
    term.setTextColor(colors.red)
    term.write("cannot render " .. type(object))
  end
end

local function render_call(name, argnum, args)
  term.setTextColor(colors.yellow)
  term.write(name)
  term.setTextColor(colors.white)
  term.write("(")
  local i = 1
  for name, value in pairs(args) do
    term.setTextColor(colors.lightGray)
    term.write(name .. "=")
    term.setTextColor(colors.white)
    render_object(value)
    if i ~= argnum then
      term.setTextColor(colors.white)
      term.write(", ")
    end
    i = i + 1
  end
  term.setTextColor(colors.white)
  term.write(")")
  write("\n")
end

local function contains(t, v)
  for _, k in ipairs(t) do
    if k == v then return true end
  end
  return false
end
        
local function strace(traced)
  debug.sethook(function (event)
    local MAX_LINE_LENGTH = term.getSize() 
    local LEVEL = 3  -- 1 is the hook, 2 is the wrapper, 3 is user func
  
    if event == "call" or event == "tail call" then
      local info = debug.getinfo(LEVEL, "n")
      if not info or not info.name then return end
      
      if #traced > 0 and not contains(traced, info.name) then return end

      local args = {}
    
      local i = 1
      while true do
        local name, value = debug.getlocal(2, i)
        if not value then break end
        args[name] = value
        i = i + 1
      end
  
      render_call(info.name, i - 1, args)
    end
  end, "c")
end


local function print_usage()
  print([[
  strace -- trace function calls
  
  Usage:
    strace -- myprogram arg1 arg2
    strace function1 function2 -- myprogram arg1]])
end


if #arg == 0 then
  print_usage()
  return
end

local traced = {}
local arg_index = 1
while true do
  local current = arg[arg_index]
  if current == "--" then break end
  if not current then
    print_usage()
    return
  end
  table.insert(traced, current)
  arg_index = arg_index + 1
end

if arg_index == #arg then
  -- user did not supply program to trace
  print_usage()
  return
end

local user_program = "/" .. shell.resolve(arg[arg_index + 1])
arg_index = arg_index + 2
local user_args = {}
while true do
  local current = arg[arg_index]
  if not current then break end
  table.insert(user_args, current)
  arg_index = arg_index + 1
end
arg = user_args

strace(traced)
require(user_program)
