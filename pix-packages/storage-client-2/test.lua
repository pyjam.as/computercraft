local std = pix_require("@stdlib")
local storage = pix_require("init")

local list = storage.list()
std.pprint(list)

local amount = storage.get("diamond", 1, "minecraft:barrel_21")
print(amount)

local details = storage.details("diamond")
std.pprint(details)

local amount = storage.dump(1, "minecraft:chest_279")
std.pprint(amount)

--local result = storage.push(1, 1, "minecraft:barrel_21")
