local FREQUENCY = 60 
local DT = 1/FREQUENCY
local x, y, z = gps.locate()
sleep(DT)

local last_time = os.epoch("local") / 1000
local max = 0

while true do
  local x_, y_, z_ = gps.locate()
  local current_time = os.epoch("local") / 1000
  local dx, dy, dz = x_-x, y_-y, z_-z
  local dt = last_time - current_time
  local vx, vy, vz = dx/dt, dy/dt, dz/dt
 
  -- Print speed
  term.clear()
  term.setCursorPos(1, 1)
  local velocity = math.sqrt(vx*vx + vy*vy + vz*vz)
  max = math.max(max, velocity)
  print("Max: ", max)
  print("Velocity: ", velocity)
  
  -- Consitent update time
  if dt > DT then
    print("WHOOPS", runtime)
  end
  sleep(DT-dt)
  x, y, z = x_, y_, z_
  last_time = current_time
end
