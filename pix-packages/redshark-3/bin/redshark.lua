local std = pix_require("@stdlib")
local pretty = require("cc.pretty")

local modem = peripheral.find("modem")

if (not modem) then
  print("You must have a modem to use redshark!")
  error()
end

function is_normal_rednet(to, from, msg)
  if not msg.nMessageID or type(msg.nMessageID) ~= "number" then
    return false
  elseif not msg.nRecipient then
    return false
  elseif not msg.nSender then
    return false
  elseif not msg.message then
    return false
  end
  
  if msg.nRecipient ~= to then
    return false
  elseif msg.nSender ~= from then
    return false
  end
  
  return true
end

-- Parse arguments
function parse_arg(argument)
  local parts = std.string.split(argument, ",")
  local ids = {}
  
  for _, part in ipairs(parts) do
    if part:match("^%d+$") then
      ids[tonumber(part)] = true
    elseif part:match("^%d+%-%d+$") then
      local bounds = std.string.split(part, "-")
      if bounds[1] > bounds[2] then
        print('Invalid range "'..part..'"')
      end
      for i = bounds[1], bounds[2] do
        ids[i] = true
      end
    else
      print('Invalid id "'..part..'"')
      error()
    end
  end
  
  return ids
end

function print_usage()
  print("Usage: ")
  print("  redshark FROM TO [PROTOCOL]")
  print('  write a computer id, a list of ids, ranges or "*"')
  print()
  print("Examples:")
  print("  redshark 10,12 *")
  print("  redshark 10-20 1,10-20")
  print("  redshark * * pix")
  error()
end

if not arg[1] or not arg[2] then
  print_usage()
end

local from_ids
local to_ids

if arg[1] ~= "*" then
  from_ids = parse_arg(arg[1])
end

-- FIXME: Maybe don't close all
modem.closeAll()
if arg[2] == "*" then
  for i = 0, 100 do
    modem.open(i)
  end
else
  to_ids = parse_arg(arg[2])
  for i, _ in pairs(to_ids) do
    modem.open(i)
  end
end

local protocol = arg[3]

function print_message(message)
  if #pretty.render(pretty.pretty(message)) < 100 then
    std.pprint(msg.message)
  else
    print("<long message>")
  end
end

while true do
  (function()
    _event, _side, to, from, msg, distance = os.pullEvent("modem_message")
    
    -- If we have a filter, and it's not in it, return
    if from_ids and not from_ids[from] then
      return
    end
    if to_ids and not to_ids[to] then
      return
    end
    
    if is_normal_rednet(to, from, msg) then
      -- Check if protcol matches filter
      if protocol and protocol ~= msg.sProtocol then
        return
      end
      
      if msg.sProtocol then
        term.setTextColor(colors.red)
        write("["..from.."->"..to.."] ")
        term.setTextColor(colors.orange)
        print(msg.sProtocol)
        term.setTextColor(colors.white)
        print_message(msg.message)
      else
        term.setTextColor(colors.red)
        write("["..from.."->"..to.."] ")
        term.setTextColor(colors.white)
        print_message(msg.message)
      end
    else
      term.setTextColor(colors.yellow)
      write("["..from.."->"..to.."] ")
      term.setTextColor(colors.white)
      print_message(msg.message)
    end
    -- TODO: log distance
    
  end)()
end

