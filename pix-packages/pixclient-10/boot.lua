-- Inject our pix_require function
function _G.pix_require(module)
  if module == "" then
    print("You wrote pix_require \"\", what do you want?!")
    error()
  end
  local source = debug.getinfo(2, "S").source
  if source:sub(1,1) ~= "@" and source:sub(1,4) ~= "=lua" then
    print("pix_require in eval is fucked, don't do it.")
    print("Please reconsider your life decisions.")
    error()
  end
  
  -- Find the relevant package.pix file
  local path
  if source:sub(1,4) == "=lua" then
    path = shell.dir()
  else
    path = source:sub(2,-1)
  end
  local package_pix_path
  while true do
    if fs.exists(path.."/package.pix") then
      package_pix_path = path.."/package.pix"
      break
    end
    if path == "/" or path == "" then break end
    path = fs.getDir(path)
  end
  if not package_pix_path then
   print("Couldn't find a package.pix file. pix_require only works in pix packages.")
   error()
  end
  
  -- read dependency list
  local f = fs.open(package_pix_path, "rb")
  local package_pix = f.readAll()
  f.close()
  local package_pix, err = textutils.unserialiseJSON(package_pix)
  if err then
    print('pix_require failed to parse "'..package_pix_path..'"')
    print(err)
    error()
  end
  
  local package_path = fs.getDir(package_pix_path)
  local module_path 
  if module:sub(1,1) == "@" then
    -- look for dependency require
    local module = module:sub(2,-1)
    local require_package_name = module:match("^([^/]+)/") or module
    local require_package 
    for _, dependency in ipairs(package_pix.dependencies) do
      if dependency:sub(1,1) == "/" then
        -- Handle local dev paths
         local name = fs.getName(dependency)
         if name == require_package_name then
           require_package = dependency
         end
      else
        -- Handle pix packages
        local name = dependency:match("^([%w%-]+)%-%d+")
        if not name then
          print('pix_require found an invalid dependency "'..dependency..'"')
          error()
        elseif name == require_package_name then
          require_package = "/pix/"..dependency 
          break
        end
      end
    end
    if not require_package then
      print('pix_require couldn\'t find package "'..require_package_name..'" did you add it to your dependency list in package.pix?')
      error()
    end
    module_path = require_package.."/lib/"..(module:match("^[^/]+/(.+)$") or "init")
  else
    -- look for local /lib require
    module_path = "/"..package_path.."/lib/"..module
    if fs.isDir(module_path) then
      module_path = module_path.."/init"
    end
  end
  if not fs.exists(module_path..".lua") then
    print('pix_require didn\'t find anything to require in "'..module_path..'"') 
    error()
  end
  return getfenv(debug.getinfo(2, "f").func).require(module_path)
end

pix_require "@stdlib"
pix_require "init"

-- Remove pix packages from path
local paths = shell.path():split(":")
local filtered_paths = {}
for _, path in pairs(paths) do
  if path:sub(1,4) ~= "/pix" then
    table.insert(filtered_paths, path)
  end
end
shell.setPath((":"):join(filtered_paths))

-- Find and run startup files
local config = read_config("/config.pix")
local installed_packages = fs.find("/pix/*")
-- Find newest version of installed packages
local latest = {}
for _, path in ipairs(installed_packages) do
  local package_name = fs.getName(path) 
  local name, version = parse_package_name(package_name)
  latest[name] = math.max(latest[name] or 0, version)
end

-- Run startup files
for _, package in ipairs(config.packages) do
  local name, version = parse_package_name(package)
  if version == "latest" then
    package = name.."-"..latest[name]
  end
  local package_path = "/pix/"..package
  local startup_file = package_path.."/startup.lua"
  if fs.exists(startup_file) then
    shell.run(startup_file, package_path)
  end
end

if arg[1] == "startup" then
  -- Run pixrc.lua
  local pixrc_path = "/pixrc.lua"
  if fs.exists(pixrc_path) and not fs.isDir(pixrc_path) then
    shell.run(pixrc_path)
  end
end
