-- yeet a file to your friends

local port = arg[1] 
local filename = arg[2]

if port == nil or filename == nil then
  print("throw PORT FILE")
end

port = tonumber(port)

if port == nil or port < 1 or port > 65535 then
  print("PORT must be between 1 and 65535")
end

local modem = peripheral.find("modem")
if modem == nil then
  print("no modem found")
  return
end

modem.open(port)
f = fs.open(shell.resolve(filename), "r")
message = f.readAll()
f.close()
modem.transmit(port, port, message)
