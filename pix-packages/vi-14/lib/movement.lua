-- vi bindings for movement
-- does not implement multiline movement e.g. pressing "w" at the end of a line

pix_require("@regex")

local brackets = {
  ["("] = ")",
  ["{"] = "}",
  ["["] = "]",
  ["<"] = ">",
  [")"] = "(",
  ["}"] = "{",
  ["]"] = "[",
  [">"] = "<",
  ["'"] = "'",
  ["\""] = "\"",
}

function find_bracket(x, y, bracket, dir)
  local nests = 0
  local dist = 0
  while true do
    x = x + dir
    dist = dist + 1
    if x > string.len(buf().data[y]) or x < 0 then
      y = y + dir
      if y > #buf().data or y < 1 then
        return nil, nil, nil
      end
      if dir == 1 then
        x = 1
      else
        x = string.len(buf().data[y])
      end
    end
    local char = string.charAt(buf().data[y], x) 
    if char == bracket and brackets[bracket] ~= bracket then
      nests = nests + 1
    elseif brackets[bracket] == char then
      if nests == 0 then
        return x, y, dist
      else
        nests = nests-1
      end
    end
  end
  return nil, nil, nil
end

function vi_percent(cursor_x, cursor_y, offset)
  offset = offset or 0
  -- forward until we hit a bracket
  local line = buf().data[cursor_y]
  local bracket_at
  local bracket
  for i=cursor_x, line:len() do 
    bracket = string.charAt(line, i)
    if brackets[bracket] then bracket_at = i break end
  end
  if not bracket_at then return cursor_x, cursor_y end
  local dirs = {}
  if bracket:match("[\"']") then dirs = {1, -1}
  elseif bracket:match("[%[%({<]") then dirs = {1}
  else dirs = {-1} end
  local x, y = bracket_at, cursor_y
  local result = {}
  for _, dir in ipairs(dirs) do
    local x, y, dist = find_bracket(x, y, bracket, dir)
    if x and y then
      table.insert(result, {x=x, y=y, dist=dist})
    end
  end
  table.sort(result, function(a,b) return a.dist < b.dist end)
  if #result > 0 then 
    return result[1].x+offset, result[1].y
  else
    return cursor_x, cursor_y
  end
end
function vi_W(cursor_x, cursor_y, offset)
  offset = offset or 0
  local line = buf().data[cursor_y]
  local _, non_white = line:find("%S*%s*", cursor_x)
  return lim_x(non_white+1, cursor_y, offset), cursor_y
end

function vi_cW(cursor_x, cursor_y, offset, last)
  offset = offset or 0
  if not last then return vi_W(cursor_x, cursor_y, offset) end
  local line = buf().data[cursor_y]
  local _, non_white = line:find("%S*", cursor_x)
  local _, white = line:find("%s*", cursor_x)
  return lim_x(math.max(non_white, white)+1, cursor_y, offset), cursor_y
end

function vi_w(cursor_x, cursor_y, offset)
  offset = offset or 0
  local line = buf().data[cursor_y]
  local _, word = line:find("[%w_]*%s*", cursor_x)
  local _, punc = line:find("[%p^_]*%s*", cursor_x)
  return lim_x(math.max(word, punc)+1, cursor_y, offset), cursor_y
end

function vi_cw(cursor_x, cursor_y, offset, last)
  offset = offset or 0
  if not last then return vi_w(cursor_x, cursor_y, offset) end
  local line = buf().data[cursor_y]
  local _, word = line:find("[%w_]*", cursor_x)
  local _, punc = line:find("[%p^_]*", cursor_x)
  local _, white = line:find("%s*", cursor_x)
  return lim_x(math.max(word, punc, white)+1, cursor_y, offset), cursor_y
end

function vi_e(cursor_x, cursor_y, offset)
  offset = offset or 0
  local line = buf().data[cursor_y]
  local _, word = line:find("%s*[%w_]*", cursor_x+1)
  local _, punc = line:find("%s*[%p^_]*", cursor_x+1)
  return lim_x(math.max(word, punc), cursor_y, offset)+offset, cursor_y
end

function vi_b(cursor_x, cursor_y, offset)
  offset = offset or 0
  local line = buf().data[cursor_y]
  local word, _ = line:sub(1,cursor_x-1):find("[%w_]*%s*$")
  local punc, _ = line:sub(1,cursor_x-1):find("[%p^_]*%s*$")
  return lim_x(math.min(word, punc), cursor_y, offset), cursor_y
end

function vi_B(cursor_x, cursor_y, offset)
  offset = offset or 0
  local line = buf().data[cursor_y]
  local non_white, _ = line:sub(1,cursor_x-1):find("%S*%s*$")
  return lim_x(non_white, cursor_y, offset), cursor_y
end

function lim_y(n)
  return math.min(math.max(1, n), #buf().data)
end

function lim_x(cursor_x, cursor_y, offset)
  offset = offset or 0
  return math.max(1, math.min(cursor_x, string.len(buf().data[cursor_y])+offset))
end

function vi_j(cursor_x, cursor_y, offset)
  local cursor_y = lim_y(cursor_y+1)
  return lim_x(buf().max_cursor_x, cursor_y), cursor_y, true
end

function vi_k(cursor_x, cursor_y, offset)
  local cursor_y = lim_y(cursor_y-1)
  return lim_x(buf().max_cursor_x, cursor_y), cursor_y, true
end

function vi_h(cursor_x, cursor_y, offset)
  return lim_x(cursor_x-1, cursor_y), cursor_y
end

function vi_l(cursor_x, cursor_y, offset)
  offset = offset or 0
  return lim_x(cursor_x+1, cursor_y, offset), cursor_y
end

function vi_a(cursor_x, cursor_y, offset)
  -- like l but without normal limits
  if #buf().data[cursor_y] == 0 then
    return cursor_x, cursor_y
  else
    return cursor_x+1, cursor_y, cursor_y
  end
end

function vi_A(cursor_x, cursor_y, offset)
  if #buf().data[cursor_y] == 0 then
    return cursor_x, cursor_y
  else
    local cursor_x, cursor_y =  vi_end(cursor_x, cursor_y)
    return cursor_x, cursor_y
  end
end

function vi_0(cursor_x, cursor_y, offset)
  return 1, cursor_y
end

function vi_end(cursor_x, cursor_y, offset)
  offset = offset or 0
  return math.max(1, string.len(buf().data[cursor_y])+offset), cursor_y
end

function vi_nop(cursor_x, cursor_y, offset)
  return cursor_x, cursor_y, nil, true
end

function vi__(cursor_x, cursor_y, offset)
  offset = offset or 0
  local line = buf().data[cursor_y]
  for i=1,#line do
    if string.charAt(line, i) ~= " " then return i, cursor_y end
  end
  return math.max(1, string.len(line)+offset), cursor_y
end

function vi_gg(cursor_x, cursor_y, offset)
  cursor_x = lim_x(cursor_x, 1)
  return cursor_x, 1
end

function vi_G(cursor_x, cursor_y, offset)
  cursor_x = lim_x(cursor_x, #buf().data)
  return cursor_x, #buf().data
end

function vi_n(cursor_x, cursor_y, offset, query)
  offset = offset or 0
  query = query or search
  for i=cursor_y, #buf().data do
    local adjust = cursor_x+1
    if i ~= cursor_y then
      adjust = 1
    end
    local matches = find_all(query, string.sub(buf().data[i], adjust, -1))
    for _, match in pairs(matches) do
      return adjust + match.from - 1 + offset, i
    end
  end
  return cursor_x, cursor_y
end

function vi_N(cursor_x, cursor_y, offset, query)
  query = query or search
  for i=cursor_y, 1, -1 do
    local adjust = cursor_x-1
    if i ~= cursor_y then
      adjust = #buf().data[i]
    end
    local matches = find_all(query, string.sub(buf().data[i], 1, adjust))
    for j=#matches,1,-1  do
      local match = matches[j]
      return match.from, i
    end
  end
  return cursor_x, cursor_y
end

function vi_t(char)
  return vi_tf(char, 1, vi_n) 
end

function vi_f(char)
  return vi_tf(char, 0, vi_n) 
end

function vi_T(char)
  return vi_tf(char, -1, vi_N) 
end

function vi_F(char)
  return vi_tf(char, 0, vi_N) 
end

function vi_tf(char, adjust, n_func)
  local func = function(cursor_x, cursor_y, offset)
    offset = offset or 0
    local new_cursor_x, new_cursor_y = n_func(cursor_x+adjust, cursor_y, offset, char)
    if new_cursor_y ~= cursor_y then
      return cursor_x, cursor_y
    else
      return new_cursor_x-adjust, new_cursor_y
    end
  end
  return func
end
