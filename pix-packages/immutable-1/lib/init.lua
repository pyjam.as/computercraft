
local std  = pix_require("@stdlib");

local lib  = {};

function lib.get (t, key) return t[key] end;

lib.clone  = std.table.clone;

local function clone_and (f)
  -- clone the table before running f
  return (function (t, ...) return f(lib.clone(t), ...) end) end;

local function _set (t, k, v, ...)
  -- set keys by mutating t
  t[k]  = v;
  local rest  = {...};
  return (function()if (#rest < 2) then 
      return t else 
      return _set(t, unpack(rest))
   end end)()
 end;

-- set key/value pairs on clone
lib.set  = clone_and(_set);
  
  
local function _append (t, e, ...)
  -- append using table.insert
  local rest  = {...};
  table.insert(t, e);
  return (function()if (#rest == 0) then 
      return t else 
      return _append(t, unpack(rest))
   end end)()
 end;

-- append values to clone
lib.append  = clone_and(_append);

local function _prepend(t, ...)
  -- prepend using mutating append
  return _append({...}, unpack(t)) end;
  
-- prepend values to clone
lib.prepend  = clone_and(_prepend);
  
return(lib);